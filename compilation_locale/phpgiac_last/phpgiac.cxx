// -*- mode:C++ ; compile-command: "g++ phpgiac.cxx -o test  -lgiac -lgmp" -*-
#include "phpgiac.h"
#include <iostream>
#include <sstream>
#include<map>
#include <giac/giac.h>
#include <giac/plot.h>
//#include <giac/input_lexer.h>

#include <giac/first.h>

#include <giac/gen.h>
#include <giac/symbolic.h>
#include <giac/usual.h>
#include <giac/intg.h>
#include <giac/derive.h>
#include <giac/series.h>

#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include <math.h>



using namespace std;
using namespace giac;


std::map<std::string,giac::context *> session_cptr;
string lemarqueur="`";
string svg_legend="";

int taille_map(){
  return session_cptr.size();
}


context * initialise_cptr(std::string s_ctx){
  setenv("XCAS_HELP","/usr/local/share/giac/doc/fr/keywords",1);
  context * cptr=session_cptr[s_ctx];
  if (!cptr){
    cptr=new context;
    session_cptr[s_ctx]=cptr;
  } 
  giac::add_language(1,cptr);
  return cptr;
}

int giac_lang(int lang, std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  giac::set_language(lang, cptr);
  return lang;
}

std::string giac_eval_txt(std::string command, std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  return g.print(cptr);
}

std::string giac_eval_prog(std::string command, std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  string message="";
  if (first_error_line(cptr)!=0) {
    std::ostringstream out;
    out<<first_error_line(cptr);
    message=out.str()+
      lemarqueur+
      error_token_name(cptr);
  }
  return message;
}


std::string capture;
std::string capture_begin="<mtable><mtr><mtd><mtext>Impression : </mtext></mtd></mtr>";
std::string capture_end="<mtr><mtd><mtext>Valeur de retour :</mtext></mtd></mtr><mtr><mtd>";
std::string table_end="</mtd></mtr></mtable>";

giac::gen capture_print(const giac::gen & g,GIAC_CONTEXT){
  if (g.is_symb_of_sommet(at_print))
    capture+="<mtr><mtd>"+gen2mathml(g._SYMBptr->feuille,contextptr)+"</mtd></mtr>";
  return g;
}

std::string giac_eval_mathml(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  g=eval(g,cptr);
  if (capture!="")
    return capture_begin+
      capture+
      capture_end+
      gen2mathml(g,cptr)+
      table_end;
  return gen2mathml(g,cptr);
}



std::string giac_txt_mathml_et_eval(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  gen h=eval(g,cptr);
  if (capture!="")
    return   h.print(cptr)+
      lemarqueur+
      gen2mathml(g,cptr)+
      "<mo>=</mo>"+
      capture_begin+
      capture+
      capture_end+
      gen2mathml(h,cptr)+
      table_end;
  return  h.print(cptr)+lemarqueur+gen2mathml(g,cptr)+"<mo>=</mo>"+gen2mathml(h,cptr);
}


std::string giac_txt_mathml(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  //g=eval(g);
  if (capture!="")
    return g.print(cptr)+
      lemarqueur+
      capture_begin+
      capture+
      capture_end+
      gen2mathml(g,cptr)+
      table_end;
  return g.print(cptr)+lemarqueur+gen2mathml(g,cptr);
}


std::string giac_eval_txt_mathml(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  g=eval(g,cptr);
  if (capture!="")
    return g.print(cptr)+lemarqueur+capture_begin+capture+capture_end+gen2mathml(g,cptr)+table_end;
  return g.print(cptr)+lemarqueur+gen2mathml(g,cptr);
}




std::string giac_suite(std::string nomSortie,std::string nomRec, std::string u0Rec, std::string fRec,std::string nomRec2, std::string u0Rec2, std::string fRec2, std::string nomEx, std::string fEx, int n,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen ini(nomRec+":="+u0Rec,cptr);
  ini=eval(ini,cptr);
  gen rec(nomRec+":="+fRec,cptr);
  gen ini2(nomRec2+":="+u0Rec2,cptr);
  ini2=eval(ini2,cptr);
  gen rec2(nomRec2+":="+fRec2,cptr);
  gen ex(nomEx+":="+fEx,cptr);
  gen x(nomRec,cptr);
  gen y(nomEx,cptr);
  gen z(nomSortie,cptr);
  vecteur v; 
  if (x.type==_VECT)
    v=*x._VECTptr;
  else
    v.push_back(x);
  vecteur w; 
  if (y.type==_VECT)
    w=*y._VECTptr;
  else
    w.push_back(y);
  vecteur vw; 
  if (z.type==_VECT)
    vw=*z._VECTptr;
  else
    vw.push_back(z);
  int i,j;
  std::string sortie="";
  for (i=0 ; i<=n ; i++){
    // evaluation de la suite explicite
    evalf(ex,1,cptr);
    for (j=0;j<vw.size();j++){
      sortie+=eval(vw[j],cptr).print(cptr);
      if (j<vw.size()-1)
        sortie+=",";
    }
    sortie+=lemarqueur; 
    // evaluation de la suite recurrente1
    // suite recurrente 2
    eval(rec2,cptr);
    if (i<n)
      evalf(rec,1,cptr);
  }
  return sortie;
}



//capture svg



// nouvelle version
string sSVG;
gen s_svg_capture(const gen &g,GIAC_CONTEXT){
  if (g.is_symb_of_sommet(at_print)){
    capture+="<mtr><mtd>"+gen2mathml(g._SYMBptr->feuille,contextptr)+"</mtd></mtr>";
    return g;
  }
 if (g.type== _SYMB || g.type== _VECT) 
   sSVG+=gen2svg(g,contextptr);
 return g;
}


std::string giac_eval_svg(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
 //recup des sorties graphiques
  __interactive.op=&s_svg_capture;
  gen g(command,cptr);
  g=eval(g,cptr);
// test pour la  3d : pas au point 
  if (g.is_symb_of_sommet(at_pnt) && g.subtype>=0){ //3d  
  } 
  else {   
    return sSVG+lemarqueur+svg_legend;
  }
}



std::string giac_eval_txt_mathml_svg(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
 //recup des sorties graphiques
  __interactive.op=&s_svg_capture;
  gen g(command,cptr);
  g=eval(g,cptr);
// test pour la  3d : pas au point 
  if (g.is_symb_of_sommet(at_pnt) && g.subtype>=0){ //3d
  } 
  else {
    return g.print(cptr)+
      lemarqueur+
      capture_begin+
      lemarqueur+
      capture+
      lemarqueur+
      capture_end+
      lemarqueur+
      gen2mathml(g,cptr)+
      lemarqueur+
      table_end+
      lemarqueur+
      sSVG+  
      lemarqueur+
      svg_legend; 
  }
}



std::string giac_svg_preamble(double width_cm, double height_cm){
  return svg_preamble(width_cm,height_cm);
}

std::string giac_svg_grid(){
  return svg_grid();
}

std::string spread2cep(const matrice & m,GIAC_CONTEXT){
  std::string s="";
  int l=m.size();
  if (!l)
    return s;
  int c=m.front()._VECTptr->size();
  for (int i=0;i<l;++i){
    for (int j=0;j<c;++j){
      s+=m[i][j][1].print(contextptr)+lemarqueur+m[i][j][2].print(contextptr)+lemarqueur;
    }
  }
  return s;
}   

// interface pour CasEnPoche -*- mode:C++ ; compile-command: "g++ -I.. -g -c phpgiac.cxx" -*-
std::string giac_eval_cep(std::string command,std::string s_ctx){ 
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  if (g.type==_VECT && g.subtype==_SPREAD__VECT)
    return spread2cep(*g._VECTptr,cptr);
  else
    return g.print(cptr);
}

std::string spread2giacOnLine(const matrice & m,GIAC_CONTEXT){
  std::string s="";
  int l=m.size();
  if (!l)
    return s;
  int c=m.front()._VECTptr->size();
  for (int i=0;i<l;++i){
    for (int j=0;j<c;++j){
      s+=m[i][j][1].print(contextptr)+lemarqueur+gen2mathml(m[i][j][1],contextptr)+lemarqueur;
    }
  }
  return s;
}   


// interface pour le tableur de Xcas en ligne
std::string giac_eval_spread(std::string command,std::string s_ctx){ 
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  if (g.type==_VECT && g.subtype==_SPREAD__VECT)
    return spread2giacOnLine(*g._VECTptr,cptr);
  else
    return g.print(cptr);
}

std::string evalfSpread2giacOnLine(const matrice & m,GIAC_CONTEXT){
  std::string s="";
  int l=m.size();
  gen g;
  if (!l)
    return s;
  int c=m.front()._VECTptr->size();
  for (int i=0;i<l;++i){
    for (int j=0;j<c;++j){
      g=evalf(m[i][j][1],1,contextptr);
      s+=g.print(contextptr)+lemarqueur+gen2mathml(g,contextptr)+lemarqueur;
    }
  }
  return s;
}   

std::string giac_evalf_spread(std::string command,std::string s_ctx){ 
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  if (g.type==_VECT && g.subtype==_SPREAD__VECT)
    return evalfSpread2giacOnLine(*g._VECTptr,cptr);
  else
    return g.print(cptr);
}



std::string giac_archive_session(std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  std::string s=archive_session(0,cptr);
  //elimine_char_null(s);
  return s; 
}

void giac_unarchive_session(std::string arch,std::string s_ctx){
  giac::child_id=1;
  sSVG="";
  // variable de mathml.cc de giac
  svg_legend="";
  capture="";
  context * cptr=initialise_cptr(s_ctx);
  std::string s_arch(arch);
  gen g("1",cptr);
  unarchive_session_string(s_arch,-1,g,cptr);
  return;
}


// sortie pour labomep
// ne recupere pas l'impression

std::string vect2labomep(const vecteur & v, GIAC_CONTEXT){
  string s("");
  vecteur::const_iterator it=v.begin(),itend=v.end();
  for (;it!=itend;){
    s += lemarqueur+gen2mathml(*it,contextptr);  
    ++it;   
  }
  return s;
}

std::string giac_labomep(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  //recup des sorties graphiques
  __interactive.op=&s_svg_capture;
  gen g(command,cptr);
  g=eval(g,cptr);
  string s=g.print(cptr);
  if (g.type==_VECT)
    s+=vect2labomep(*g._VECTptr, cptr);  
  else
    s+=lemarqueur+gen2mathml(g,cptr);
  s+=lemarqueur+"SVG"+sSVG;
  s+=lemarqueur+svg_legend;
  return s;
}

// traduction xml des sorties graphiques

static string color_string(int color){
  int i;
  string colors[5]={"black","red","green","yellow","blue"}; 
  if (i>-1 && i<5)
    return colors[i];
  return "black";
}

static string open_xml_tag(string name, int color,string legend){
  string s= "<"+name;
  if (legend!="")
    s+=" legend='"+legend+"'";
  if (color!=-1)
    s+=" color='"+color_string(color)+"'";
  return s;
}

static string close_xml_tag(string name){
  return "</"+name+">\n";
}

static string point2xml(gen A, int  color, string legend,GIAC_CONTEXT){
  string s=open_xml_tag("pt",color,legend);
  A=evalf(A,1,contextptr); 
  s+=" x='"+
    re(A,contextptr).print(contextptr)+
    "' y='"+
    im(A,contextptr).print(contextptr)+
    "' />\n";
  return s;
}

static string circle2xml(gen diameter0, gen diameter1, int color, string legend,GIAC_CONTEXT){
  string deca=" ";
  string s=open_xml_tag("circle",color,legend)+"/>\n";
  gen center=evalf((diameter0+diameter1)/2,1,contextptr);
  gen radius=evalf(abs(diameter1-diameter0)/2,1,contextptr);
  s+=point2xml(center,-1,"",contextptr);
  s+=open_xml_tag("radius",-1,"")+
  "='"+radius.print(contextptr)+"' />\n";
  s+=close_xml_tag("circle");
  return s;
  }


static string curve2xml(gen g, int color, string legend,GIAC_CONTEXT){ 
  string deca=" ";
  int i;
  string s=open_xml_tag("curve",color,legend)+" />\n";
  g=evalf(g,1,contextptr);
  vecteur v=*(g._VECTptr);
  for (i=0 ; i<signed(v.size()) ; i++)
    s+=deca+point2xml(v[i],-1,"",contextptr);  
  s+=close_xml_tag("curve");
  return s;
}

static string polyline2xml(gen g,int color, string legend,GIAC_CONTEXT){
  string x="x", y="y", s, tagname, deca=" ";
  int i,n;
  g=eval(g,eval_level(contextptr),contextptr);
  vecteur v=*(g._VECTptr);
  n=v.size();
  if (v[0]==v[v.size()-1]){
    tagname="polygon";
    n-=1;
  }
  else
    tagname="polyline";
  s=open_xml_tag(tagname,color,legend)+"/>\n";
  for (i=0 ; i<n; i++){
    s+=point2xml(v[i],-1,"",contextptr);
  }
  s+=close_xml_tag(tagname);
  return s;
}

static string bipoint_line2xml(string name,gen A, gen B, int  color, string legend,GIAC_CONTEXT){
   string s=open_xml_tag(name,color,legend)+"/>\n";
   string deca=" ";  
   A=evalf(A,1,contextptr); 
   B=evalf(B,1,contextptr);
   s+=deca+point2xml(A,-1,"",contextptr);
   s+=deca+point2xml(B,-1,"",contextptr);
   s+=close_xml_tag(name);
   return s;
 }

static string symbolic2xml(const symbolic & mys,GIAC_CONTEXT);


//fonction appelee ssi v est un vecteur
static string vect2xml(gen v, int color, string name,GIAC_CONTEXT){
  if (v.type != _VECT)
    return "error";
  if (v.subtype==_SYMB){
    if (v[0].type==_VECT){
      return vect2xml(v[0], color, name,contextptr);
    }
    if (v[0].type==_SYMB)
      return symbolic2xml(*v[0]._SYMBptr,contextptr);
  }
  if (v.subtype==_GROUP__VECT)
    return polyline2xml(v, color, name,contextptr);
  if (v.subtype==_LINE__VECT)
    return bipoint_line2xml("line",v[0],v[1], color, name,contextptr);
  if (v.subtype==_HALFLINE__VECT)
    return bipoint_line2xml("halfline",v[0],v[1], color, name,contextptr);
  return "vect2svg error";
}

static string symbolic2xml(const symbolic & mys,GIAC_CONTEXT){ 

  int color=default_color(contextptr);
  string name="";
  if (mys.sommet==at_pnt){
    vecteur v=*(mys.feuille._VECTptr);
    if(v.size()==3)
      name=v[2].print(contextptr); 
    if (v[0].type==_VECT){
      return vect2xml(v[0], color, name,contextptr);
    }               
    if (v[0].type==4)           //indispensable, mais je ne sais pas pourquoi
      v[0]=gen(v[0].print(contextptr),contextptr);
    if (v[0].type==_SYMB){ 
      symbolic figure=*v[0]._SYMBptr; 
      if (figure.sommet == at_curve){
	gen curve=figure.feuille;
	return curve2xml(curve[1],color,name,contextptr);
      }
      if (figure.sommet == at_pnt)
	return symbolic2xml(figure,contextptr);
      if (figure.sommet==at_segment){
	gen segment=figure.feuille;
	return bipoint_line2xml("segment",segment[0],segment[1], color, name,contextptr); 
      } 
      if (figure.sommet==at_droite ){
	gen segment=figure.feuille;
	return bipoint_line2xml("line",segment[0],segment[1], color, name,contextptr);
      }
      if (figure.sommet==at_demi_droite ){
	gen segment=figure.feuille;
	return bipoint_line2xml("halfline",segment[0],segment[1], color, name,contextptr);
      }
      if (figure.sommet==at_cercle ){
	gen diametre;
	if (figure.feuille[0].type==_VECT)
	  diametre=figure.feuille[0];
	else
	  diametre=figure.feuille;
	return circle2xml(diametre[0],diametre[1], color, name,contextptr);
      }
      return point2xml(v[0], color, name,contextptr); 
    }
    return point2xml(v[0], color, name,contextptr); 
  }
  return "undef";
}

string gen2xml(const gen &e,GIAC_CONTEXT){  
  if (e.type== _SYMB)
    return symbolic2xml(*e._SYMBptr,contextptr);
  if (e.type==_VECT){
    string s;
    vecteur v=*e._VECTptr;
    for (int i=0; i<signed(v.size()); i++){
      if (v[i].type==_SYMB){
	symbolic sym=*v[i]._SYMBptr; 
	if (sym.sommet==at_pnt)
	  s=s+symbolic2xml(sym,contextptr);
      }
    }
    return s;
  }
  return "error";
}

string sXML;
gen s_xml_capture(const gen &g,GIAC_CONTEXT){
  if (g.is_symb_of_sommet(at_print)){
    capture+="<mtr><mtd>"+gen2mathml(g._SYMBptr->feuille,contextptr)+"</mtd></mtr>";
    return g;
  }
  if (g.type== _SYMB || g.type== _VECT) 
    sXML+=gen2xml(g,contextptr);
  return g;
}

std::string giac_graph2xml(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  //recup des sorties graphiques
  __interactive.op=&s_xml_capture;
  gen g(command,cptr);
  g=eval(g,cptr);
  // test pour la  3d : pas au point 
  if (g.is_symb_of_sommet(at_pnt) && g.subtype>=0){ //3d  
  } 
  else {   
    return sXML;
  }
}
  



// decommenter pour compil directe a titre de test

//main(){
//}






