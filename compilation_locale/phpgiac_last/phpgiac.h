#include <string>
#include <giac/giac.h>
#include <giac/plot.h>
//#include <giac/input_lexer.h>


using namespace std;

// string s variable de contexte 
int taille_map();
int giac_lang(int lang, std::string s_ctx);
std::string giac_txt_mathml(std::string command, std::string s);
std::string giac_txt_mathml_et_eval(std::string command,std::string s);
std::string giac_eval_prog(std::string command, std::string s_ctx);
std::string giac_eval_txt(std::string command,std::string s);
std::string giac_eval_mathml(std::string command,std::string s);
std::string giac_eval_txt_mathml(std::string command,std::string s);
std::string giac_labomep(std::string command,std::string s_ctx);
std::string giac_suite(std::string nomSortie,std::string nomRec, std::string u0Rec, std::string fRec, std::string nomRec2, std::string u0Rec2, std::string fRec2,std::string nomEx, std::string fEx, int n,std::string s);
std::string giac_eval_svg(std::string command,std::string s);
std::string giac_eval_txt_mathml_svg(std::string command,std::string s);
std::string giac_svg_preamble(double width_cm, double height_cm);
std::string giac_svg_grid();
std::string giac_eval_cep(std::string command,std::string s);
std::string giac_eval_spread(std::string command,std::string s);
std::string giac_evalf_spread(std::string command,std::string s);
std::string giac_graph2xml(std::string command,std::string s_ctx);
std::string giac_archive_session(std::string s);
void giac_unarchive_session(std::string arch,std::string s);
