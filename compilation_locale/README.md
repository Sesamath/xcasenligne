Compilation de php_giac
=======================

La dernière compilation réussie de php_giac a été faite sous debian squeeze (6).

php_giac utilise une version légèrement modifiée de giac (de Bernard Parisse) qui a pas mal de dépendances.
Ci-dessous les notes prises lors de tentatives de compilation sous debian jessie et
wheezy

Les sources de php_giac mises à disposition ici ont été créées par
Jean-Pierre Branchard.

giac est développé par Bernard Parisse.

jessie
------

En root, on installe les dépendances (cf http://www-fourier.ujf-grenoble.fr/~parisse/giac_compile_fr.html et https://www-fourier.ujf-grenoble.fr/~parisse/install_fr#linux)

On tente via le dépôt https://www-fourier.ujf-grenoble.fr/~parisse/install_fr#linux, en mettant dans /etc/apt/sources.list.d/giac.list
```
# il n'y a pas de nom de release dans ce dépôt, obligé de passer par stable
deb http://www-fourier.ujf-grenoble.fr/~parisse/debian/ stable main
deb-src http://www-fourier.ujf-grenoble.fr/~parisse/debian/ stable main
```

```bash
# 1er pb, la clé
wget https://www-fourier.ujf-grenoble.fr/~parisse/xcas_public_key.gpg
apt-key add xcas_public_key.gpg
# màj
aptitude update
aptitude install giac
# => pb de dépendance avec libjpeg8 qui n'existe pas dans jessie
# On peut forcer avec
dpkg --force-depends -i giac_1.2.2-75_amd64.deb
# mais ensuite il voudra virer le paquet à chaque mise à jour, 
# faudrait construire un fake libjpeg8 et l'installer
```

On passe à la compilation manuelle, mais on va tenter d'après les sources du paquet deb de Bernard Parisse

```bash
# outils de compil
aptitude install gcc g++  make g++ checkinstall

# pour giac
aptitude install libgmp-dev libmpfr-dev libmpfi-dev libpari-dev libgsl0-dev libpng12-dev libjpeg-dev libreadline-dev libncurses5-dev libntl-dev libao-dev hevea libgmp3-dev libmpfr-dev
# on laisse juste de coté les lib X : libxext-dev mesa-common-dev libx11-dev libxt-dev libxft-dev libgl1-mesa-dev libgl-dev

cd /var/www/xcas
mkdir -p deb/build
cd deb/build
# install des dépendances
apt-get build-dep giac
# on lance la compil
apt-get -b source giac
# il manque gmp.h, on le cherche
apt-file search gmp.h
# et on installe le paquet qui le contient
aptitude install libgmp-dev
# on utilisera apt-build
aptitude install apt-build
# On tente la compil
apt-build build-source giac 2>&1 |tee build-source.log
```

Ça passe toujours pas, pas mal d'erreurs…

On tente la solution proposée sur http://xcas.e.ujf-grenoble.fr/XCAS/viewtopic.php?f=3&t=1563&start=15#p7722
```bash
mkdir /var/www/xcas/tmp
cd /var/www/xcas/tmp
aptitude download giac=1.2.2-75
ar x giac_1.2.2-75_amd64.deb 
mkdir control
cd control
tar xf ../control.tar.gz
# On remplace libjpeg8 par libjpeg62 (qui est dispo dans jessie)
sed -i -e 's/libjpeg8 (>= 8c)/libjpeg62-turbo | libjpeg62/' control
# On change la version pour ajouter un a en suffixe
sed -i -e 's/^Version\(.*\)$/Version\1a/' control
# et on recrée le control.tar.gz avec tout ça
mv ../control.tar.gz ../control.orig.tar.gz
tar -zcf ../control.tar.gz ./
cd ..
ar rc giac_1.2.2-75a_amd64.deb debian-binary control.tar.gz data.tar.gz
dpkg -i giac_1.2.2-75a_amd64.deb
# et c'est passé :-)
```

```bash
# pour phpgiac 
aptitude install swig php5-dev 
```


wheezy
------

Ce qui est ci-dessous a été réalisé sous debian wheezy.

En root, on installe les dépendances (cf http://www-fourier.ujf-grenoble.fr/~parisse/giac_compile_fr.html)
```bash
# outils de compil
aptitude install gcc g++  make g++ checkinstall

# pour giac
aptitude install libgmp3-dev libmpfr-dev libpari-dev libgsl0-dev libfltk1.1-dev libxext-dev libpng12-dev libjpeg62-dev libreadline5-dev libncurses5-dev  latex2html hevea 
# on laisse juste de coté les lib X : xlibmesa-gl-dev libx11-dev libxt-dev libxft-dev

# pour phpgiac 
aptitude install swig php5-dev 

# pour l'install de giac (la génération de la doc), sous squeeze il fallait aussi texinfo texlive-math-extra
# On verra si on peut s'en passer
```

Et sous un user ordinaire on va pouvoir compiler

```bash
cd ~/compilation_locale_wheezy

# compil ntl
wget http://shoup.net/ntl/ntl-5.5.2.tar.gz
tar xf ntl-5.5.2.tar.gz
cd ntl-5.5.2/src/
# le tee sert à garder une trace de ce qui défile à l'écran
./configure NTL_GMP_LIP=on NTL_STD_CXX=on 2>&1 | tee configure.log
make 2>&1 | tee make.log

#####
#ici, sous wheezy, ça tourne en rond et la compil termine jamais, idem en 6.0.0 :-/
######

# on vérifie qu'il n'y a pas eu d'erreur
egrep -i '(err|warn)' make.log
# on prépare le rep de doc pour checkinstall (même si là on a pas grand chose à mettre dedans)
mkdir doc-pak
cp -a VERSION_INFO doc-pak/
# et on créé le paquet deb et installer (en root),
# (donner la description et vérifier les différents champs)
checkinstall -D --maintainer tech@sesamath.net

# On oublie CoCoA qui fait planter la compil de giac (cf + loin)

# on peut enfin passer à giac (user ordinaire)
cd /var/www/www.xcasenligne.net/compilation_locale/
# On récupère la dernière version stable
wget ftp://ftp-fourier.ujf-grenoble.fr/xcas/giac_frozen.tgz
tar xf giac_frozen.tgz
# on remplace le mathml.cc par celui modifié par Jean-Pierre Branchard
cp mathml.cc.mod_by_jpb giac-0.8.6/src/mathml.cc
cd giac-0.8.6
./configure 2>&1 | tee configure.log
make 2>&1 | tee make.log
# on vérifie qu'il n'y a pas eu d'erreur
egrep -i '(err|warn)' make.log
# on prépare le rep de doc pour checkinstall (même si là on a pas grand chose à mettre dedans)
mkdir doc-pak
cp AUTHORS COPYING INSTALACION INSTALL LEAME.ipaq LEAME.WIN README NEWS TODO TROUBLES doc-pak/
# et on créé le paquet deb que l'on installe (en root donc)
checkinstall -D --maintainer tech@sesamath.net

# et on compile phpgiac
cd /var/www/www.xcasenligne.net/compilation_locale/phpgiac
./compile 2>&1 | tee compile.log
# reste à copier le so produit à sa place (en root)
cp -a phpgiac.so /usr/lib/php5/20060613+lfs/
```

CoCoA
-----

Pas réussi à installer CoCoa sans faire planter la compil de giac, on met ici ce qui a été fait pour mémoire, pour une éventuelle prochaine fois...

```bash
# CoCoa
cd /var/www/www.xcasenligne.net/compilation_locale/
wget ftp://ftp-fourier.ujf-grenoble.fr/xcas/devel/sources/CoCoALib-0.9915.tgz
tar xf CoCoALib-0.9915.tgz 
cd CoCoALib-0.9915/
./configure 2>&1 | tee configure.log
make 2>&1 | tee make.log
# => et là ça plante, on passe à la dernière version sur le site officiel
cd /var/www/www.xcasenligne.net/compilation_locale/
wget http://cocoa.dima.unige.it/cocoalib/tgz/CoCoALib-0.9937.tgz
tar xf CoCoALib-0.9937.tgz 
cd CoCoALib-0.9937/
./configure 2>&1 | tee configure.log
# pfff, faut tout lui dire...
./configure --with-libgmp=/usr/lib/libgmp.so 2>&1 | tee configure.log
make 2>&1 | tee make.log
# c'est tout bon (grep -i err sur le log, idem pour warn), mais on ne peut pas créer de paquet, 
# faut faire à la main (en root)
mkdir /usr/local/include/CoCoA
cp -R include/CoCoA/* /usr/local/include/CoCoA
cp lib/libcocoa.a /usr/local/lib
ranlib /usr/local/lib/libcocoa.a

# Et comme ensuite la compil de giac plante, on défait
rm -rf /usr/local/include/CoCoA
rm -f /usr/local/lib/libcocoa.a
```

memo patches sur giac 0.8.6 pour voir si ça passe avec CoCoA

```bash
sed -i -e '81 s#return CoCoA::ZZ(\*g\._ZINTptr);#return CoCoA::ZZ(CoCoA::CopyFromMPZ,\*g\._ZINTptr);#' ./src/cocoa.cc
sed -i -e 's/NewDenseMatrix/NewDenseMat/' ./src/TmpFGLM.C
# on ajoute le bak oublié avant (même pas peur mais ça mange pas de pain)
sed -i.bak -e 's/NewDenseMatrix/NewDenseMat/' ./src/TmpLESystemSolver.C
sed -i.bak '/#include "CoCoA\/ring.H"/a\
#include "CoCoA/error.H"' ./src/TmpLESystemSolver.C
./configure 2>&1 | tee configure.log
make 2>&1 | tee make.log
# et en root
checkinstall 2>&1 |tee checkinstall.avec-CoCoA.log
```
