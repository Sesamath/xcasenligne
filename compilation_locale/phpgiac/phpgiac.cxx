// -*- mode:C++ ; compile-command: "g++ phpgiac.cxx -o test  -lgiac -lgmp" -*-
#include "phpgiac.h"
#include <iostream>
#include <sstream>
#include<map>
#include <giac/giac.h>
#include <giac/plot.h>
#include <giac/input_lexer.h>

#include <giac/first.h>

#include <giac/gen.h>
#include <giac/symbolic.h>
#include <giac/usual.h>
#include <giac/intg.h>
#include <giac/derive.h>
#include <giac/series.h>

#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <iomanip>
#include <math.h>



using namespace std;
using namespace giac;


std::map<std::string,giac::context *> session_cptr;
string lemarqueur="`";

int taille_map(){
  return session_cptr.size();
}


context * initialise_cptr(std::string s_ctx){
  setenv("XCAS_HELP","/usr/local/share/giac/doc/fr/keywords",1);
  giac::add_language(1);
  context * cptr=session_cptr[s_ctx];
  if (!cptr){
    cptr=new context;
    session_cptr[s_ctx]=cptr;
  } 
  return cptr;
}

int giac_lang(int lang, std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  giac::set_language(lang, cptr);
  return lang;
}

std::string giac_eval_txt(std::string command, std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  return g.print(cptr);
}

std::string giac_eval_prog(std::string command, std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  string message="";
  if (first_error_line(cptr)!=0) {
    std::ostringstream out;
    out<<first_error_line(cptr);
    message=out.str()+
      lemarqueur+
      error_token_name(cptr);
  }
  return message;
}


std::string capture;
std::string capture_begin="<mtable><mtr><mtd><mtext>Impression : </mtext></mtd></mtr>";
std::string capture_end="<mtr><mtd><mtext>Valeur de retour :</mtext></mtd></mtr><mtr><mtd>";
std::string table_end="</mtd></mtr></mtable>";

giac::gen capture_print(const giac::gen & g,GIAC_CONTEXT){
  if (g.is_symb_of_sommet(at_print))
    capture+="<mtr><mtd>"+gen2mathml(g._SYMBptr->feuille,contextptr)+"</mtd></mtr>";
  return g;
}

std::string giac_eval_mathml(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  g=eval(g,cptr);
  if (capture!="")
    return capture_begin+
      capture+
      capture_end+
      gen2mathml(g,cptr)+
      table_end;
  return gen2mathml(g,cptr);
}



std::string giac_txt_mathml_et_eval(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  gen h=eval(g,cptr);
  if (capture!="")
    return   h.print(cptr)+
      lemarqueur+
      gen2mathml(g,cptr)+
      "<mo>=</mo>"+
      capture_begin+
      capture+
      capture_end+
      gen2mathml(h,cptr)+
      table_end;
  return  h.print(cptr)+lemarqueur+gen2mathml(g,cptr)+"<mo>=</mo>"+gen2mathml(h,cptr);
}


std::string giac_txt_mathml(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  //g=eval(g);
  if (capture!="")
    return g.print(cptr)+
      lemarqueur+
      capture_begin+
      capture+
      capture_end+
      gen2mathml(g,cptr)+
      table_end;
  return g.print(cptr)+lemarqueur+gen2mathml(g,cptr);
}


std::string giac_eval_txt_mathml(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  capture="";
  __interactive.op=&capture_print;
  gen g(command,cptr);
  g=eval(g,cptr);
  if (capture!="")
    return g.print(cptr)+lemarqueur+capture_begin+capture+capture_end+gen2mathml(g,cptr)+table_end;
  return g.print(cptr)+lemarqueur+gen2mathml(g,cptr);
}




std::string giac_suite(std::string nomSortie,std::string nomRec, std::string u0Rec, std::string fRec,std::string nomRec2, std::string u0Rec2, std::string fRec2, std::string nomEx, std::string fEx, int n,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen ini(nomRec+":="+u0Rec,cptr);
  ini=eval(ini,cptr);
  gen rec(nomRec+":="+fRec,cptr);
  gen ini2(nomRec2+":="+u0Rec2,cptr);
  ini2=eval(ini2,cptr);
  gen rec2(nomRec2+":="+fRec2,cptr);
  gen ex(nomEx+":="+fEx,cptr);
  gen x(nomRec,cptr);
  gen y(nomEx,cptr);
  gen z(nomSortie,cptr);
  vecteur v; 
  if (x.type==_VECT)
    v=*x._VECTptr;
  else
    v.push_back(x);
  vecteur w; 
  if (y.type==_VECT)
    w=*y._VECTptr;
  else
    w.push_back(y);
  vecteur vw; 
  if (z.type==_VECT)
    vw=*z._VECTptr;
  else
    vw.push_back(z);
  int i,j;
  std::string sortie="";
  for (i=0 ; i<=n ; i++){
    // evaluation de la suite explicite
    evalf(ex,1,cptr);
    for (j=0;j<vw.size();j++){
      sortie+=eval(vw[j],cptr).print(cptr);
      if (j<vw.size()-1)
        sortie+=",";
    }
    sortie+=lemarqueur; 
    // evaluation de la suite recurrente1
    // suite recurrente 2
    eval(rec2,cptr);
    if (i<n)
      evalf(rec,1,cptr);
  }
  return sortie;
}



//capture svg



// nouvelle version
string sSVG;
gen s_svg_capture(const gen &g,GIAC_CONTEXT){
  if (g.is_symb_of_sommet(at_print)){
    capture+="<mtr><mtd>"+gen2mathml(g._SYMBptr->feuille,contextptr)+"</mtd></mtr>";
    return g;
  }
 if (g.type== _SYMB || g.type== _VECT) 
   sSVG+=gen2svg(g,contextptr);
 return g;
}


std::string giac_eval_svg(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
 //recup des sorties graphiques
  __interactive.op=&s_svg_capture;
  gen g(command,cptr);
  g=eval(g,cptr);
// test pour la  3d : pas au point 
  if (g.is_symb_of_sommet(at_pnt) && g.subtype>=0){ //3d  
  } 
  else {   
    return sSVG+lemarqueur+svg_legend;
  }
}



std::string giac_eval_txt_mathml_svg(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
 //recup des sorties graphiques
  __interactive.op=&s_svg_capture;
  gen g(command,cptr);
  g=eval(g,cptr);
// test pour la  3d : pas au point 
  if (g.is_symb_of_sommet(at_pnt) && g.subtype>=0){ //3d
  } 
  else {
    return g.print(cptr)+
      lemarqueur+
      capture_begin+
      lemarqueur+
      capture+
      lemarqueur+
      capture_end+
      lemarqueur+
      gen2mathml(g,cptr)+
      lemarqueur+
      table_end+
      lemarqueur+
      sSVG+  
      lemarqueur+
      svg_legend; 
  }
}



std::string giac_svg_preamble(double width_cm, double height_cm){
  return svg_preamble(width_cm,height_cm);
}

std::string giac_svg_grid(){
  return svg_grid();
}

std::string spread2cep(const matrice & m,GIAC_CONTEXT){
  std::string s="";
  int l=m.size();
  if (!l)
    return s;
  int c=m.front()._VECTptr->size();
  for (int i=0;i<l;++i){
    for (int j=0;j<c;++j){
      s+=m[i][j][1].print(contextptr)+lemarqueur+m[i][j][2].print(contextptr)+lemarqueur;
    }
  }
  return s;
}   

// interface pour CasEnPoche -*- mode:C++ ; compile-command: "g++ -I.. -g -c phpgiac.cxx" -*-
std::string giac_eval_cep(std::string command,std::string s_ctx){ 
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  if (g.type==_VECT && g.subtype==_SPREAD__VECT)
    return spread2cep(*g._VECTptr,cptr);
  else
    return g.print(cptr);
}

std::string spread2giacOnLine(const matrice & m,GIAC_CONTEXT){
  std::string s="";
  int l=m.size();
  if (!l)
    return s;
  int c=m.front()._VECTptr->size();
  for (int i=0;i<l;++i){
    for (int j=0;j<c;++j){
      s+=m[i][j][1].print(contextptr)+lemarqueur+gen2mathml(m[i][j][1],contextptr)+lemarqueur;
    }
  }
  return s;
}   


// interface pour le tableur de Xcas en ligne
std::string giac_eval_spread(std::string command,std::string s_ctx){ 
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  if (g.type==_VECT && g.subtype==_SPREAD__VECT)
    return spread2giacOnLine(*g._VECTptr,cptr);
  else
    return g.print(cptr);
}

std::string evalfSpread2giacOnLine(const matrice & m,GIAC_CONTEXT){
  std::string s="";
  int l=m.size();
  gen g;
  if (!l)
    return s;
  int c=m.front()._VECTptr->size();
  for (int i=0;i<l;++i){
    for (int j=0;j<c;++j){
      g=evalf(m[i][j][1],1,contextptr);
      s+=g.print(contextptr)+lemarqueur+gen2mathml(g,contextptr)+lemarqueur;
    }
  }
  return s;
}   

std::string giac_evalf_spread(std::string command,std::string s_ctx){ 
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  gen g(command,cptr);
  g=eval(g,cptr);
  if (g.type==_VECT && g.subtype==_SPREAD__VECT)
    return evalfSpread2giacOnLine(*g._VECTptr,cptr);
  else
    return g.print(cptr);
}



std::string giac_archive_session(std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  std::string s=archive_session(0,cptr);
  //elimine_char_null(s);
  return s; 
}

void giac_unarchive_session(std::string arch,std::string s_ctx){
  giac::child_id=1;
  sSVG="";
  // variable de mathml.cc de giac
  svg_legend="";
  capture="";
  context * cptr=initialise_cptr(s_ctx);
  std::string s_arch(arch);
  gen g("1",cptr);
  unarchive_session_string(s_arch,-1,g,cptr);
  return;
}


// sortie pour labomep
// ne recupere pas l'impression

std::string vect2labomep(const vecteur & v, GIAC_CONTEXT){
  string s("");
  vecteur::const_iterator it=v.begin(),itend=v.end();
  for (;it!=itend;){
    s += lemarqueur+gen2mathml(*it,contextptr);  
    ++it;   
  }
  return s;
}

std::string giac_labomep(std::string command,std::string s_ctx){
  giac::child_id=1;
  context * cptr=initialise_cptr(s_ctx);
  //recup des sorties graphiques
  __interactive.op=&s_svg_capture;
  gen g(command,cptr);
  g=eval(g,cptr);
  string s=g.print(cptr);
  if (g.type==_VECT)
    s+=vect2labomep(*g._VECTptr, cptr);  
  else
    s+=lemarqueur+gen2mathml(g,cptr);
  s+=lemarqueur+"SVG"+sSVG;
  s+=lemarqueur+svg_legend;
  return s;
}



// decommenter pour compil directe

 main(){
cout<<giac_labomep("f(a):={;print(x);A:=point(0);B:=point(1);};","z")<<endl;
 cout<<"----"<<endl<<giac_labomep("f(1);","z")<<endl;

}






