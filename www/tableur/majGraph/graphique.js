//   Copyright (C) 2009 KEOPS (E.Ostenne) - emmanuel.ostenne@sesamath.net
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

/*keops
- voir synchro courbe/valeurs tableur
- filtrage MSIE version pour setVerticalTitle -> JS + CSS
*/

//var graphique_JSX_UniqueIndex = 0; cr��e dynamiquement

//construit le popup (mobile) et l'instance du graphique 
//retourne cette instance
function graphiqueFenetre(left,top,width,height,mobile,movegraph) {
      var p = new graphiquePopUp(left,top,width,height,mobile,movegraph);  
      var g = new graphique(p);  
	  return g;
}

//l'objet qui pilote le gaphique : 
// - popup pour l'origanisation : titre, dessin, l�gende
// - dessin : repr�sentation des donn�es
graphique = function(id) {
	//variable globale dynamique
	if(window.graphique_JSX_UniqueIndex==undefined) {
		window["graphique_JSX_UniqueIndex"]=1;
	} else {
		window.graphique_JSX_UniqueIndex++; 
	}
	//unicit� du dom.contenu.id du graphique pour le board JSX + titrage par d�faut
	this.graphiqueIndex=window.graphique_JSX_UniqueIndex;
	//
	if (id.charAt) id=document.getElementById(id);
	this.dom = id;
	//ZIndex pour assurer l'ordre des textes JSXGraph dans les popup
	this.dom.setZIndex(this.graphiqueIndex);
	//s'assurer que this.board sera supprim� de la liste de JSX
	this.dom.onSupprime=this.destroy;
	//titres par d�faut
	this.dom.titre.innerHTML = "Graphique"+this.graphiqueIndex; 
	this.dom.xaxislegende.innerHTML = "";
	this.dom.verticallegende.innerHTML= "";
	this.dom.legende.innerHTML= ""; 
	//this.dom.contenu.className="jxgbox";
	//div contenu : 25 provient du CSS pour h3, 20 provient du vi xaxislegende
	this.dom.contenu.style.height = (parseInt(this.dom.div.style.height,10)-25-20-15)+"px"; /*keops � revoir pour 25*/
	this.dom.contenu.id="graphique"+this.graphiqueIndex;
	this.dom.contenu.style.width=(parseInt(this.dom.div.style.width,10)-30-30)+"px"; /* marges de 30 droite gauche*/
	//options jsxgraph pour le rendu du grpahique
	this.jsxBoardStyle = {axis:true, grid:false, showCopyright:false, showNavigation:false};
	this.jsxStyle={};
	this.pieColors = ['#B02B2C','#3F4C6B','#C79810','#D15600','#FFFF88','#C3D9FF','#4096EE','#008C00'];
	//board par d�faut (d�truit aussit�t si dessin)
	var attributes =  this.jsxBoardStyle;
	attributes["boundingbox"]=[-1, 9, 13, -3];
	this.board = JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);    
	//
	//this.dom.legende.innerHTML="L�gende:<br><font color='red'>&diams;</font>&nbsp;legende<br<font color='red'>&diams;</font>&nbsp;legende <font color='red'>&diams;</font>&nbsp;legende <font color='red'>&diams;</font>&nbsp;legende ";
}

//propri�t�s

graphique.prototype.destroy=function() {
	if (this.board == undefined) return;
	JXG.JSXGraph.freeBoard(this.board);
}

graphique.prototype.setStyle = function(style) {
	this.jsxStyle = style;
}

graphique.prototype.setTitle = function(titre) {
  	this.dom.titre.innerHTML = titre; 
}

graphique.prototype.setBoardStyle = function(style) {
 	this.jsxBoardStyle = {axis:true, grid:false, showCopyright:false, showNavigation:false};	
	for(s in style) {
		this.jsxBoardStyle[s]=style[s];
	}
}

graphique.prototype.setLegend = function(liste) {
	//liste = {titre1:"",color1:"",titre2:"",color2:""}
	if(liste) {
		this.dom.legende.style.width="100px";
		this.dom.contenu.style.width=(parseInt(this.dom.div.style.width,10)-30-100)+"px"; /* marge 30 droite marge 100 gauche : l�gende*/
		var s= ["ok",""];
		for(var i=1;s[0]!=="";i++) {
			s[i*2]=(liste["color"+i])?"<font color="+liste["color"+i]+">&#9632;</font>&nbsp;":"";
			s[i*2+1]=(liste["title"+i])?liste["title"+i]+"<br>":"";
			s[0]=s[i*2];
		}
		s[0]="";
		this.dom.legende.innerHTML="<p><u>L&eacute;gende:</u><br>"+s.join("")+"</p>";
	} else {
		this.dom.legende.style.width="30px";
		this.dom.contenu.style.width=(parseInt(this.dom.div.style.width,10)-30-30)+"px"; /* marges de 30 droite gauche*/
		this.dom.legende.innerHTML="";
	}
}

graphique.prototype.setHorizontalTitle = function(texte) {
	this.dom.xaxislegende.innerHTML = texte;
}

graphique.prototype.setVerticalTitle = function(texte) {
	this.dom.verticallegende.innerHTML = texte;
	/*console.log("haut:",this.dom.verticallegende.offsetWidth,"-",this.dom.verticallegende.offsetHeight)
	console.log("AppName:",navigator.appName," codeName:",navigator.appCodeName," version:",navigator.appVersion );*/
	
	/* Si Internet Explorer avant version 9 : ajouter ce test + CSS : activer filter ou/et -ms-filter -> filtrage � faire !
	/*if(navigator.appName=="Microsoft Internet Explorer") {
		this.dom.verticallegende.style.top=(25+30+Math.round(this.dom.verticallegende.offsetHeight/2))+"px"; 
	} else {
	*/
	this.dom.verticallegende.style.top=(25+30+Math.round((parseInt(this.dom.contenu.style.height,10)+this.dom.verticallegende.offsetWidth)/2))+"px"; 
}

graphique.prototype.setPieColors = function(liste)  {
	//
	if(liste) {
		this.pieColors = liste;
	} else {
		this.pieColors = ['#B02B2C','#3F4C6B','#C79810','#D15600','#FFFF88','#C3D9FF','#4096EE','#008C00'];
	}
}
		
//m�thodes

graphique.prototype.completeListeNumerotee = function(liste,prefixe,indexdebut,indexfin,tableau) {
	//pour faire {title1:valeur1,title2:valeur2 ...}
	//avec indexdebut=1 ! tableau = [valeur1,valeur2 ...]
	// ex h.completeListeNumerotee(lgnd,"title",1,4,["A","B","C","D"]);
	for(var i=indexdebut;i<=indexfin;i++) {
		liste[""+prefixe+i]=tableau[i-indexdebut];
	}	
}
//calcul automatique des valeurs min et max pour les x et les diff�rents y
graphique.prototype.prepMinMax=function(values) {

	function prepYMinMax() {
		var vyMin = 0;
		var vyMax = 0;
		var v = 0;
		for(var j=1;j<values.length;j++) {
		  for(var i=0;i<values[j].length;i++) {
			  v=values[j][i];
			  if(v<vyMin) vyMin=v;
			  if(v>vyMax) vyMax=v;
		  }
		}
		//marge droite et gauche
		v=(vyMax-vyMin)/10;
		//
		return 	[vyMin-v,vyMax+v];
	}
	
	function prepXMinMax() {
		var vxMin = 0;
		var vxMax = 0;
		var v = 0;
			for(var i=0;i<values[0].length;i++) {
				v=values[0][i];
				if(v<vxMin) vxMin=v;
				if(v>vxMax) vxMax=v;
			}
			//marge droite et gauche
			v=(vxMax-vxMin)/10;
		//
		return 	[vxMin-v,vxMax+v];	
	}
	
	var y=prepYMinMax();
	var x=prepXMinMax();
	return [x[0],x[1],y[0],y[1]]
}

//ligne bris�e
// + points sommets
graphique.prototype.courbe=function(xMin,xMax,values) {
//  si xMin=xMax alors auto calcul� !	
//  values: tableau de tableaux de valeurs : [0]:x, [1]:y1 [2]:y2 ...
	if (this.board == undefined) return;
	JXG.JSXGraph.freeBoard(this.board);
	//
	var minMax=this.prepMinMax(values);
	//si xMin<>xMax alors prioritaire sur auto calc
	if(xMin!==xMax) {
		minMax[0]=xMin;
		minMax[1]=xMax;
	}
	//zone de dessin
    var attributes =  this.jsxBoardStyle;
	attributes["boundingbox"]=[minMax[0], minMax[3], minMax[1], minMax[2]]
	this.board=JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);  
	this.board.suspendUpdate(); 
	//options g�n�rales
	this.jsxStyle.chartStyle='line';
	if(this.jsxStyle["spline"]) this.jsxStyle.chartStyle='spline';
	if(this.jsxStyle["points"]) this.jsxStyle.chartStyle+=',point';
	//rendu
	var jStyle={};
	for(var j=1;j<values.length;j++) {
		dataArr=[values[0],values[j]];
		jStyle=this.jsxStyle;
		if(this.jsxStyle["color"+j]) jStyle.strokeColor=this.jsxStyle["color"+j];
		jStyle.labels=dataArr;
		this.board.create('chart', dataArr, jStyle);
	}
	this.board.unsuspendUpdate();
}

//barres l'une derri�res l'autre
graphique.prototype.barres=function(xMin,xMax,values) {
//  si xMin=xMax alors auto calcul� !	
//  values: tableau de tableaux de valeurs : [0]:x, [1]:y1 [2]:y2 ...
	if (this.board == undefined) return;
	JXG.JSXGraph.freeBoard(this.board);
	//
	var minMax=this.prepMinMax(values);
	//si xMin<>xMax alors prioritaire sur auto calc
	if(xMin!==xMax) {
		minMax[0]=xMin;
		minMax[1]=xMax;
	}
	//zone de dessin
    var attributes =  this.jsxBoardStyle;
	attributes["boundingbox"]=[minMax[0], minMax[3], minMax[1], minMax[2]]
	this.board=JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);  
	this.board.suspendUpdate(); 
	//options g�n�rales
	this.jsxStyle.chartStyle='bar';
	this.jsxStyle.width=0.8;
	this.jsxStyle.color='olive';	
	//rendu
	var jStyle={};
	for(var j=1;j<values.length;j++) {
		dataArr=[values[0],values[j]];
		jStyle=this.jsxStyle;
		if(this.jsxStyle["color"+j]) jStyle.strokeColor=this.jsxStyle["color"+j];
		jStyle.labels=dataArr[1];
		this.board.create('chart', dataArr, jStyle);
	}
	this.board.unsuspendUpdate();
}

//points (x,y), 
// + reli�s par segment
graphique.prototype.nuage=function(xMin,xMax,values) {
//  si xMin=xMax alors auto calcul� !	
//  values: tableau de tableaux de valeurs : [0]:x, [1]:y1 [2]:y2 ...
	if (this.board == undefined) return;
	JXG.JSXGraph.freeBoard(this.board);
	//
	var minMax=this.prepMinMax(values);
	//si xMin<>xMax alors prioritaire sur auto calc
	if(xMin!==xMax) {
		minMax[0]=xMin;
		minMax[1]=xMax;
	}
	//zone de dessin
    var attributes =  this.jsxBoardStyle;
	attributes["boundingbox"]=[minMax[0], minMax[3], minMax[1], minMax[2]]
	this.board=JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);  
	this.board.suspendUpdate(); 
	//options g�n�rales
	this.jsxStyle.chartStyle='point';
	if(this.jsxStyle["line"]) this.jsxStyle.chartStyle+=',line';
	//if(this.jsxStyle["spline"]) this.jsxStyle.chartStyle+=',spline';
	//rendu
	var jStyle;
	for(var j=1;j<values.length;j++) {
		dataArr=[values[0],values[j]];
		jStyle=this.jsxStyle;
		if(this.jsxStyle["color"+j]) jStyle.strokeColor=this.jsxStyle["color"+j];
		jStyle.labels=dataArr;
		this.board.create('chart', dataArr, jStyle);
	}
	this.board.unsuspendUpdate();
}

//ligne bris�e + aire dessous
// + points sommets
graphique.prototype.aire=function(xMin,xMax,values) {
//  si xMin=xMax alors auto calcul� !	
//  values: tableau de tableaux de valeurs : [0]:x, [1]:y1 [2]:y2 ...
	if (this.board == undefined) return;
	JXG.JSXGraph.freeBoard(this.board);
	//
	var minMax=this.prepMinMax(values);
	//si xMin<>xMax alors prioritaire sur auto calc
	if(xMin!==xMax) {
		minMax[0]=xMin;
		minMax[1]=xMax;
	}
	//zone de dessin
    var attributes =  this.jsxBoardStyle;
	attributes["boundingbox"]=[minMax[0], minMax[3], minMax[1], minMax[2]]
	this.board=JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);  
	//console.log("JXG deb:",JXG.Options.layer);
	/*
	console.log("JXG OB:",JXG.Options.layer);
	var jsxol=JXG.Options.layer;	
	for(va in jsxol) {
		jsxol[""+va]=0;
	};
	JXG.Options.layer.text=0;
	JXG.Options.layer.numlayers=20;
	*/
	this.board.suspendUpdate(); 
	//options g�n�rales
	//rendu	
	var jStyle,cjStyle;
	var polyStyle;
	var couleur;
	for(var j=1;j<values.length;j++) {
		var p=[];
		//style		
		jStyle=this.jsxStyle;
		jStyle["fixed"]=true;
		jStyle["name"]="";
		if (this.jsxStyle["points"]) jStyle["visible"]=this.jsxStyle["points"];
		polyStyle={withLines:false};
		if(this.jsxStyle["color"+j]) {
			couleur=this.jsxStyle["color"+j];
			jStyle.strokeColor=couleur;
			jStyle.fillColor=couleur;			
			polyStyle.fillColor=couleur;
		} else couleur="blue";
		//
		p.push(this.board.create('point',[values[0][0],0],{visible:false,name:"",fixed:true}));
		for(var i=0; i<values[0].length;i++) {
			p.push(this.board.create('point',[values[0][i],values[j][i]],jStyle));
		}
		p.push(this.board.create('point',[values[0][values[0].length-1],0],{visible:false,name:"",fixed:true}));
		//polygone pour l'aire
		this.board.create('polygon', p, polyStyle);
		//ligne
		cjStyle={visible:true};
		cjStyle.strokeColor=couleur;
		cjStyle.strokeWidth=jStyle.strokeWidth;
		this.board.create('curve',[values[0],values[j]],cjStyle);
	}
	this.board.unsuspendUpdate();
}

//barres l'une � c�t� de l'autre
graphique.prototype.barresCote=function(xMin,xMax,values) {
//  si xMin=xMax alors auto calcul� !	
//  values: tableau de tableaux de valeurs : [0]:x, [1]:y1 [2]:y2 ...
	if (this.board == undefined) return;
	JXG.JSXGraph.freeBoard(this.board);
	//
	var minMax=this.prepMinMax(values);
	//si xMin<>xMax alors prioritaire sur auto calc
	if(xMin!==xMax) {
		minMax[0]=xMin;
		minMax[1]=xMax;
	}	
	//zone de dessin
    var attributes =  this.jsxBoardStyle;
	attributes["boundingbox"]=[minMax[0], minMax[3], minMax[1], minMax[2]]
	this.board=JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);  
	this.board.suspendUpdate(); 
	//options g�n�rales
	this.jsxStyle.chartStyle='bar';
	this.jsxStyle.color='olive';	
	//rendu
	var jStyle={};
	var x1,x2,y1;
	var pas1 = Math.round(10/values.length)/10;
	var pas2=-0.5*(pas1*(values.length-1));
	for(var i=0; i<values[0].length;i++) {
		values[0][i]+=pas2;
	}
	for(var j=1;j<values.length;j++) {
		jStyle=this.jsxStyle;
		if(jStyle["color"+j]) {
			jStyle.color=jStyle["color"+j];
		}
		jStyle.withLines=false;
		jStyle.strokeColor=jStyle.color;
		jStyle.fillColor=jStyle.strokeColor;
		jStyle.fillOpacity=0.8;
		jStyle["lines"]={strokeColor:jStyle.strokeColor};
		jStyle.highlightFillColor=jStyle.strokeColor;
		jStyle.highlightFillOpacity=0.6;
		//jStyle.labels=dataArr[1];
		/*
		var pas2 = pas1/2;
		for(var i=0; i<values[0].length;i++) {
			values[0][i]+=pas2;
		}
		*/
		for(var i=0; i<values[0].length;i++) {
			x1=values[0][i];
			x2=x1+pas1;
			y1=values[j][i];
			pv=new Array([x1,0], [x1,y1], [x2,y1], [x2,0]);
			var p = new Array();
			for(var k=0;k<4;k++) {
				p.push(this.board.create('point',pv[k],{visible:false}));
			}			
			/*var pol = this.board.create('polygon',sq,{lines:{strokeColor:h["color"],strokeWidth:h["strokeWidth"]},fillOpacity:0,highlightFillOpacity:0});*/
			this.board.create('polygon', p, jStyle);
			values[0][i]+=pas1;
		}
	}
	this.board.unsuspendUpdate();
}


//barres l'une � c�t� de l'autre
graphique.prototype.camembert=function(xMin,xMax,values) {
//  si xMin=xMax alors auto calcul� !	
//  values: tableau de tableaux de valeurs : [0]:x, [1]:y1 [2]:y2 ...
	if (this.board == undefined) return;
	JXG.JSXGraph.freeBoard(this.board);
	//
	var minMax=this.prepMinMax(values);
	//si xMin<>xMax alors prioritaire sur auto calc
	if(xMin!==xMax) {
		minMax[0]=xMin;
		minMax[1]=xMax;
	}	
	//zone de dessin
    var attributes = this.jsxBoardStyle;
	attributes.axis=false;
	attributes["keepaspectratio"]=true;
	attributes["originX"]=0;
	attributes["originY"]=0;
	attributes["unitX"]=50, 
	attributes["unitY"]=50;
	//
	var myHighlightColors = [];
	var v;
	for(var i=0;i<this.pieColors.length;i++) {
		v=JXG.rgb2hsv(this.pieColors[i]);
		v[2]=Math.min(v[2]+0.2,1);
		myHighlightColors[i]=JXG.hsv2rgb(v[0],v[1],v[2]);
	}
	//
	this.board=JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);  
	this.board.suspendUpdate(); 
	//options g�n�rales
	this.jsxStyle.chartStyle='pie';
	this.jsxStyle.color='blue';	
	var dataArr=values[1];
	var jStyle=this.jsxStyle;
	this.board.createElement('chart', dataArr, 
        {chartStyle:'pie',  
         fillOpacity:1, 
		 center:[(attributes.boundingbox[0]+attributes.boundingbox[2])/2,(attributes.boundingbox[1]+attributes.boundingbox[3])/2], 
		 radius: Math.min(attributes.boundingbox[0]+attributes.boundingbox[2],attributes.boundingbox[1]+attributes.boundingbox[3])*0.8,
		 strokeColor:"white",
		 colors: this.pieColors,
		 highlightStrokeColor:'white',
		 highlightcolors: myHighlightColors,
		 highlightFillOpacity:1,
         labels:values[1],
         highlightOnSector:true,
     });	
	this.board.unsuspendUpdate();
}

//histogramme (aire proport.)
graphique.prototype.histogramme=function(xMin,xMax,values) {
//  values: tableau de tableaux de valeurs : [0]:x, [1]:y1 [2]:y2 ...
//  le dernier y n'est pas pris en compte car on travaille avec des intervalles ouverts � droite : 
//  classes : [x0,x1[ [x1,x2[ ... [xn-1,xn[
//  valeurs :  y0      y1          yn-1
	if (this.board == undefined) return;
	//if(document.getElementsByName("myinfobox")) {

		this.myinfobox=document.createElement("myinfobox");
		this.myinfobox.id="myinfobox";
		this.myinfobox.setAttribute("class","myinfobox");
		document.body.appendChild(this.myinfobox);
	//}
	JXG.JSXGraph.freeBoard(this.board);
	// hauteur des barres r�duite !
	var nvalues = new Array(values.length);
	nvalues[0]=new Array(values[0].length);
	for(var j=1;j<values.length;j++) {
		var x1 =  values[0][0];
		nvalues[0][0]=x1;
		var x2 =  0;
		nvalues[j]=new Array(values[j].length);
		for(var i=0; i<values[0].length-1;i++) {
			x2=values[0][i+1];
			nvalues[j][i]=values[j][i]/(x2-x1);
			nvalues[0][i+1]=x2;
			x1=x2;
		}
		nvalues[j][values[0].length-1]=nvalues[j][0];
	}
	var minMax=this.prepMinMax(nvalues);
	//si xMin<>xMax alors prioritaire sur auto calc
	if(xMin!==xMax) {
		minMax[0]=xMin;
		minMax[1]=xMax;
	}	
	//zone de dessin
    var attributes =  this.jsxBoardStyle;
	attributes["boundingbox"]=[minMax[0], minMax[3], minMax[1], minMax[2]]
	this.board=JXG.JSXGraph.initBoard(this.dom.contenu.id, attributes);  
	this.board.suspendUpdate(); 
	//options g�n�rales
	this.jsxStyle.chartStyle='bar';
	this.jsxStyle.color='blue';	
	//rendu
	var jStyle={};
	var r;
	for(var j=1;j<nvalues.length;j++) {
		jStyle=this.jsxStyle;
		if(jStyle["color"+j]) {
			jStyle.color=jStyle["color"+j];
		}
		jStyle.withLines=false;
		jStyle.strokeColor=jStyle.color;
		jStyle.fillColor=jStyle.strokeColor;
		jStyle.fillOpacity=0.8;
		jStyle["lines"]={strokeColor:jStyle.strokeColor};
		jStyle.highlightFillColor=jStyle.strokeColor;
		jStyle.highlightFillOpacity=0.6;
		//jStyle.labels=values[j];
		x1 = nvalues[0][0];
		x2 = 0;
		for(var i=0; i<nvalues[0].length-1;i++) {
			x2=nvalues[0][i+1];
			y1=nvalues[j][i];
			pv=new Array([x1,0], [x1,y1], [x2,y1], [x2,0]);
			var p = new Array();
			for(var k=0;k<4;k++) {
				p.push(this.board.create('point',pv[k],{visible:false}));
			}			
			r=this.board.create('polygon', p, jStyle);
			r.infoText = "["+x1+";"+x2+"[:"+values[j][i];
			x1=x2;
			JXG.addEvent(r.rendNode, 'mouseover', 
            	(function(g,d){ return function(){ 
										var w = document.getElementById("myinfobox");
										var c = g.vertices[1].coords.scrCoords;
										w.innerHTML=g.infoText;
										w.style.left = (c[1]+d.left+g.board.containerObj.offsetLeft+2)+'px';
										w.style.top =  (c[2]+d.top+g.board.containerObj.offsetTop-18+0)+'px';
										w.style.display='block';
										g.highlight(); 
									}; 
							})(r,this.dom)
				, r);		
			JXG.addEvent(r.rendNode, 'mouseout', 
            	(function(g){ return function(){ 
										document.getElementById("myinfobox").style.display='none';
										g.noHighlight(); 
									}; 
							})(r)
				, r);		
		}
	}
	this.board.unsuspendUpdate();
}

