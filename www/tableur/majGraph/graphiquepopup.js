// JavaScript Document extrait de base.js :
//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


/* KEOPS
- modif graphiquepopup.css : couleur 
- ajout support height en option sur graphiquePopUp()
- supprime : h�ritage par this.onSupprime()
- renommage mobile en seDeplace
- ajout mobile pour permettre ou pas le d�placement du PopUp
*/

function graphiqueInclureStyle(fileName){
  var link= document.createElement("link");
  link.type = "text/css"; 
  link.href =fileName;
  link.rel="stylesheet";
  var head=document.getElementsByTagName("head");
  if (head[0])
    head[0].appendChild(link);
  return;
}

function graphiquePopUp(left,top,width,height,mobile,movegraph){
/* 
   mobile  :  true|false : si on peut d�placer le popup
   movegraphe : true|false : si on bouge en montrant le graphique ou en le masquant
   
*/
  this.seDeplace=false;
  if(mobile!==undefined) {
	  this.mobile=mobile;
  } else this.mobile=true;
  if(movegraph!==undefined) {
	  this.moveWithGraph=movegraph;
  } else this.moveWithGraph=true;
  this.x0=0;
  this.y0=0;
  this.left=left;
  this.top=top;
  graphiqueInclureStyle("graphiquepopup.css");
  graphiqueInclureStyle("graphique/graphiquepopup.css");
  //div principal
  this.div=document.createElement("div");
  this.div.setAttribute("class","popup");
  this.div.style.top=top+"px";
  this.div.style.left=left+"px";
  if (width)
    this.div.style.width=width+"px";
  if (height)
    this.div.style.height=height+"px";
  document.body.appendChild(this.div);
  //Top Titre
  this.titre=document.createElement("h3");
  this.titre.js=this;
  //this.titre.onmousedown=function(event){this.js.mouseDown(event)};
  this.titre.onmousedown=function(event){this.js.graphiqueMouseDown(event);}; 
  this.div.appendChild(this.titre);
  //Conteneur du graph avec l�gende et titres
  this.nograph=document.createElement("div");
  this.nograph.setAttribute("class","nograph");
  this.nograph.innerHTML='<center>&nbsp;<br>&nbsp;<br><font size="-1" color="darkgray">Affichage d&eacute;sactiv&eacute; pendant le d&eacute;placement.</font></center>';
  this.nograph.style.display="none";
  this.nograph.js=this;
  this.div.appendChild(this.nograph);
  this.graph=document.createElement("div");
  this.graph.setAttribute("class","graph");
  this.graph.js=this;
  this.div.appendChild(this.graph);
  //Right l�gende yaxis
  this.yaxislegende=document.createElement("div");
  this.yaxislegende.setAttribute("class","yaxislegende");
  this.yaxislegende.js=this;
  this.graph.appendChild(this.yaxislegende);
  //Left legende g�n�rale
  this.legende=document.createElement("div");
  this.legende.setAttribute("class","legende");
  this.legende.js=this;
  this.graph.appendChild(this.legende);
  //Center container jsxgraph
  this.contenu=document.createElement("div");
  this.contenu.setAttribute("class","contenu");
  this.contenu.js=this;
  this.graph.appendChild(this.contenu);
  //Bottom l�gende xaxis
  this.xaxislegende=document.createElement("div");
  this.xaxislegende.setAttribute("class","xaxislegende");
  this.xaxislegende.js=this;
  this.graph.appendChild(this.xaxislegende);
  //Top Image close
  var img=document.createElement("img");
  img.setAttribute("class","fermer");
  img.setAttribute("src","data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH1gQFFCUoDIRw0QAAAaJJREFUOMulk0FLG0EYhp9ZV9zpKnFbgzSbo80PEA8RLbSXQv5DjulRf0x7bI/5Dx5bQSh4UrxIPJq0DdsQ4kLWZZP5euhszBpzqQMDwzvzvXzf+74Dz1zqMdCGEDgCGkDNwh3gBPjchN5Sgja0gE/1MNQlrREAEYwx3N0nnP/6nQDHTfi6QNCG1ivP+7JXrfIniojjmIkxCLDiOKz7Pi+DgMt+n0GafsxJ1FzbNx92dvTP21uSNEWgsAHWVld5vb3NabebAG+a0HPs3VG9UtGDKCJJU+oi7IvMCg9FOBQhyTIGwyF75bK2OpETNDa05i6OZ0UAByIcyANigNF4jHZdrMgzgpoSYWpnPlML5vBdKQSYymyoGoCbPxARTH5+wu+CJuYBzzvoTI3gOA4CvJVFivdWE6UU5h9DZ57gJL5PWPd9zKO2v82NI8CG5zGeTLDBKtr4rlrV3X6fJMsK9okV0HNdwiDgRxQVbbTxPL6KIirlMiXfR1nRDIBSlLQmDDa5Ho2waewtjfLu1pZ+4a6AgCCIwDjLuBgOl0f5fz/Ts9dfScq9YFxk3JAAAAAASUVORK5CYII%3D");
  img.js=this;
  img.onclick=function(){this.js.supprime()};
  this.div.appendChild(img);
  //span
  //
  this.verticallegende=document.createElement("span");
  this.verticallegende.setAttribute("class","verticallegende");
  this.verticallegende.js=this;
  this.yaxislegende.appendChild(this.verticallegende);
  //this.onmousedown=function(event){graphiqueMouseDown(event,this);};

}

graphiquePopUp.prototype.setZIndex=function(zindex){
  this.div.style.zIndex=zindex;
  this.titre.style.zIndex=zindex;
  this.nograph.style.zIndex=zindex;
  this.graph.style.zIndex=zindex;
  /*
  this.yaxislegende.style.zIndex=zindex;
  this.legende.style.zIndex=zindex;
  this.contenu.style.zIndex=zindex;
  this.xaxislegende.style.zIndex=zindex;
  this.verticallegende.style.zIndex=zindex;
  */
}

graphiquePopUp.prototype.sansGraph=function(evt){
  //pour �tre au-dessus des autres fen�tres graphiquePopup d�j� construites	
  if(window.graphique_JSX_UniqueIndex==undefined) {
    window["graphique_JSX_UniqueIndex"]=1;
  } else {
	window.graphique_JSX_UniqueIndex++; 
  }
  this.titre.onmousedown=function(event){this.js.graphiqueMouseDownSansGraph(event);}; 
  this.graph.style.display="none";
  this.nograph.innerHTML='';
  this.nograph.style.display="block";
  this.setZIndex(graphique_JSX_UniqueIndex);
}

graphiquePopUp.prototype.avecGraph=function(evt){
  //uniquement pour rebasculer apr�s appel � sansGraph !
  this.titre.onmousedown=function(event){this.js.graphiqueMouseDown(event);}; ;
  this.nograph.style.display="none";
  this.nograph.innerHTML='<center>&nbsp;<br>&nbsp;<br><font size="-1" color="darkgray">Affichage d&eacute;sactiv&eacute; pendant le d&eacute;placement.</font></center>';
  this.graph.style.display="block";
}

graphiquePopUp.prototype.supprime=function(){
  if(this.onSupprime) this.onSupprime(); /*keops*/
  document.body.removeChild(this.div);
  delete(this);
}

/* souris et d�placements */

graphiquePopUp.prototype.graphiqueMouseDown = function(evt){
  evt.preventDefault();  
  if(this.mobile==false) return false; /*keops*/
  this.seDeplace=true;
  this.x0=evt.clientX;
  this.y0=evt.clientY;
  if(!this.moveWithGraph) {
	  this.graph.style.display = "none";
	  this.nograph.style.display = "block";
  }
  var graphiquePopupJStemp=this;
  document.onmousemove=function(event){graphiquePopupJStemp.graphiqueMouseMove(event);};
  document.onmouseup=function(event){graphiquePopupJStemp.graphiqueMouseUp(event);};
  return false;
}

graphiquePopUp.prototype.graphiqueMouseMove=function(evt){
  //alert(js.seDeplace);
  if (!this.seDeplace) return false;
  this.left+=evt.clientX-this.x0;
  this.top+=evt.clientY-this.y0;
  this.div.style.top=this.top+"px";
  this.div.style.left=this.left+"px";
  //alert(js.top+" "+evt.clientX+" "+js.x0);
  this.x0=evt.clientX;
  this.y0=evt.clientY;
  return false;
}

graphiquePopUp.prototype.graphiqueMouseUp=function(evt){
  this.seDeplace=false;
  if(!this.moveWithGraph) {
	  this.nograph.style.display = "none";
	  this.graph.style.display = "block";
  }
  document.onmousemove=null;
  document.onmouseup=null;
  graphiquePopupTempJs=null;
  return false;
}

//utilisation du popup mais sans graphique

graphiquePopUp.prototype.graphiqueMouseDownSansGraph = function(evt){
  evt.preventDefault();  
  if(this.mobile==false) return false; /*keops*/
  this.seDeplace=true;
  this.x0=evt.clientX;
  this.y0=evt.clientY;
  var graphiquePopupJStemp=this;
  document.onmousemove=function(event){graphiquePopupJStemp.graphiqueMouseMoveSansGraph(event);};
  document.onmouseup=function(event){graphiquePopupJStemp.graphiqueMouseUpSansGraph(event);};
  return false;
}

graphiquePopUp.prototype.graphiqueMouseMoveSansGraph=function(evt){
  //alert(js.seDeplace);
  if (!this.seDeplace) return false;
  this.left+=evt.clientX-this.x0;
  this.top+=evt.clientY-this.y0;
  this.div.style.top=this.top+"px";
  this.div.style.left=this.left+"px";
  //alert(js.top+" "+evt.clientX+" "+js.x0);
  this.x0=evt.clientX;
  this.y0=evt.clientY;
  return false;
}

graphiquePopUp.prototype.graphiqueMouseUpSansGraph=function(evt){
  this.seDeplace=false;
  document.onmousemove=null;
  document.onmouseup=null;
  graphiquePopupTempJs=null;
  return false;
}
