// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distribute  in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

function chemin(sourceJs){
  var scripts=document.getElementsByTagName("script");
  var adresse="",fin=-1;
  for (var i in scripts){
    if (scripts[i].hasAttribute("src")){
      adresse=scripts[i].getAttribute("src");
      fin=adresse.indexOf(sourceJs, 0);
      if (fin!=-1)
	break;
    }
  }
  if (fin==-1)
    return "";
  return adresse.substring(0,fin);
}

if (!CHEMIN)
  var CHEMIN=chemin("xcas/base.js");


function xcasBalise(){
  var xcas=document.getElementsByTagName("script");
  var i;
  for (i=0 ; i<xcas.length ; i++){
    if (xcas[i].getAttribute("language")=="xcas")
       xcasRequete(xcas[i].innerHTML);
  }
}

// inclusion feuille de style
function xcasInclureStyle(fileName){
  var link= document.createElement("link");
  link.type = "text/css"; 
  link.href =fileName;
  link.rel="stylesheet";
  var head=document.getElementsByTagName("head");
  if (head[0])
    head[0].appendChild(link);
  return;
}

// classe xcasbase et ses methodes
function xcasBase(){
  this.index;

  // serveur xcas :
  this.url="https://www.xcasenligne.fr/fabrique/"; 
  //this.url="http://localhost/giac_online/fabrique/"; 
  
  // Methodes de xcasBase

  this.initialise=function(){
    this.index=xcasBase.listeElements.length;
    xcasBase.listeElements.push(this);
  }
  
  // inclusion de programmes javascript
  this.include=function(fileName){
    if (!document.getElementsByTagName){
      alert ("Chargement des modules impossible");
      return;
    }
    var script = document.createElement("script");
    script.type = "text/javascript"; 
    script.src =fileName;
    var head=document.getElementsByTagName("head");
    if (head[0])
      head[0].appendChild(script);
    return;
  }

  this.includeStyle=function(fileName){
    xcasInclureStyle(fileName);
  }

  this.fleche=function(ev){
    if (ev.keyCode!=FLECHE_HAUTE && ev.keyCode != FLECHE_BASSE)
      return;
    var input=document.getElementById("in");
    if (ev.keyCode==FLECHE_HAUTE && indexTabHisto>0){
      indexTabHisto--;
      input.value=tabHisto[indexTabHisto];
    }
    if (ev.keyCode==FLECHE_BASSE && indexTabHisto<tabHisto.length){
      indexTabHisto++; 
      if (indexTabHisto<tabHisto.length)
	input.value=tabHisto[indexTabHisto];
      else 
	input.value="";
    }
    return;
  }


  this.reqInit=function(){
    var req;
    if(window.XMLHttpRequest){
      req = new XMLHttpRequest(); 
    }
    else if(xcasBase.isIE) 
      //req = new ActiveXObject("Microsoft.XMLHTTP"); 
      req = new XMLHttpRequest(); 
    else { 
      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
      return; 
    } 
    return req;
  }

  this.reqG=function(s, url,option,xx,yy){
   //alert("s="+" url="+url+" option="+option);
    var i,reg;
    // correction du bug sur les saisie d exposant
    for(i=4 ;i<10;i++){
      reg=RegExp(String.fromCharCode(i+8304),"g");
      s=s.replace(reg,"^"+i);
    }
    var i;
    if (!s)
      return "";
    var reg=RegExp("\'","g");
    s=s.replace(reg,"\"");
    var req=new this.reqInit();
    url=this.url+url;
    try {
       req.open('post',url, false);
    }
    catch(err) {
       var txt="Erreur\n\n";
       txt+="Acces refuse au serveur XCAS en ligne.\n\n";
       txt+="Il faut activer l'acces aux sources de donnees sur plusieurs domaines dans les parametres de securite de votre navigateur.\n\n";
       alert(txt);
  }
    req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
    try {
      req.send("option="+encodeURIComponent(option)+"&in="+encodeURIComponent(s)
	       +"&session_id="+encodeURIComponent(xcasBase.session_id)
	       +"&xx="+encodeURIComponent(xx)
	       +"&yy="+encodeURIComponent(yy));
    }
    catch(err) {
      alert(err);
    }
    var s=req.responseText; 
    for(i=0;i<xcasBase.htmlSymbols.length;i++){
      reg=RegExp(xcasBase.htmlSymbols[i],"g");
      s=s.replace(reg,xcasBase.utfSymbols[i]);
    }
    return s;
  }



  this.reqGiac=function(s){
   return s= this.reqG(s,"giac_interface.php","text");
 //return s= this.reqG(s,"test.php","text");
  }

  this.reqLabomep=function(s){
    return this.reqG(s,"giac_interface.php","labomep");
  }

  this.reqProg=function(s){
    return this.reqG(s,"giac_interface.php","prog");
  }

  this.reqMath=function(s){
    return this.reqG(s,"giac_interface.php","math");
  }

  this.reqTableur=function(s){
   var s=this.reqG(s,"giac_interface.php","spread");
   return s;
  }

  this.reqTableurApprox=function(s){
   var s=this.reqG(s,"giac_interface.php","spreadf");
   return s;
  }

  this.reqTableurTexte=function(s){
   var s=this.reqG(s,"giac_interface.php","cep");
   return s;
  }

  this.reqSVG=function(s){
    var s=this.reqG(s,"giac_interface.php","svg");
    return s;
  }

  this.reqMathSVG=function(s){
    return this.reqG(s,"giac_interface.php","text_math_svg");
  }

  this.reqSVGgrid=function(s,xx,yy){
    var s= this.reqG(s,"giac_interface.php","svg_grid",xx,yy);
    return s;
  }

  // affiche le resultat en mathml
 
  this.insereMathML=function(id,s,inline,remplace){
    var s=">"+s+"</math>";
    if (inline)
      s=" display='inline'"+s;
    else
      s=" display='block'"+s;
    s="<math"+s;
    if (remplace)
      id.innerHTML=s;
    else
      id.innerHTML+=s;
  }

  this.calcule=function(s){
    var sr=this.reqGiac(s);
    // rustine pour IE
    if (xcasBase.isIE)
      sr=sr.replace(new RegExp("``","g"),"` `");
    var t=sr.split(new RegExp("`","g"));
    xcasBase.session_id=t[0];
    return t[1];
  }


  this.calculeMml=function(s,conteneur,inline,remplace){
    var sr=this.reqGiac(s);
    // rustine pour IE
    if (xcasBase.isIE)
      sr=sr.replace(new RegExp("``","g"),"` `");
    var t=sr.split(new RegExp("`","g"));
    xcasBase.session_id=t[0];
    // on affiche le resultat en mathml  
    this.insereMathML(conteneur,t[2],inline,remplace);
    if (conteneur.scrollIntoView)
      conteneur.scrollIntoView(false); 
    return t[1];
  }

  this.createElement=function(type){
    var elt=document.createElement(type);
    elt.setAttribute("index",this.index);
    return elt;
  }

  this.element=function(type,cible,classe){
    var elem=document.createElement(type);
    if (classe)
      elem.className=classe;
    cible.appendChild(elem);
    return elem;
  }

  this.elementIndexe=function(type,cible,classe){
    var elem=this.createElement(type);
    if (classe)
      elem.className=classe;
    cible.appendChild(elem);
    return elem;
  }

}
// ------------ fin fonction xcasBase


// Constantes


xcasBase.isIE = window.ActiveXObject;
xcasBase.isOpera=(navigator.appName=="Opera");
xcasBase.isFirefox=(navigator.userAgent.indexOf('Gecko')>-1 && navigator.userAgent.indexOf('KHTML')==-1);
xcasBase.isWebKit=(navigator.userAgent.indexOf('AppleWebKit/') > -1 || navigator.userAgent.indexOf('KHTML')>-1);
if (xcasBase.isIE &&document.documentMode==9) {
  xcasBase.isIE=false;
  xcasBase.isWebKit=true
}

xcasBase.mathmlURL = "http://www.w3.org/1998/Math/MathML";
xcasBase.svgURL="http://www.w3.org/2000/svg";

xcasBase.htmlSymbols=["&int;","&Integral;","&Sigma;","&part;","&nbsp;",
		      "&rarr;","&VerticalBar;","&deg;","&infin;","&InvisibleTimes;",
		      "&pi;","&le;","&ge;","\""];
xcasBase.utfSymbols=["\u222B","\u0222B","\u03A3","\u2202","\u0020",
		     "\u2192","|","\u000B0","\u221E","", 
		     "\u03C0","\u2264","\u2265","'"];

xcasBase.TABULATION=9;
xcasBase.FLECHE_HAUTE=38;
xcasBase.FLECHE_BASSE=40; 
xcasBase.FLECHE_GAUCHE=37;
xcasBase.FLECHE_DROITE=39; 
xcasBase.CTRL_M=77; 
xcasBase.ENTREE=13;
xcasBase.session_id="";


if (xcasBase.isWebKit)
  xcasInclureStyle(CHEMIN+"xcas/mathml.css");

function xcasOpacite(objet,opaque){
  var IEopacite, opacite;
  if (opaque) {
    IEopacite="100";
    opacite=1;
  } else {
    IEopacite="60";
    opacite=0.6;
  }
  if (xcasBase.isIE)
    objet.style.filter="alpha(opacity="+IEopacite+")";
  else
    objet.style.opacity=opacite;
}



// liste des objets crees
xcasBase.listeElements=new Array();

function xcasElement(elt){
  return xcasBase.listeElements[parseInt(elt.getAttribute("index"))];
}

function xcasInsereTexte(texte,tag,cible){
  var tag=document.createElement(tag);
  text=document.createTextNode(texte);
  tag.appendChild(text);
  cible.appendChild(tag);
}


// fonctions de requetes

function xcasRequete(s){
  var x=new xcasBase();
  return x.calcule(s);
}

// pour ~viter que l'utlisateur attende la premiere reponse :
//xcasRequete("0");

function xcasRequeteMml(s,conteneur,inline,remplace){
  if (typeof conteneur == typeof "a")
    conteneur=document.getElementById(conteneur);
  var x=new xcasBase();
  return x.calculeMml(s,conteneur,inline,remplace);
}



// autres classes
// leurs methodes sont dans des fichiers annexes

xcas.prototype=new xcasBase;

function xcas(){
  this.parent=xcasBase;
  this.parent();
  this.commande="";
  this.retours=new Array();
  this.retoursMml=new Array();
  this.retoursSvg=new Array();
  this.mmlEnLigne=false;
  this.remplaceMml=false;
  this.remplaceSvg=false;
  this.couleurs=new Array();
  this.couleurSvg="black";
  this.colors=new Array();
  this.colors["noir"]="black";
  this.colors["blanc"]="white";
  this.colors["gris"]="gray";
  this.colors["rouge"]="red";
  this.colors["jaune"]="goldenrod";
  this.colors["bleu"]="darkblue";
  this.colors["orange"]="darkorange";
  this.colors["violet"]="purple";
  this.colors["vert"]="green";
  return this;
}


xcas.prototype.argument=function(arg){
  var elem=document.getElementById(arg);
  if (!elem){
    /*alert("ATTENTION : l'element "+arg+" n\'existe pas");*/
    return arg;
  }
  switch (elem.tagName.toLowerCase()){
    case "input":
    return elem.value;
    case "select":
    return elem.selectedIndex;
    case "div":
    return elem.getAttribute("texte");
    case "p":
    return elem.getAttribute("texte");
    case "span":
    return elem.getAttribute("texte");
    case "form" : //boutons radio
    for (var i in elem.elements)
      if (elem.elements[i].checked)
	  return i;
    return 0;
  }
}

  xcas.prototype.eval=function(){
  var i;
  // synthese de la commande
  this.commande="";
  if (arguments.length>0){
    this.commande=arguments[0];
    if (arguments.length>1){
      this.commande+="("+this.argument(arguments[1]);
      for (i=2 ; i<arguments.length ; i++)
	this.commande+=","+this.argument(arguments[i]);
      this.commande+=")";
    }
  }
  var sr;
  sr=this.reqLabomep(this.commande);
  var tr=sr.split(new RegExp("`SVG","g"));
  var t=tr[0].split(new RegExp("`","g"));
  var r=t[1].split(new RegExp(",","g"));
  // traitement des couleurs
  this.couleurs=new Array();
  this.retours=new Array();
  this.retoursMml=new Array();
  var c;
  for (i=0;i<r.length;i++){
    c=this.couleur(r[i]);
    if (c[0])
      this.couleurs.push(c[1]);
    else {
      this.retours.push(r[i]);
      this.retoursMml.push(t[i+2]);
      if (i>this.couleurs.length)
	this.couleurs.push("black");
    }
  }
  //graphique eventuel
  this.retoursSvg=new Array();
  t=tr[1].split(new RegExp("`","g"));
  if (t[0] && t[0] != "" && t[0]!=" " 
      && t[0] != "error" 
      && t[0]!="undef")
    this.retoursSvg=t;
  return sr;
}


xcas.prototype.evalSvg=function(){
  var i;
  // synthese de la commande
  this.commande="";
  if (arguments.length>0){
    this.commande=arguments[0];
    if (arguments.length>1){
      this.commande+="("+this.argument(arguments[1]);
      for (i=2 ; i<arguments.length ; i++)
	this.commande+=","+this.argument(arguments[i]);
      this.commande+=")";
    }
  } 
  var sr;
  sr=this.reqSVG(this.commande); 
  var t=sr.split(new RegExp("`","g"));
  //this.retours=t[1].split(new RegExp(",","g"));
  this.retoursSvg=new Array();
  for (i=1; i<t.length ; i++)
    this.retoursSvg.push(t[i]);
  return sr;
}



xcas.prototype.nonEval=function(){
  this.couleurs=new Array();
  var i;
  // synthese de la commande
  this.commande="";
  if (arguments.length>0){
    this.commande=arguments[0];
    if (arguments.length>1){
      this.commande+="("+this.argument(arguments[1]);
      for (i=2 ; i<arguments.length ; i++)
	this.commande+=","+this.argument(arguments[i]);
      this.commande+=")";
    }
  }
  var sr;
  sr=this.reqMath(this.commande);
  var t=sr.split(new RegExp("`","g"));
  this.retours=t[1].split(new RegExp(",","g"));
  this.retoursMml=new Array();
  for (i=2; i<t.length ; i++)
    this.retoursMml.push(t[i]);
 //alert(t);
  return sr;
}

xcas.prototype.imprimeEnLigne=function(enLigne){
    this.mmlEnLigne=enLigne;
}

xcas.prototype.remplaceQuandImprime=function(remplace){
    this.remplaceMml=remplace;
    this.remplaceSvg=remplace;
}

xcas.prototype.couleur=function(s){
  if (s.length<3)
     return [false,s];
  var t=s.substring(1,s.length-1);
  if (this.colors[t])
    return [true, this.colors[t]];
  if (t.charAt(0)=="#")
    return [true,t];
  return [false,s]; 
}

xcas.prototype.imprimeMml=function(conteneur,i,enLigne,remplace){
  if (conteneur=="")
    return;
  if (this.retoursMml.length==0)
    return;
  if (typeof conteneur == typeof "a")
    conteneur=document.getElementById(conteneur);
  //alert("conteneur : "+conteneur);
  if (!conteneur)
    return;
  if (enLigne!=null)
    this.mmlEnLigne=enLigne;
  // specifique a la fabrique
  if (conteneur.getAttribute("horsligne"))
    this.mmlEnLigne=false;
  if (remplace!=null)
    this.remplaceMml=remplace; 
  if (!i)
    var i=0;
  if (i>this.retoursMml.length)
    i=retoursMml.length-1;
  if (this.retours[i]==null || this.retours[i]=="SVG" || i>0 &&this.retours[i-1]=="SVG" )
    return;
  conteneur.setAttribute("texte",this.retours[i]);
  var mml=this.insereMathML(conteneur,this.retoursMml[i],this.mmlEnLigne,this.remplaceMml);
 //alert(i+this.couleurs+this.retours);
  conteneur.style.color=this.couleurs[i];
  if (!this.remplaceMml){
    var hr=document.createElement("hr");
    conteneur.appendChild(hr);
    hr.scrollIntoView(false);
  }
  if (fab.id && 
      conteneur.getAttribute("class")=="sortie"){ // on est sous la fabrique
    //alert(conteneur.getAttribute("class"));
    var h=parseInt(conteneur.style.height);
    if (isNaN(h))
      h=0;
    conteneur.style.height="auto";
    conteneur.style.lineHeight="auto";
    var ht=conteneur.offsetHeight;
    //alert(h+" "+ht);
    if (ht>h+2){
      conteneur.style.height=ht+"px";
      //conteneur.style.lineHeight=conteneur.style.height;
    } 
    else{
      conteneur.style.height=h+"px";
      //conteneur.style.lineHeight=conteneur.style.height;
    } 
  }
}


xcas.prototype.imprimeSvg=function(xcSvg,remplace,protege){
  if (!xcSvg)
    return;
  if (remplace!=null)
    this.remplaceSvg=remplace;
  if (protege==null)
    protege=false;
  var couleur="black";
  if (this.couleurs.length>this.retours.length)
    couleur=this.couleurs[this.couleurs.length-1];
  xcSvg.insere(this.retoursSvg[0],"figures",couleur,this.remplaceSvg,protege);
  xcSvg.insere(this.retoursSvg[1],"legende",couleur,this.remplaceSvg,protege);
}

// -----------------------------------------------------------------------------
//xcasConsole : envoyer des requetes xcas et afficher les reponses

xcasConsole.prototype=new xcasBase;

function xcasConsole(id, message){ 
  this.parent=xcasBase;
  this.parent();
  this.include(CHEMIN+"xcas/console.js");
  this.includeStyle(CHEMIN+"xcas/console.css");
  this.initialise();
  if (id){
    var console;
    if (typeof id==typeof "a")
      console=document.getElementById(id);
    else
      console=id;
    //console.setAttribute("class","console");
	console.className="console";
    if (message) 
      xcasInsereTexte(message,"p",console);
    this.histo=document.createElement("div");
    console.appendChild(this.histo);
    this.input=this.createElement("input");
    this.input.onkeydown=function(event){this.blur();this.focus();xcasElement(this).valide(event)};
    console.appendChild(this.input);
    console.setAttribute("index",this.index);
    console.onclick=function(){xcasElement(this).input.focus()};
    this.input.focus();
    this.tabHisto=new Array();
  }
}


//-------------------------------------------------------------------------
var xcasTableurActif;


xcasTableur.prototype=new xcasBase;

xcasTableur.mini=false;
xcasAttendre=true;
xcasTableur.hauteurMax=500; //en px

function xcasTableur(id, nLi, nCol){
 if (document.getElementById("xcA0")){
    alert("une seule feuille par page !");
    return;
  }
  this.parent=xcasBase;
  this.parent();
  this.include(CHEMIN+"xcas/tableur.js");
  this.includeStyle(CHEMIN+"xcas/tableur.css");
  this.initialise();
  xcasTableurActif=this;
  // exact ou approx
  this.exact=true;
  this.cle="ma feuille";
  var bloc=document.getElementById(id);
  bloc.className="tableur";
  // neutralise le copier coller
  bloc.onkeydown=function(ev){ if (ev.ctrlKey) return false;}
  var i,j;
  this.outils=this.element("div",bloc,"outils");
  if (xcasTableur.mini){
    var divEntree=this.element("div",bloc,"diventree");
    bloc.style.width="415px";
  }
  else {
    var divEntree=this.element("div",this.outils,"diventree");
  }
  this.tabCell=this.element("span",divEntree);
  this.tabCell.appendChild(document.createTextNode("A0")); 
  this.entree=this.elementIndexe("input",divEntree);
  this.insereOutil("triangleb.png",divEntree,"Assistant","dispAssist",true);
  this.entree.onclick=function(event){xcasElement(this).curseur()};
  this.entree.onkeyup=function(event){xcasElement(this).curseur();xcasElement(this).modeSaisie(event);};
  this.entree.onkeydown=function(event){this.blur();this.focus();xcasElement(this).ctrl(event);};
  this.insereOutil("initial.gif",this.outils,"Recalculer la feuille","fill_cell",true);
  this.insereOutil("flecheb.png",this.outils,"Copier vers le bas","fill_bottom");
  this.insereOutil("fleched.png",this.outils,"Copier vers la droite","fill_right");
  this.insereOutil("annul.gif",this.outils,"Annuler la dernière entrée","annule");
  this.insereOutil("gomme.gif",this.outils,"Vider la feuille","vide");
  this.insereOutil("chart.png",this.outils,"Graphiques","assistGraph",true);
  this.insereOutil("ardoise.png",this.outils,"Ardoise Xcas","afficheArdoise",true);
  this.insereOutil("prog.png",this.outils,"Programmation Xcas","afficheProg",true);
  this.insereOutil("ouvrir.gif",this.outils,"Ouvrir une feuille","ouvrir",true);
  this.insereOutil("disquette.gif",this.outils,"Sauvegarder la feuille","sauve",true);
  this.insereOutil("fusion.png",this.outils,"Fusionner les cellules sélectionnées","fusion");
  this.insereOutil("pref.png",this.outils,"Préférences","pref",true);
  this.insereOutil("aide.gif",this.outils,"Aide","aide",true);
  var conteneur=this.element("div",bloc,"conteneur");
  this.divAssist=this.element("div",conteneur,"assistant");
  this.faitAssist(this.divAssist);
  this.sel=this.elementIndexe("select",this.divAssist,"commandes");
  if (xcasTableur.mini)
    this.sel.style.display="block";
  this.sel.onchange=function(){xcasElement(this).assiste(this)};
  this.assistant(this.sel,this.divAssist);
  this.spanAssist=this.element("span",this.divAssist);
  this.spanAssist.style.display="inline";
  this.divAssistGraph=this.element("div",conteneur,"assistant");
  this.spanAssistGraph=this.element("span",this.divAssistGraph);
  this.spanAssistGraph.style.display="inline";
  this.faitAssist(this.divAssistGraph);
  this.progress=this.element("progress",conteneur,"progress");
  this.progress.setAttribute("style","display:none");
  this.tableau=this.element("div",conteneur,"tableau");
  var table=this.element("table",this.tableau,"table");
  var A="A";
  var tr,td;
  this.thead=this.element("thead",table);
  tr=this.element("tr",this.thead);
  td=this.element("th",tr,"col1");
  this.lettres=new Array();
  for (j=0 ; j<nCol ; j++) {
    td=this.element("th",tr);
    this.lettres.push(String.fromCharCode(A.charCodeAt(0)+j));
    td.appendChild(document.createTextNode(this.lettres[j]));
  }
  for (j in this.lettres)
    this.lettres[j]="xc"+this.lettres[j];
  this.tbody=this.element("tbody",table); 
  this.tbody.setAttribute("id","corps_table");
  for (i=0;i<nLi;i++){
    tr=this.element("tr",this.tbody);
    td=this.element("td",tr,"col1");
    td.appendChild(document.createTextNode(i));
    for (j=0 ; j<nCol ; j++) {
      td=this.elementIndexe("td",tr);
      td.setAttribute("id",this.lettres[j]+i);
      td.onclick=function(){xcasElement(this).cell(this.id)};
      td.onmousedown=function(event){xcasElement(this).mouseDown(event,this)};
      td.onmouseover=function(event){xcasElement(this).mouseOver(event,this)};
      td.onmouseup=function(event){xcasElement(this).mouseUp(event,this)};
      td.ondragenter=function(event){xcasElement(this).dragEnter(event,this);};
      //modif pour fabrique
      //td.setAttribute("onclick","xcasElement(this).cell(this.id)");
    }
  }
  this.hmax=xcasTableur.hauteurMax;
  this.entreeSauve=this.entree;
  this.prog="//Taper des fonctions ici\n//et elles seront utilisables dans le tableur";
  this.cellActive="A0";
  this.cell("A0");
  this.debutZone="";
  this.zone=new Array();
  this.zoneCode="";
  this.cellDragStart=""
  this.cellDragEnd="";
  this.cellZone=new Array();
  this.saisie=false;
  this.changeEntree=false;
  this.n_lign=nLi;
  this.n_col=nCol;
  this.nb_lign=0;
  this.nb_col=0;
  this.sauvegarde="";
  this.retourGiac="";
  this.nb_lign_sauv;
  this.nb_col_sauv;
  this.ref_copy;
  this.form_copy;
  this.cell_marquees=new Array();
  // remplissage
  if (arguments.length>3){
    //methode par les chaines
    var lettre;
      var cell, t, formule;
    if (arguments[3].charAt){
      if (!cellule)
	cellule="cxA0";
      var coord=this.coordonnees(cellule);
      for (i=4 ; i<arguments.length; i++){
	lettre="cx"+coord[0];
	formule=arguments[i]+"";
	t=formule.split(new RegExp(";","g"));
	for (j in t){
	  cell=document.getElementById(lettre+coord[1]);
	  this.cell_max(lettre+coord[1]);
	  lettre=String.fromCharCode(lettre.charCodeAt(0)+1);
	  cell.setAttribute("formule",t[j]);
	//  cell.appendChild(document.createTextNode(t[j]));
	}
	coord[1]++;
      }
    } else {
      // methode par les objets
      this.remplir(arguments[3],false);
      this.timer=setInterval(xcasTableurActif.preremplissage,10);
     // setTimeout("xcasTableurActif.fill_cell(false)",1000);
    }
   
  } 
  return this;
}

xcasTableur.prototype.remplir=function(obj,calculer){
  for (i in obj){
    if (i==this.cellActive)
      this.entree.value=obj[i];
    cell=document.getElementById("xc"+i);
    this.cell_max(i);
    cell.setAttribute("formule",obj[i]);
  }
  if (calculer)
    this.fill_cell(false);

}

xcasTableur.prototype.preremplissage=function(){
  if (xcasAttendre) 
    return;
  clearInterval(xcasTableurActif.timer);
  xcasTableurActif.fill_cell(false);
}

xcasTableur.prototype.poignee=function(c2){
    // la poignee passe de c1 a c2
    var p=document.getElementById("xcdpoignee");
    var c1;
    if(p){
      c1=p.parentNode;
      c1.innerHTML=this.sauveCell;
    }
    this.sauveCell=c2.innerHTML;
    var esp="";
	//var xcA1="\\"xcA1\\"";
    if (this.sauveCell=="")
      esp="&nbsp;"
    c2.innerHTML="<div id='xcdpoignee'>"+esp+this.sauveCell+
	"<img id='poignee' ondragstart='xcasElement(this.parentNode.parentNode).dragStart(event,this)' ondragEnd='xcasTableurActif.dragEnd()' src='"+CHEMIN+"xcas/img/poignee.png'></div>";
    var xcdpoignee=document.getElementById("xcdpoignee");
    xcdpoignee.style.height=xcdpoignee.parentNode.offsetHeight+"px";
}

xcasTableur.prototype.faitAssist=function(div){ 
  var fermer;
  var img=document.createElement("img");
  img.src="xcas/img/fermer.png";
  if (div==this.divAssist)
    img.onclick=function(){xcasTableurActif.stopAssist();};
  else if (div==this.divAssistGraph)
    img.onclick=function(){xcasTableurActif.stopAssist(true);};
  div.appendChild(img);
}


xcasTableur.prototype.commandes=[
				 ["Choisir ...","",""],
			       
				 ["Développer","developper","l\'expression"],
				 ["Factoriser","factoriser","l\'expression"],
				 ["Simplifier","simplifier","l\'expression"],
				 ["Forme canonique","forme_canonique","du trinôme"],
				 ["Résolution équation","resoudre","",["d\'inconnue","x"]],
				 ["Résolution système","linsolve",[["eq1",""],["eq2",""],["eq3",""],["eq4",""],["inconnues","x y z t"]]],
				 ["Matrice","matrice",[["ligne1","1 2 3 "],["ligne2",""],["ligne3",""],["ligne4",""]]],
			       
				 ["Dériver","deriver","la fonction",["de la variable","x"]],
				 ["Intégrer","integration","la fonction",["de la variable","x"],"de","à"],
				 ["Une primitive","integration","de la fonction",["de la variable","x"]],
				 ["Limite de","limite","la fct",["quand","x"],"→"],
			       
				 ["Quotient div euclid","iquo","de","par"],
				 ["Reste div euclid","irem","de","par"],
				 ["Nombre premier ?","est_premier",":"],
				
				 ["Forme algébrique","evalc","de :"],
				 ["Partie réelle","re","de :"],
				 ["partie imaginaire","im","de :"],
				 ["Module","abs","de :"],
				 ["Argument","arg","de :"],

				 ["Somme","somme","Série :"],
				 ["Moyenne","moyenne","Série :"],
				 ["Ecart-type","ecart_type","Série :"],
				 ["Minimum","min","Série :"],
				 ["Maximum","max","Série :"],
				 ["Quantile","quantile","Série :",["Fréquence (0.25 pour Q1) :","0.25"]],
			
				 ["Entier aléatoire","alea","compris entre 0 et"],
				 ["Réel aléatoire","hasard","compris entre","et"],
				 ["Selon loi normale","randnorm","de moyenne","et d\'écart-type"]
				 ];

xcasTableur.prototype.stopAssist=function(finSaisie){
  this.entree=this.entreeSauve;
  this.divAssist.style.display="none";
  this.divAssistGraph.style.display="none";
  this.entree.focus();
  if (finSaisie)
    this.saisie=false;
}

xcasTableur.prototype.dispAssist=function() { 
  if (this.divAssist.style.display=="block"){
    //this.divAssist.style.display="none";
    this.stopAssist()
  }
  else {
    this.stopAssist();
    this.divAssist.style.display="block";
  }
}

xcasTableur.prototype.assistant=function(sel,div){
  var s="",option;
  var optGroups=[1,7,4,3,5,6,3];
  var groupTitles=["Algèbre","Analyse","Arithmétique","Nombres complexes","Statistiques","Tirages aléatoires"];
  var i,j;
  for (i=1; i<optGroups.length ; i++)
    optGroups[i]+=optGroups[i-1];
  for (i in this.commandes){
    for (j in optGroups){
      if (optGroups[j]==i){
	s=groupTitles[j];
	option=this.element("optgroup",sel);
	option.setAttribute("label",s);
      }
    }
    option=this.element("option",sel);
    s=this.commandes[i][0];
    option.appendChild(document.createTextNode(s));
  }
}

xcasTableur.prototype.curseur=function() { 
    // d'apres Diego Perini <dperini@nwbox.com>
	if (this.entree.createTextRange) {
		var r = document.selection.createRange().duplicate()
		r.moveEnd('character', this.entree.value.length)
		if (r.text == '') 
		  this.entree.debut=this.entree.value.length
		else
		  this.entree.posCurseur=this.entree.value.lastIndexOf(r.text);
	} else  {
	  this.entree.posCurseur=this.entree.selectionStart;
	}
}

xcasTableur.prototype.insereDansEntree=function(chaine){
  if (chaine=="")
    return;
  if (chaine.charAt(0)=="x")
    chaine=chaine.slice(2);
  var s=this.entree.value;
  var s1=s.substring(0,this.entree.posCurseur);
  var s2=s.substring(this.entree.posCurseur,s.length);
  this.entree.value=s1+chaine+s2;
  this.curseur();
}

xcasTableur.prototype.cell=function(cellule){
  if (this.saisie){
    this.insereDansEntree(cellule);
    return;
  }
  if (this.changeEntree){
    this.changeEntree=false;
    this.fill_cell(false);
  }
  if (this.debutZone!="copy"){
    this.debutZone="";
    this.zone=new Array();
  }
  var domCellule=document.getElementById("xc"+this.cellActive);
//alert("xc"+this.cellActive+" "+domCellule);
  //domCellule.style.border="1px solid #333333";
  domCellule.style.backgroundColor="white";
  domCellule.style.color="black";
  if(document.getElementById("xcdpoignee"))
    domCellule.innerHTML=this.sauveCell;
  if (cellule.charAt(0)=="x")
    this.cellActive=cellule.slice(2);
  else
    this.cellActive=cellule;
  domCellule=document.getElementById("xc"+this.cellActive);
  if (domCellule.hasAttribute("groupe")){
    this.cellActive=domCellule.getAttribute("groupe");
    domCellule=document.getElementById("xc"+this.cellActive);
  }
  domCellule.style.backgroundColor="#ffff99";
  //domCellule.style.border="2px solid black ";
  this.poignee(domCellule);
  this.tabCell.firstChild.nodeValue=this.cellActive;
  var formule=domCellule.getAttribute('formule');
  if (formule &&  formule!="null"){
    this.entree.value=formule;
    this.entree.select();
  }
  else
    this.entree.value="";
  // sauvegarde pour annulation ulterieure
  this.entree.sauvegarde=this.entree.value;
  this.entree.focus();
  // reinitialisation de l'assistant
  this.spanAssist.innerHTML="";
  this.sel.selectedIndex=0;
}

xcasTableur.prototype.coordonnees=function(ref){
  if (ref.charAt(0)!="x")
    ref="xc"+ref;
  // ref est une reference de cellule, de ligne ou de colonne
  if (ref.length==1){ 
    // ligne ou colonne
    var n=parseInt(ref);
    if (isNaN(n))
      return [ref,n];
    else
      return ["",n];
  }
  // cellule :
  var s=ref.slice(3);
  return [ref.slice(2,3), parseInt(s)];
}

xcasTableur.prototype.insereOutil=function(src,cible,titre,fonction,param){
  if (!param)
    var param="";
  var a=this.element("a",cible);
  a.setAttribute("title",titre);
  a.onmouseover=function(){xcasOpacite(this,false)};
  a.onmouseout=function(){xcasOpacite(this,true)};
  img=this.elementIndexe("img",a);
  img.setAttribute("src",CHEMIN+"xcas/img/"+src);
  img.onclick=function(){eval("xcasElement(this)."+fonction+"("+param+")")};
  // modif pour fabrique
  //img.setAttribute("onclick","xcasElement(this)."+fonction+"("+param+")");
  a.appendChild(img);
}

 
xcasTableur.prototype.cell_max=function(cellule){
  var t=this.coordonnees(cellule);
  var A="A";
  this.nb_col_sauv=this.nb_col;
  this.nb_lign_sauv=this.nb_lign;
  this.nb_lign=Math.max(this.nb_lign,t[1]+1);
  this.nb_col=Math.max(this.nb_col,Number(t[0].charCodeAt(0))-A.charCodeAt(0)+1);
  return;
}
  

xcasSvg.prototype=new xcasBase;

function xcasSvg(id,largeur,hauteur,X,Y){
  this.parent=xcasBase;
  this.parent();
  this.include(CHEMIN+"xcas/svg.js");
  //this.includeStyle(CHEMIN+"xcas/svg.css");
  this.initialise();
  this.NS="http://www.w3.org/2000/svg";
  if (id){
    id=document.getElementById(id);
    this.svg=document.createElementNS(this.NS,"svg");
    this.svg.setAttribute("width",largeur+"cm");
    this.svg.setAttribute("height",hauteur+"cm");
    this.X=X;
    this.Y=Y;
    id.appendChild(this.svg);
  }
}

xcasSvg.prototype.exploreDomSvg=function(node,clone){
  var node1;
  var i;
  if (node.tagName){
    node1=document.createElementNS(this.NS,node.tagName);
    for (i=0; i<node.attributes.length; i++)
	node1.setAttribute(node.attributes[i].name,node.attributes[i].value);
  }
  else
    node1=document.createTextNode(node.nodeValue);
  clone.appendChild(node1);
  var fils=node.firstChild;
  while (fils){
    this.exploreDomSvg(fils,node1);
    fils=fils.nextSibling;
  }
  return clone;
}



xcasSvg.prototype.insere=function(str,sId,couleur,remplace,protege){
  if( !str)
    return;
  var id, i,node;;
  if (sId && sId!="")
     id=document.getElementById(sId);
  else
    if (this.svg)
      id=this.svg;
    else
      return;
  if (remplace){
    for (i=id.childNodes.length-1; i>=0; i--){
      node=id.childNodes[i];
      if (protege 
	  || !node.tagName 
	  || node.getAttribute("protege")!="1")	   
	id.removeChild(node);
      }
  }
  var svgElement;
  if (xcasBase.isWebKit){ 
    svgElement=document.createElementNS(this.NS,"svg");
    svgElement1=document.createElementNS(this.NS,"svg");
    var node=parent.document.createElement("span");
    node.innerHTML="<g>"+str+"</g>";
    svgElement=this.exploreDomSvg(node.firstChild,svgElement1);
    svgElement=svgElement.firstChild;
   } else {
    var range = document.createRange();
    range.selectNodeContents(id); //(svg)
    //plantage safari ici
    svgElement = range.createContextualFragment(str); 
  }
  id.appendChild(svgElement);
  // on retrouve svgElement dans le dom
  i=id.childNodes.length-1;
  if (i==-1)
    return;
  while ( i>0 && !id.childNodes[i].tagName){
    i--;
  }
  if (protege && id.childNodes[i].setAttribute)
    id.childNodes[i].setAttribute("protege","1");
  if (sId=="legende")
    id.childNodes[i].setAttribute("fill",couleur);
  else
    id.childNodes[i].setAttribute("stroke",couleur);
}


xcasSvg.prototype.svgGrid=function(xmin,ymin,xmax,ymax){
  var fenetre="xyztrange("+xmin+","+xmax+","+ymin+","+ymax+",-5,5,"+xmin+","+xmax+","+ymin+","+ymax+",-2,2,1,0,1)";
  var sr=this.reqSVGgrid(fenetre,this.X,this.Y);
  var t=sr.split(new RegExp("`","g"));
  while (this.svg.firstChild)
    this.svg.removeChild(this.svg.firstChild);
  this.insere(t[1]);
  return;
}

function xcasPopUp(left,top,width){
  this.mobile=false;
  this.x0=0;
  this.y0=0;
  this.left=left;
  this.top=top;
  xcasInclureStyle(CHEMIN+"xcas/popup.css");
  this.div=document.createElement("div");
  this.div.setAttribute("class","popup");
  this.div.style.top=top+"px";
  this.div.style.left=left+"px";
  if (width)
    this.div.style.width=width+"px";
  document.body.appendChild(this.div);
  this.titre=document.createElement("h3");
  this.titre.js=this;
  this.titre.onmousedown=function(event){this.js.mouseDown(event)};
  this.div.appendChild(this.titre);
  this.contenu=document.createElement("div");
  this.contenu.js=this;
  this.div.appendChild(this.contenu);
  var img=document.createElement("img");
  img.setAttribute("class","fermer");
  img.setAttribute("src","data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAAsSAAALEgHS3X78AAAAB3RJTUUH1gQFFCUoDIRw0QAAAaJJREFUOMulk0FLG0EYhp9ZV9zpKnFbgzSbo80PEA8RLbSXQv5DjulRf0x7bI/5Dx5bQSh4UrxIPJq0DdsQ4kLWZZP5euhszBpzqQMDwzvzvXzf+74Dz1zqMdCGEDgCGkDNwh3gBPjchN5Sgja0gE/1MNQlrREAEYwx3N0nnP/6nQDHTfi6QNCG1ivP+7JXrfIniojjmIkxCLDiOKz7Pi+DgMt+n0GafsxJ1FzbNx92dvTP21uSNEWgsAHWVld5vb3NabebAG+a0HPs3VG9UtGDKCJJU+oi7IvMCg9FOBQhyTIGwyF75bK2OpETNDa05i6OZ0UAByIcyANigNF4jHZdrMgzgpoSYWpnPlML5vBdKQSYymyoGoCbPxARTH5+wu+CJuYBzzvoTI3gOA4CvJVFivdWE6UU5h9DZ57gJL5PWPd9zKO2v82NI8CG5zGeTLDBKtr4rlrV3X6fJMsK9okV0HNdwiDgRxQVbbTxPL6KIirlMiXfR1nRDIBSlLQmDDa5Ho2waewtjfLu1pZ+4a6AgCCIwDjLuBgOl0f5fz/Ts9dfScq9YFxk3JAAAAAASUVORK5CYII%3D");
  img.js=this;
  img.onclick=function(){this.js.supprime()};
  this.div.appendChild(img);
}

xcasPopUp.prototype.supprime=function(){
  document.body.removeChild(this.div);
  delete(this);
}

xcasPopUp.prototype.mouseDown=function(evt){
  evt.preventDefault();
  this.mobile=true;
  this.x0=evt.clientX;
  this.y0=evt.clientY;
  var js=this;
  document.addEventListener("mousemove",mm=function(event){xcasMouseMove(event,js);},false);
  document.addEventListener("mouseup",mu=function(event){xcasMouseUp(event,js);},false);
  return false;
}

xcasMouseMove=function(evt,js){
  if (!js.mobile)
    return false;
  js.left+=evt.clientX-js.x0;
  js.top+=evt.clientY-js.y0;
  if (js.top<0)
    js.top=0;
  if (js.top>document.body.clientHeight)
    js.top=document.body.clientHeight;
  var w=50-parseInt(js.div.style.width);
  if (js.left<w)
    js.left=w;
 if (js.left>document.body.clientWidth)
    js.left=document.body.clientWidth;
  js.div.style.top=js.top+"px";
  js.div.style.left=js.left+"px";
  //window.status=js.top+" "+evt.clientX+" "+js.x0+" "+document.body.clientHeight;
  js.x0=evt.clientX;
  js.y0=evt.clientY;
  return false;
}

xcasMouseUp=function(evt,js){
  js.mobile=false;
  document.removeEventListener("mousemove",mm,false);
  document.removeEventListener("moussup",mu,false);
  return false;
}

