// -*- coding: utf-8-unix -*-

//   Copyright (C) 2009 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//    KEOPS (E.Ostenne) - emmanuel.ostenne@sesamath.net (graphiques)
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA



xcasTableur.prototype.fusion=function(){
  var cell=document.getElementById("xc"+this.cellActive);
  if (this.zone.length==0){
    if (!cell.hasAttribute("groupe")){
      alert("Sélectionner d\'abord des cellules à fusionner");
      return;
    }
    else {
      coords=this.coordonnees(this.cellActive);
      if (cell.hasAttribute("rowspan")){
	cell.removeAttribute("rowspan");
	for (i=0 ; i<this.n_lign ; i++){
	  cell=document.getElementById("xc"+coords[0]+i);
	  if (cell.getAttribute("groupe")==this.cellActive){
	    cell.removeAttribute("groupe");
	    cell.style.display="table-cell";
	  }
	}
	return;
      }
      if (cell.hasAttribute("colspan")){
	cell.removeAttribute("colspan");
	for (i=0 ; i<this.n_col ; i++){
	  cell=document.getElementById(this.lettres[i]+coords[1]);
	  if (cell.getAttribute("groupe")==this.cellActive){
	    cell.removeAttribute("groupe");
	    cell.style.display="table-cell";
	  }
	}
	return;
      }
    }
  }
  if (this.zone[0]!=this.zone[2] && this.zone[1]!=this.zone[3])
    return;
  if (this.zone[1]==this.n_lign-1 && this.zone[3]==this.n_lign-1){
    alert("Pas de fusion sur la dernière ligne.");
    return;
  }
  var i;
  var id=this.zone[0]+this.zone[1]; 
  cell=document.getElementById("xc"+id);
  cell.setAttribute("groupe",id);
  if (this.zone[0]==this.zone[2]){
    cell.setAttribute("rowspan",this.zone[3]-this.zone[1]+1);
    for (i=this.zone[1]+1 ; i<=this.zone[3]; i++){
      cell=document.getElementById("xc"+this.zone[0]+i);
      cell.style.display="none";
      cell.setAttribute("groupe",id);
    }
  }
  else {
    var cDebut=this.indexLettre(this.zone[0]);
    var cFin=this.indexLettre(this.zone[2]);
    cell.setAttribute("colspan",cFin-cDebut+1);
    for (i=cDebut+1 ; i<=cFin; i++){
      cell=document.getElementById(this.lettres[i]+this.zone[1]);
      cell.style.display="none";
      cell.setAttribute("groupe",id);
    }
  }
  this.zoneCode="";
}
 
xcasTableur.prototype.indexLettre=function(c){
  for (var i=0; i<this.lettres.length ; i++)
    if (this.lettres[i]=="xc"+c)
      break;
  return i;
}
  
xcasTableur.prototype.ctrl=function(ev){
  if (ev.keyCode==xcasBase.ENTREE){
    //this.entree.select();  utile ?
    this.colorieZone(this.zone[0],this.zone[2],this.zone[1],this.zone[3],"white");
    this.fill_cell(false);
    delete(this.entree.sauvegarde);
  }
 
  // deplacement avec les fleches
  var cellule=document.getElementById("xc"+this.cellActive);
  var x=0,y=0;
  if (cellule.hasAttribute("colspan"))
    x=parseInt(cellule.getAttribute("colspan"))-1;
  if (cellule.hasAttribute("rowspan"))
    y=parseInt(cellule.getAttribute("rowspan"))-1;
  var t=this.coordonnees(this.cellActive);
  var i=this.indexLettre(t[0]);
  var etatSaisie=this.saisie;
  this.saisie=false;
  switch  (ev.keyCode) {
  case xcasBase.FLECHE_HAUTE :
    this.cell(t[0]+Math.max(t[1]-1,0));
    break;
  case xcasBase.FLECHE_BASSE :
    this.cell(t[0]+Math.min(t[1]+1+y,this.n_lign-1));
    break;
  case xcasBase.TABULATION :
    ev.preventDefault();
    this.cell(this.lettres[Math.min(i+1+x,this.n_col-1)]+t[1]);
    break;
  case xcasBase.FLECHE_GAUCHE :
      if (this.entree.selectionStart==0){
	this.cell(this.lettres[Math.max(i-1,0)]+t[1]);
	this.entree.setSelectionRange(0,0);
      }
      break;
  case xcasBase.FLECHE_DROITE :
    if (this.entree.selectionEnd==this.entree.value.length)
      this.cell(this.lettres[Math.min(i+1+x,this.n_col-1)]+t[1]); 
      break;
  } 
}

// touche sur this.entree (veiller au focus !)
xcasTableur.prototype.modeSaisie=function(ev){
 // touche ctrl 
  if (ev.ctrlKey){
    if (ev.keyCode==67){ 
      this.copy();
      this.saisie=false;
    } else if (ev.keyCode==86){
      this.paste();
    }
    return;
  }
  if (ev.keyCode==27 || ev.keycode==8){
    this.debutZone=this.zone[0]+this.zone[1];
    if (this.zone){
      this.colorieZone(this.zone[0],this.zone[2],this.zone[1],this.zone[3],"white");
      this.zone=new Array();
    }
    return;
  }
  if (ev.keyCode==13){ //entree
    this.saisie=false;
    return;
  }
  this.changeEntree=true;
  if (this.entree.value.charAt(0)!="=")
    return;
  if (ev.keyCode!=17)
    this.saisie=true;	
  return;
}

xcasTableur.prototype.vider=function(){
  this.vide(true);
}

xcasTableur.prototype.vide=function(sansConfirm){
  if (!sansConfirm){
    if (this.zone.length==0){
      if (!confirm("Vider la feuille ?"))
	return;
    } else {
      if(!confirm("vider la zone "+this.zone[0]+this.zone[1]+":"+this.zone[2]+this.zone[3]+" ?"))
	return;
    }
  }
  var i,j, dom_cellule, t,cell;
  var iDebut=0,iFin=this.nb_col-1;
  var jDebut=0,jFin=this.nb_lign-1;
  this.cell_marquees=[]; 
  if (this.zone.length>0){
    iDebut=this.indexLettre(this.zone[0]);
    jDebut=parseInt(this.zone[1]);
    iFin=this.indexLettre(this.zone[2]);
    jFin=parseInt(this.zone[3]);
  }
  for (i=iDebut; i<=iFin; i++)
    for (j=jDebut; j<=jFin ; j++){
      cell=this.lettres[i]+j;
      dom_cellule=document.getElementById(cell);
      dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
      dom_cellule.setAttribute("formule","");
      this.cell_marquees.push(cell);
      if (dom_cellule.firstChild)
	dom_cellule.removeChild(dom_cellule.firstChild);
    }
  if (this.zone.length==0){
    this.nb_col=0;
    this.nb_lign=0;
    this.entree.value="";
    this.cell("A0");
  } else {
    this.colorieZone(this.zone[0],this.zone[2],this.zone[1],this.zone[3],"white");
    this.zone=new Array();
  }
}

xcasTableur.prototype.annule=function(){
  if (this.entree.sauvegarde ){
    this.entree.value=this.entree.sauvegarde;
    this.entree.focus();
    return;
  }
  var i,t,dom_cellule;
  alert(this.cell_marquees);
  for (i=0; i<this.cell_marquees.length;i++){
      dom_cellule=document.getElementById("xc"+this.cell_marquees[i]);
      t=dom_cellule.getAttribute("ancienne");
      dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
      dom_cellule.setAttribute("formule",t);
  }
  this.calculeTab();
  this.nb_lign=this.nb_lign_sauv;
  this.nb_col=this.nb_col_sauv;
  return;
}


xcasTableur.prototype.subst_cell=function(){
 var dom_cellule=document.getElementById("xc"+this.cellActive);
 dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
 dom_cellule.setAttribute("formule",this.entree.value);
 return;
}

xcasTableur.prototype.fill_cell=function(statique){
  if (this.changeEntree)
    this.changeEntree=false;
  this.cell_marquees=[this.cellActive];
  this.cell_max(this.cellActive);
  this.subst_cell();
  var t=this.coordonnees(this.cellActive);
  this.calculeTab();
  if (statique)
    return;
  if (t[1]<this.n_lign-1){
    t[1]+=1;
    this.saisie=false;
    this.cell(t[0]+t[1]);
  }
  return;
}

xcasTableur.prototype.remplit_cellule=function(cellule,formule){
  var cell;
  if (cellule.charAt(0)=="x")
    cell=document.getElementById(cellule);
  else
    cell=document.getElementById("xc"+cellule);
  cell.setAttribute("formule",formule);
  this.cell_max(cellule);
}

xcasTableur.prototype.colorieZone=function(c1,c2,li1,li2,color){
  c1="xc"+c1;
  c2="xc"+c2;
  var i,td,col,i1,i2,iMin;
  for (i in this.cellZone)
    if (this.cellZone[i].style)
      this.cellZone[i].style.backgroundColor="white";
  this.cellZone=new Array();
  for (i in this.lettres){
    if (this.lettres[i]==c1)
      i1=i;
    if (this.lettres[i]==c2)
      i2=i;
  }
  iMin=Math.min(i1,i2);
  i2=Math.max(i1,i2);
  for (i=iMin ; i<=i2 ; i++){
    col=this.lettres[i];
    for (var j=li1;j<=li2;j++){
      td=document.getElementById(col+j);
      if (!td.style.backgroundColor || td.style.backgroundColor=="white"){
	this.cellZone.push(td);
	td.style.backgroundColor=color;
      }
    }
  }
}
  
xcasTableur.prototype.mouseDown=function(evt,td){
 // if(this.cellActive!=td.id) inutile ?
  //evt.preventDefault();
  evt.stopPropagation();
  if (this.debutZone=="copy")
    return false;
  if (this.zone){
    this.colorieZone(this.zone[0],this.zone[2],this.zone[1],this.zone[3],"white");
    this.zone=new Array();
  } else 
    this.colorieZone(this.zone[0],this.zone[2],this.zone[1],this.zone[3],"white");
  this.zone=this.coordonnees(td.id);
  this.zone.push("");
  this.zone.push("");
  this.debutZone=td.id;
  return false;
}
  
xcasTableur.prototype.mouseOver=function(evt,td){
  evt.preventDefault();
  if (this.cellDragStart!="")
    return false;
  if (this.debutZone=="" ||this.debutZone=="copy")
    return false;
  var ends=this.coordonnees(td.id);
  this.zone[2]=ends[0];
  this.zone[3]=ends[1];
  this.colorieZone(this.zone[0],ends[0],this.zone[1],ends[1],"#ffffe0");
  this.zoneCode=this.debutZone.substr(2)+":"+td.id.substr(2);
  return false;
}

xcasTableur.prototype.mouseUp=function(evt,td){
  this.entree.focus();
  if (this.debutZone=="")
    return false;
  if (this.debutZone!="copy")
    this.debutZone="";
  if (this.saisie)
    this.insereDansEntree(this.zoneCode);
  return false;
}

xcasTableur.prototype.dragStart=function(evt,img){ 
    this.cellDragStart=img.parentNode.parentNode.id;
 if (document.body.style.overflow!="")
    document.body.saveOverflow=document.body.style.overflow;
  else
    document.body.saveOverflow="visible";

}
  
xcasTableur.prototype.dragEnter=function(evt,td){
  var starts=this.coordonnees(this.cellDragStart);
  var ends=this.coordonnees(td.id);
  this.colorDrag(starts,ends,"#ffffac");
  this.poignee(document.getElementById("xc"+this.cellDragEnd));
  document.body.style.overflow="hidden";
  return false;
}

xcasTableur.prototype.colorDrag=function(starts,ends,color){
  var x=ends[0].charCodeAt(0)-starts[0].charCodeAt(0);
  var y=ends[1]-starts[1];
  var n=ends[1]+1;
  window.status=ends[0]+n+" "+this.n_lign;
  if (n<this.n_lign){
    var c=document.getElementById("xc"+ends[0]+n);
    if (c.scrollIntoView)
      c.scrollIntoView(false);
  }
  if (x<y){
    this.cellDragEnd=starts[0]+ends[1];
    this.colorieZone(starts[0],starts[0],starts[1],ends[1],color);
  }
  else {
    this.cellDragEnd=ends[0]+starts[1];
    this.colorieZone(starts[0],ends[0],starts[1],starts[1],color)
  }
}

xcasTableur.prototype.dragEnd=function(){
  var starts=this.coordonnees(this.cellDragStart);
  var ends=this.coordonnees(this.cellDragEnd);
  var st=this.cellDragStart;
  this.cellDragStart="";
  this.cellDragEnd="";
  document.body.style.overflow=document.body.saveOverflow;
  this.colorDrag(starts,ends,"white");
  if (starts[1]==ends[1])
    this.fill_right(ends[0]);
 else
   this.fill_bottom(ends[1]);
  this.cell(st);
}

xcasTableur.prototype.fill_bottom=function(nFin) {
  if (!nFin)
    var nFin=this.n_lign;
  else
    nFin+=1;
  this.cell_marquees=[];
  this.subst_cell();
  var cell=document.getElementById("xc"+this.cellActive);
  var formule=cell.getAttribute("formule");
  var nt=0;
  if (formule==parseInt(formule)){
    formule="="+this.cellActive+"+1";
    nt=1;
  }
  var reg_lettre=RegExp("[A-Z]","g");
  var letter=this.cellActive.match(reg_lettre);
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre=Number(this.cellActive.match(reg_nombre));
  var i=0;
  for (i=nombre+1 ; i<nFin ; i++){ 
    this.cell_marquees.push(letter+i);
    cell =document.getElementById("xc"+letter+i);
    cell.setAttribute("formule",this.translation(formule,0,i-nombre-nt));
  }
  i=nFin-1;
  this.cell_max(letter+i);
  this.calculeTab();
  this.saisie=false;
  return;
}

xcasTableur.prototype.fill_right=function(lfin) {
  this.cell_marquees=[];
  this.subst_cell();
  var cell=document.getElementById("xc"+this.cellActive);
  var formule=cell.getAttribute("formule");
  var nt=0;
  if (formule==parseInt(formule)){
    formule="="+this.cellActive+"+1";
    nt=1;
  }
  var cell=document.getElementById("xc"+this.cellActive);
  var reg_lettre=RegExp("[A-Z]","g");
  var letter=String(this.cellActive.match(reg_lettre));
  var code=Number(letter.charCodeAt(0));
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre=Number(this.cellActive.match(reg_nombre));
  var fin;
  if (lfin)
    fin=lfin.charCodeAt(0)-code+1;
  else
    fin=this.n_col;//=this.lettres[this.n_col-1].charCodeAt(0)-code+1;
  for (i=1 ; i<fin ; i++){
    letter=String.fromCharCode(code+i);
    this.cell_marquees.push(letter+nombre);
    cell =document.getElementById("xc"+letter+nombre);
    cell.setAttribute("formule",this.translation(formule,i-nt,0));
  }
  if (nombre<this.n_lign-1)
    nombre++;
  this.cell_max(this.lettres[this.n_col-1].slice(2)+nombre);
  this.calculeTab();
  this.saisie=false;
  return;
}


//translation de coordonnees de cellules

xcasTableur.prototype.translation=function(chaine,k,l){
  var chiffres=["0","1","2","3","4","5","6","7","8","9"];
  var i=0;
  var absLi,absCol=false;
  var refCol=-1,refLi;
  var nouvelleChaine="";
  var s,lettre;
  while (i<chaine.length){
    lettre=chaine.charAt(i);
    if (lettre=="$"){
	absCol=true;   
	nouvelleChaine+=lettre;
	i++;
    } else {
      if (refCol==-1){
	for (j in this.lettres)
	  if (lettre==this.lettres[j].slice(2)){
	    refCol=j;
	    break;
	  }
      } 
      if (refCol==-1){
	nouvelleChaine+=lettre;
	i++;
      }
      else {
	refLi=0;
	i++;
	lettre=chaine.charAt(i);
	if (lettre=="$"){
	  absLi=true;
	  i++;
	  lettre=chaine.charAt(i);
	}
	j=i;
	while (lettre in chiffres){
	  refLi=10*refLi+parseInt(lettre);
	  i++;
	  if (i<chaine.length)
	    lettre=chaine.charAt(i);
	  else
	    lettre="";
	}
	if (j!=i) {
	  if (!absCol)
	    refCol=parseInt(refCol)+k;
	  if (!absLi)
	    refLi+=l;
	  else
	    refLi="$"+refLi;
	  nouvelleChaine+=this.lettres[refCol].slice(2)+refLi;
	}
	else
	  nouvelleChaine+=lettre;
	refCol=-1;
	absCol=false;
	absLi=false;
      }
    }
  }
  return nouvelleChaine;
}

 

xcasTableur.prototype.copy=function(){
  saisie=false;
  if (this.zone.length!=0){
    this.debutZone="copy";
    this.colorieZone(this.zone[0],this.zone[2],this.zone[1],this.zone[3],"orange")
    return;
  }
  this.ref_copy=this.cellActive;
  dom_cellule=document.getElementById("xc"+this.cellActive);
  this.form_copy=this.entree.value;
  dom_cellule.setAttribute("formule",this.entree.value);
}


xcasTableur.prototype.paste=function(){
  if (this.zone.length==0){
    this.cell_marquees=[this.cellActive];
    this.cell_max(this.cellActive);
    var s=copyPaste(this,this.ref_copy,this.cellActive,this.form_copy);
    this.entree.value=s;
  } else {
    //this.debutZone="";
    var c0=this.cellZone[0].getAttribute("id");
    var tDZ=this.coordonnees(c0);
    var tCA=this.coordonnees(this.cellActive);
    var colDZ=this.indexLettre(tDZ[0]);
    var colCA=this.indexLettre(tCA[0]);
    var cell,id;
    for (var i in this.cellZone){
      id=this.cellZone[i].getAttribute("id");
      cell=coord(this,colDZ,tDZ[1],colCA,tCA[1],id);
      copyPaste(this,id,cell); 
      this.cell_max(cell);
    }
  }
  this.calculeTab(); 
  this.cell(this.cellActive);
  function coord(obj,colDZ,liDZ,colCA,liCA,cell){
    var t=obj.coordonnees(cell);
    var col=obj.indexLettre(t[0]);
    col=colCA+col-colDZ;
    var li=liCA+t[1]-liDZ;
    return obj.lettres[col]+li;
  }
  function copyPaste(obj,cell1,cell2,form_copy){
    var reg_lettre=RegExp("[A-E]","g");
    var lettre_cell=String(cell2.match(reg_lettre));
    var lettre_ref=String(cell1.match(reg_lettre));
    var reg_nombre=RegExp("[0-9].|[0-9]","g");
    var nombre_cell=Number(cell2.match(reg_nombre));
    var nombre_ref=Number(cell1.match(reg_nombre));
    var dom_cellule;
    if (cell2.charAt(0)=="x")
     dom_cellule=document.getElementById(cell2);
    else
     dom_cellule=document.getElementById("xc"+cell2);
    var c;
    if (!form_copy)
      var form_copy=document.getElementById(cell1).getAttribute("formule");
    c=obj.translation(form_copy,lettre_cell.charCodeAt(0)-lettre_ref.charCodeAt(0) ,nombre_cell-nombre_ref );
    dom_cellule.setAttribute("formule",c);
    return c;
  }
}

xcasTableur.prototype.spreadForm=function(i,j){
  var cell=document.getElementById(this.lettres[j]+i);
  var f=cell.getAttribute("formule");
  if (f!="" && f!=" ")
    return f;
  else
    return "null";
   // return "0";
}

xcasTableur.prototype.cellSubst=function(cell,s,m){
  var l,L,f;
  result=cell.getAttribute("result");
  var f=cell.getAttribute("formule");
  this.sauvegarde+=f+"`";
  if (result+""==s && result!="0")
    return;
  cell.setAttribute("result",s);
  if (cell.firstChild)
    cell.removeChild(cell.firstChild);
  l=cell.clientWidth+0;
  if (f!="" && f!=" "){
    this.insereMathML(cell,m,false,false);
    if (this.isIE){
      L=cell.firstChild.clientWidth+0;
      if (L>l){
       cell.style.width=L;
       this.tbody.style.width=tab.clientWidth+L-l;
      }
      else
       cell.style.width=l;
    }
  } 
  return;
}

xcasTableur.prototype.spreadSubst=function(i,j,s,m){
  var cell=document.getElementById(this.lettres[j]+i);
  this.cellSubst(cell,s,m);
  return;
}


xcasTableur.prototype.actualiseTab=function(){
  var t=this.retourGiac.split(new RegExp("`","g"));
  xcasBase.session_id=t[0];
  var i,j,k,id;
  var A="A";
  var s="<table><thead id='xcthead'><tr><th class='col1'></th>";
  for (j=0 ; j<this.n_col ; j++)
    s+="<th>"+String.fromCharCode(A.charCodeAt(0)+j)+"</th>";
  s+="</tr></thead>\n<tbody id='xctbody'>";

  //var s="";
  this.sauvegarde=""+this.nb_lign+"`"+this.nb_col+"`";
  var A="A";
  var cell=document.getElementById("xcA0"); 
  for (i=0; i<this.n_lign ; i++){
    s+="<tr>\n<td class='col1'>"+i+"</td>";
    for (j=0 ; j<this.n_col ; j++){
      id=this.lettres[j]+i;
      s+="<td id='"+id+"' ";
      s+=imite(cell);
      s+="onclick='xcasElement(this).cell(this.id)' ";
      s+="onmousedown='xcasElement(this).mouseDown(event,this)' ";
      s+="onmouseover='xcasElement(this).mouseOver(event,this)' ";
      s+="onmouseup='xcasElement(this).mouseUp(event,this)' ";     
      s+="ondragenter='xcasElement(this).dragEnter(event,this)'";
      if (i<this.nb_lign && j<this.nb_col){
	k=2*(i*this.nb_col+j)+1;
	if (t[k]!="null"){
	    s+="result='"+t[k]+"'><math display='inline'>"+t[k+1]+"</math>";
	}
	this.sauvegarde+=cell.getAttribute("formule")+"`";
	      } else
	s+=">";
      s+="</td>"; 
      cell=nextCell(cell);
    }
    s+="\n</tr>";
  }
  s+="</tbody>\n</table>\n";;
  //this.tbody.innerHTML=s;
  this.tableau.innerHTML=s;
  this.thead=document.getElementById("xcthead");
  this.tbody=document.getElementById("xctbody");
  for (i in this.cellZone)
    this.cellZone[i]=document.getElementById(this.cellZone[i].id);
  if (!window.ActiveXObject)
    setTimeout("xcasTableurActif.dimTable()",1000);
  function imite(cell){
    var s="";
    var attr=["index","style","rowspan","colspan","formule","groupe"];
    for (var i in attr){
      if (cell.hasAttribute(attr[i]))
	s+=attr[i]+"='"+cell.getAttribute(attr[i])+"' ";
    }
    return s;
  }
  function nextCell(c){    
    // on passe a la cellule suivante
    var cNext=successeur(c);
    if (cNext==null || !cNext || !cNext.tagName){ 
      // on est en fin de ligne
      cNext=c.parentNode;
      cNext=successeur(cNext);
      if (cNext){
	cNext=cNext.firstChild;
	if (!cNext.tagName)
	  cNext=successeur(cNext);
	cNext=successeur(cNext);
      } 
    }
    return cNext;
  }
  function successeur(c){ 
    var cNext=c.nextSibling; 
    while (cNext && !cNext.tagName)
      cNext=cNext.nextSibling;
    if (cNext)
      return cNext;
  }
}


xcasTableur.prototype.dimTable=function(){
 // largeur de la barre de défilement
  var l=this.tbody.offsetWidth-this.tbody.clientWidth;
  if (l==0)
    return;
  if (!this.hmax.isNaN){
    if (this.hmax<100)
      this.hmax=100;
    if (this.hmax>1500)
      this.hmax=1500;
    this.tableau.parentNode.style.maxHeight=this.hmax+21+"px";
    this.tbody.style.maxHeight=this.hmax+"px";

  }

 // this.tbody.style.maxHeight=this.hmax+"px";
 
  var tr=this.thead.firstChild;
  while (!tr.tagName)
    tr=tr.nextSibling;
  var ths=tr.childNodes;
  var largCell=0;
  var cellule;
  var m=this.n_lign-1;
  var n=ths.length;
  for (i=1; i<=this.n_col ;i++) {
    cellule=document.getElementById(this.lettres[i-1]+m);
    cellule.style.width="";
  } 
  for (i=1; i<=this.n_col ;i++) {
    cellule=document.getElementById(this.lettres[i-1]+m);
    largCell=cellule.offsetWidth;
    // correction empirique
    if (largCell==parseInt(cellule.style.width)+1)
      largCell=largCell-1;
    cellule.style.width=largCell+"px";
  }
  this.thead.className="bloc";
  this.tbody.className="bloc";
  var conteneur=this.tableau.parentNode;
  conteneur.style.overflowY="hidden";
  for (i=1; i<=this.n_col ;i++) {
    cellule=document.getElementById(this.lettres[i-1]+m);
    largCell=cellule.offsetWidth;
    // correction empirique
    if (largCell==parseInt(cellule.style.width)+1)
      largCell=largCell-1;
    ths[i].style.minWidth=largCell-1+"px";
    ths[i].style.width=largCell+"px";
  }
  if (ths.length<=this.n_col+1){
   var th=document.createElement("th");
   ths[ths.length-1].parentNode.appendChild(th);
   th.style.minWidth=l+"px";
   th.style.color="red";
  }
}

xcasTableur.prototype.spreadsheet=function(){
  var spreadheet=new String();
  spreadsheet="spreadsheet[";
  var cell=document.getElementById('xcAO');
  var i=0;
  var j=0;     
  var der_col=this.nb_col-1, der_lign=this.nb_lign-1;
  var formule="";
  for (i=0 ; i<this.nb_lign ; i++){
    spreadsheet+="[";
    for (j=0 ; j<der_col ; j++){
      formule=this.spreadForm(i,j);
      spreadsheet+="[regrouper("+formule+"),1,0],";
    }
    formule=this.spreadForm(i,j);
    spreadsheet+="[regrouper("+formule+"),1,0]]";
    if (i==der_lign)
      spreadsheet+="]";
    else
      spreadsheet+=",";
  }
  return spreadsheet;
}

xcasTableur.prototype.calculeCache=function(str){
  var  spreadsheet;
  if (!str)
    spreadsheet=this.spreadsheet();
  else
    spreadsheet=str;
  if (spreadsheet.length>10000)
    this.progress.setAttribute("style","display:block"); 
  var s;
  if (this.exact)
    s=this.reqTableur(spreadsheet); 
  else
    s=this.reqTableurApprox(spreadsheet); 
  if (s.indexOf("Fatal error")>=0){
    alert("DUREE EXCESSIVE. CALCUL REFUSE. Passez en mode numerique")
    return;
  }
  this.retourGiac=s;
  this.progress.setAttribute("style","display:none");
}
  
xcasTableur.prototype.calculeTab=function(str){
  this.stopAssist();
  s= this.calculeCache(str); 
  this.actualiseTab();
  return ;
}

xcasTableur.prototype.restaure=function(){
  if (this.sauvegarde=="")
    return;
  var tab=this.sauvegarde.split(new RegExp("`","g"));
  if (parseInt(tab[0])>this.n_lign){
    alert("Il faut au moins "+tab[0]+" lignes.");
    return;
  }
  if (parseInt(tab[1])>this.n_col){
    alert("Il faut au moins "+tab[1]+" colonnes.");
    return;
  }
  this.nb_lign=tab[0];
  this.nb_col=tab[1];
  var cell;
  for (i=0; i<this.nb_lign ; i++)
    for (j=0 ; j<this.nb_col ; j++){
      cell=document.getElementById(this.lettres[j]+i);
      k=2+i*this.nb_col+j;
      if (tab[k])
	cell.setAttribute("formule",tab[k]);
      else
	cell.setAttribute("formule","");
    }
  this.calculeTab();
}

// preferences utilisateur

xcasTableur.prototype.pref=function(bool,x,y){
  xcasTableurActif=this;
  if (x)
    var p=new xcasPopUp(x,y,500);
  else
    var p=new xcasPopUp(200,100,500);
  p.titre.innerHTML="Préférences";
  var div=this.outils.parentNode;
  var w=div.style.width;
  if (!div.style.width)
    w="900px";
  var approx="checked='checked'";
  if (xcasTableurActif.exact)
    approx="";
 var compl="checked='checked'";
  var cNouveau=0;
  var s="<p>Largeur : <input value='"+w+
  "' onchange='xcasTableurActif.larg(this)'><br><br>"+
  "Hauteur max : <input value='"+this.hmax+
  "px' onchange='xcasTableurActif.hmax=parseInt(this.value);xcasTableurActif.actualiseTab();'><br>"+
  "<br>Nombre de lignes : <input value='"+
  this.n_lign+
  "' onchange='xcasTableurActif.redim(this,0)'></p><p>Nombre de colonnes (1-15): <input value='"+
  this.n_col+
  "' onchange='xcasTableurActif.redim(this,1)'></p><hr><p>";
  p.contenu.innerHTML=s;
  s="Calculs approchés <input type='checkbox' "+approx+
  "' onchange='xcasTableurActif.exact=!xcasTableurActif.exact'>"+
  " Précision <input width='3'onblur='xcasPrecision(this)' value='"+
  xcasRequete("DIGITS")+"'></p><hr>\n";
  p.contenu.innerHTML+=s;
  if (xcasRequete("solve(x^2+1=0)")=="[]"){
    compl="";
    cNouveau=1;
  }
  s="<p>Résoudre les équations dans C  <input type='checkbox' "+compl+
  "' onchange='xcasChangeComplexMode("+cNouveau+")'></p>"
  p.contenu.innerHTML+=s;
}

function xcasPrecision(input){
  var x=parseInt(input.value);
  if (!isNaN(x))
    xcasRequete("DIGITS:="+x);
}

function xcasChangeComplexMode(mode){
  xcasRequete("complex_mode:="+mode);
}

xcasTableur.prototype.larg=function(input){
  var div=this.outils.parentNode;
  var sauve="900px";
  if (div.style.width)
    sauve=div.style.width;
  div.style.width=input.value;
  if (!confirm("Confirmer la nouvelle largeur")){
    div.style.width=sauve; 
    input.value=sauve;
  }
}

xcasTableur.prototype.redim=function(input,direction){
  var x=parseInt(input.value);
  if (isNaN(x))
    return;
  var n_lign=this.n_lign;
  var n_col=this.n_col;
  var div=this.outils.parentNode;
  var w="900px";
  if (div.style.width)
    w=div.style.width;
  while (div.firstChild)
    div.removeChild(div.firstChild);
  var tableur; 
  var s=this.sauvegarde;
  if (direction==0){
    x=Math.max(x,this.nb_lign);
    tableur=new xcasTableur(div.id,x,n_col);
  }
  else{
    x=Math.min(x,15);
    x=Math.max(x,this.nb_col);
    tableur=new xcasTableur(div.id,n_lign,x);
  }
  input.value=x;
  div=tableur.outils.parentNode;
  div.style.width=w;
  tableur.sauvegarde=s;
  tableur.restaure();
  var x=parseInt(input.parentNode.js.left);
  var y=parseInt(input.parentNode.js.top);
  input.parentNode.js.supprime();
  tableur.pref(true,x,y);
}



// sauve et restaure

var xcasTableurOuvrirPopup;

xcasTableur.prototype.sauve=function(){
  var leMarqueur="@`";
  if (navigator.userAgent.indexOf("Firefox")!=-1){
    /*if(window.location.protocol=="file:"){
      alert("Ne fonctionne pas hors ligne sous firefox");
      return;
      }*/    
  }
  var s=prompt("nom de la feuille ?",this.cle);
  if (!s)
    return;
  this.cle=s;

  s=leMarqueur+this.prog+leMarqueur+this.sauvegarde;
  try
    {
      localStorage.setItem("TX"+this.cle, s); // this.sauvegarde);
    }
  catch(err)
    {
      alert("fonction non supportée par le navigateur");
    }
}
		
xcasCharge=function(i){
  var leMarqueur="@`";
  var cle=localStorage.key(i);
  var s=localStorage.getItem(cle);
  var t=s.split(leMarqueur);
  xcasTableurActif.prog=t[1];
  xcasTableurActif.sauvegarde=t[2];
//  xcasTableurActif.sauvegarde=localStorage.getItem(cle);
  xcasTableurActif.restaure();
}

xcasSupprime=function(i){
  var cle=localStorage.key(i);
  if (confirm("supprimer "+cle+" ?")){
    localStorage.removeItem(cle);
    xcasTableurOuvrirPopup.supprime();
    xcasTableurActif.ouvrir();
  }
}

xcasTableur.prototype.ouvrir=function(popup){
  function testeLS(){	
    if (localStorage) {
      return true;
    } else {
      alert("Vous ne pourrez pas stocker les données ! Mettre à jour votre navigateur !");
      return false;
    }
  }
  function bouton(valeur,effet){
    var input=document.createElement("input");
    input.type="button";
    input.setAttribute("onclick",effet);
    input.value=valeur;
    return input;
  }
  function ligne(table){
    var tr=document.createElement("tr");
    var td,texte;
    table.appendChild(tr);
    for (var i=1; i<arguments.length ; i++){
      td=document.createElement("td");
      tr.appendChild(td);
      td.appendChild(arguments[i]);
    }
  }
 // if (xcasTableurOuvrirPopup.supprime);
   // xcasTableurOuvrirPopup.supprime();
  if (!testeLS())
    return;
  var n=localStorage.length;
  if (n==0){
    alert("Aucune feuille stockée"); 
    return;
  }
  var cle,p,a,br,test=false;
  xcasTableurActif=this;
  xcasTableurOuvrirPopup=new xcasPopUp(400,150);
  xcasTableurOuvrirPopup.titre.innerHTML="Choisir une feuille";
  p=xcasTableurOuvrirPopup.contenu;
  p.innerHTML="";
  var table=document.createElement("table");
  p.appendChild(table);
  var b1,b2;
  for (var i=0 ; i<n ; i++){
    cle=localStorage.key(i);
    if (cle.substring(0,2)=="TX"){
      cle=cle.substring(2);
      texte=document.createTextNode(cle);
      b1=bouton("Charger","xcasCharge("+i+");");
      b2=bouton("supprimer","xcasSupprime("+i+");");
      ligne(table,texte,b1,b2);
    }
  }
}

// retour du calcul

xcasTableur.prototype.retour=function(quoi, col, li){
  if (!isNaN(col))
    col=this.lettres[col];
  else
    col="xc"+col;
  var c=document.getElementById(col+li);
  if (quoi=="f")
    return c.getAttribute("formule");
  else
    return c.getAttribute("result");
}

xcasTableur.prototype.formule=function(col, li){
  return this.retour("f",col,li);
}

xcasTableur.prototype.resultat=function(col, li){
  return this.retour("r",col,li);
}

// ardoise

xcasTableur.prototype.afficheArdoise=function(){
  var popup=new xcasPopUp(400,60,350);
  popup.titre.innerHTML="Ardoise xcas";
  var c=new xcasConsole(popup.contenu);
}

// cadre de programmation

xcasTableur.prototype.afficheProg=function(){
  var popup=new xcasPopUp(400,60,350);
  popup.titre.innerHTML="Fenêtre de programmation"
  var t=document.createElement("textarea");
  t.value=this.prog;
  popup.contenu.appendChild(t);
  t.onblur=function(){xcasTableurActif.prog=this.value;xcasRequete(this.value);};
}

// aide

xcasTableur.prototype.aide=function(){
  var p=new xcasPopUp(300,80,500);
  p.titre.innerHTML="Aide tableur";
  p.contenu.innerHTML="<object data='"+CHEMIN+"xcas/tableur.html' type='text/html'></object>";
}

// gestion de l'assistant

xcasTableur.prototype.assiste=function(sel){
  if (!this.saisie){
    if (this.entree.value!=""){
      alert("Une formule doit commencer par =");
      sel.selectedIndex=0;
      sel.blur();
      return;
    } else {
      this.entree.value="=";
      this.saisie=true;
    }
  }
  var i=sel.selectedIndex;
  if (i==0)
    return;
  var t=this.commandes[i];
  if (t.charAt)
    return;
  this.spanAssist.innerHTML="";
  var s=" ";//t[0]; 
  if (t[1]=="linsolve"){
    this.combi(this.spanAssist,"xcarg",t[2],true);
  } else
  if (t[1]=="matrice"){
    this.combi(this.spanAssist,"xcarg",t[2],false);
  } else {
    for (var j=2; j<t.length ; j++){
      if (t[j].charAt)
	s+=" "+t[j]+" <input id='xcarg"+j+"' onfocus='xcasTableurActif.entree=this'>";
      else
	s+=" "+t[j][0]+" <input value='"+t[j][1]+"' id='xcarg"+j+"' onfocus='xcasTableurActif.entree=this'>";
    }
    this.spanAssist.innerHTML+=s;
  }
  var input=this.bouton(this.spanAssist,"ok");
  input.className="ok";
  input.onclick=function(){xcasTableurActif.comAssist(i)};
  document.getElementById("xcarg2").focus();
}

xcasTableur.prototype.comAssist=function(i){
  function li2matrix(li){
    var li1="",s="";
    // elimination des blancs
    var b=false;
    for (var j=0; j<li.length; j++){
      if (li.charAt(j)!=" ")
	b=false;
      if (!b)
	li1+=li.charAt(j);
      if (li.charAt(j)==" ")
	b=true;
    }
    if (li1!="")
      li=li1.split(" ");
    else
      li=new Array();
    if (li.length>0){
      s+="["+li[0];
      for (j=1 ; j<li.length ; j++)
	if (li[j]!="")
	  s+=","+li[j];
      s+="]";
    }
    return s;
  }
  var t=this.commandes[i];
  var s="",sprim="",i=0;
  if (t[1]=="linsolve"){
    s="linsolve([";
    sprim="";
    for (i=0 ; i<4 ; i++){
      sprim=document.getElementById("xcarg"+i).value;
      spim=sprim.replace(/ /g,"");
      if (sprim!=""){
	if (i!=0)
	  s+=",";
	s+=sprim;
      }  
    }
    s+="],"+li2matrix(document.getElementById("xcarg4").value)+")";
  }
  else  if( t[1]=="matrice"){
    var li="",li1="";
    s="[";
    for (i=0 ; i<4 ; i++){
      li=document.getElementById("xcarg"+i).value;
      li1=li2matrix(li);
      if (li1!="") {
	if (i>0)
	  s+=",";
	s+=li1;
      }
    }
    s+="]";
  } else {
    s=t[1]+"(";
    for (var j=2; j<t.length ; j++){
      if (j>2)
	s+=",";
      s+=document.getElementById("xcarg"+j).value;
    }
    s+=")";
  }
  if (t[1]=="quantile")
    s+="[0]";
  this.entree=this.entreeSauve;
  this.entree.value+=s;
  this.stopAssist();
}


// graphiques

xcasTableur.prototype.assistGraph=function(){
  if (this.divAssistGraph.style.display=="block"){
    this.stopAssist(true);
    return;
  }
  this.stopAssist(true);
  this.divAssistGraph.style.display="block";
  if (this.exact){
   // Il faut d'abord recalculer la feuille en mode numerique
      this.exact=false; 
      this.calculeCache(); 
      this.exact=true;
  }
  this.spanAssistGraph.innerHTML="";
  var commandes=[
		 ["Graphique :",""],
		 ["Aires","aire"],
		 ["Camembert","camembert"],
		 ["Courbes","courbes"],
		 ["Barres","barres"],
		 ["Barres multiples","barresCote"],
		 ["Histogramme","histogramme"],
		 ["Histogramme automatique","histauto"],
		 ["Nuage de points","nuage"]
		 ];
  var select=document.createElement("select");
  select.setAttribute("id","xcgselect");
  select.onchange=function(){document.getElementById('xcgspan').style.display='inline';document.getElementById('xcgplage').select();};
  this.optionSelect(select,commandes);
  this.spanAssistGraph.appendChild(select);
  span=document.createElement("span");
  span.setAttribute("id","xcgspan");
  this.spanAssistGraph.appendChild(span);
  var dia1={
    xcgplage:[" Plage de données : ",this.zoneCode],
  }
  var xcgplage=this.dialogue(span,dia1);
  xcgplage.onkeyup=function(){this.value=this.value.toUpperCase();};
  var legendes=[
		 ["Titre","Titre du graphique"],
		 ["Légende des x","Titre éventuel abscisses"],
		 ["Légende des y","Titre éventuel ordonnées"],
		 ];
  this.combi(span,"xcgl",legendes);

  input=new this.bouton(span,"ok");
  input.className="ok";
  input.onclick=function(){xcasTableurActif.graphique();};

  // capture des clics sur cellule
  this.saisie=true;
  this.entree=xcgplage;//document.getElementById("xcgplage");
  //xcgplage.focus();
  setTimeout("xcasTableurActif.entree.focus()",1000);
  this.include(CHEMIN+"jsx/jsxgraphcore.js");
  this.include(CHEMIN+"graphique/graphique.js");
  this.include(CHEMIN+"graphique/graphiquepopup.js");
}

xcasTableur.prototype.bouton=function(conteneur,valeur){
  var input=document.createElement("input");
  inputclassName="ok";
  input.value=valeur;
  input.type="button";
  conteneur.appendChild(input);
  return input;
}

xcasTableur.prototype.combi=function(conteneur,prefixe,legendes,focus){
  var select=document.createElement("select");
  select.prefixe=prefixe;
  conteneur.appendChild(select);
  var option,input, txt="";
  for (var i in legendes){
    option=document.createElement("option");
    txt=document.createTextNode(legendes[i][0]);
    option.appendChild(txt);
    select.appendChild(option);
    input=document.createElement("input");
    input.setAttribute("id",prefixe+i);
    if (i==0){
      input.style.display="inline";
      input.focus;
      select.cible=input;
    }
    else
      input.style.display="none";
    input.value=legendes[i][1];
    input.className="graph";
    if (focus)
      input.onfocus=function(){xcasTableurActif.entree=this;};
    conteneur.appendChild(input); 
  }
  select.onchange=function(){
    if (this.cible)
      this.cible.style.display="none";
    var cible=document.getElementById(this.prefixe+this.selectedIndex);
    if (cible){
      this.cible=cible;
      cible.style.display="inline";
      cible.select();
    }
  }
  
 /* setTimeout("xcasTableurActif.selectOpt('"+prefixe+"')",1000);*/

  /*var cible0= document.getElementById(prefixe+"0");
  cible0.style.display="inline";
  select.cible=cible0;
  cible0.focus();
  */
}


xcasTableur.prototype.optionSelect=function(select,opt){
  var option,txt;
  for (var i in opt){
    option=document.createElement("option");
    option.value=opt[i][1];
    txt=document.createTextNode(opt[i][0]);
    option.appendChild(txt);
    select.appendChild(option);
  }
}


xcasTableur.prototype.dialogue=function(span,dia,classe) { 
  var input,txt; 
  for (var a in dia){ 
    if (dia[a][0]!=""){
      txt=document.createTextNode(dia[a][0]);
      span.appendChild(txt);
    }
    input=document.createElement("input");
    input.setAttribute("id",a);
    input.value=dia[a][1];
    if (classe)
      input.className=classe;
    span.appendChild(input); 
  }
  return input;
}

xcasTableur.prototype.graphique=function(){
  var h,i,j,k,l,m;
  var s;
  // si ivar==1, lecture verticale
  var ivar=1,jvar;
  var plage=document.getElementById("xcgplage").value;
  // remplace les ; et les blancs par des virgules
  plage=plage.replace(RegExp(";","g"),",");
  plage=plage.replace(RegExp(" ","g"),",");
  var tab=plage.split(","); 
  for (i in tab)
    tab[i]=zone(this,tab[i]);
  if (tab[0][1][0]-tab[0][0][0]> tab[0][1][1]-tab[0][0][1])
    ivar=0;
  jvar=1-ivar;
  var g = new graphiqueFenetre(10,20,500,400);
  var titre=document.getElementById("xcgl0").value;
  if (titre && titre!="")
    g.dom.titre.innerHTML=titre;
  var t=this.retourGiac.split(new RegExp("`","g"));
  g.setStyle({strokeWidth:1,points:false,face:"square",color1:"red",color2:"blue"});
  // Cherche la premiere donnee numerique
  // variante qui recherche la premiere donnee num dans n'importe quelle rangee
  // tab[i] zone i
  // tab[i][0]==[numligne,numcol] premiere cellule de la zone i
  // tab[i][1] derniere cellule de la zone i
  var br=false;
  for (i in tab){
    for (l=tab[i][0][ivar]; l<=tab[i][1][ivar] ; l++){
      for (m=tab[i][0][jvar]; m<=tab[i][1][jvar] ; m++){
	if (jvar==0) 
	  k=cell(this,l,m);
	else
	  k=cell(this,m,l);
	//alert(l+" - "+m+" - "+ t[k]);
	if (!isNaN(parseFloat(t[k]))){
	  br=true;
	  break;
	}
      }
      if (br)
	break;
    }
    if (br)
      break;
  }
 // legende des axes
  var select=document.getElementById("xcgselect");
  var gtype=select.options[select.selectedIndex].value;
  if (gtype!="camembert"){
    if (l>tab[0][0][ivar]) {
      if (ivar==0)
	g.setHorizontalTitle(t[cell(this,l-1,tab[0][0][jvar])]);
      else
	g.setHorizontalTitle(t[cell(this,tab[0][0][jvar],l-1)]);
    }
    else
      g.setHorizontalTitle(document.getElementById("xcgl1").value);
    g.setVerticalTitle(document.getElementById("xcgl2").value);
  }
  
  // taille du tableau de donnees
  var xy=new Array();
  for (i=tab[0][0][ivar]; i<=tab[0][1][ivar] ; i++)
    xy.push(new Array());
  // construction du tableau de donnees
  // et de la legende generale
  var legend=new Object();
  m=0;
  for (h in tab){
    for (j=tab[h][0][jvar] ; j<=tab[h][1][jvar] ; j++){
      if (m>0){
	if (l>0){
	  if (jvar==0) 
	    s=t[cell(this,l-1,j)];
	  else
	    s=t[cell(this,j,l-1)];
	  legend["title"+m]=s	
	} else
	  legend["title"+m]="courbe "+m;
	legend["color"+m]=g.jsxStyle["color"+m];
	
      }
      for (i=l; i<=tab[h][1][ivar]; i++){
	if (jvar==0)
	  k=cell(this,i,j);
	else
	  k=cell(this,j,i);
	if (t[k] && t[k]!="null"){
	  // pb : ne pousse que les donnees numeriques
	  if (isNaN(parseFloat(t[k])))
	    xy[m].push(t[k]);
	  else
	    xy[m].push(parseFloat(t[k]));
	}
      }
      m+=1;
    }
  }

  // construction du graphique
  // legende generale
  if (gtype!="camembert")
    g.setLegend(legend);
  this.stopAssist(true);
  switch (gtype){
  case "aire":
    g.aire(0,0,xy);
    break;
  case "camembert":
    var lgnd={};
    g.completeListeNumerotee(lgnd,"title",1,xy.length,xy[0]);
    g.completeListeNumerotee(lgnd,"color",1,xy.length,g.pieColors);		
    g.setLegend(lgnd);
    g.camembert(0,0,xy);
    legend=xy[0];
    break;
  case "courbes":
    g.courbe(0,0,xy);
    break;
  case "barres":
    g.barres(0,0,xy);
    break;
  case "barresCote":
    g.barresCote(0,0,xy);
    break;
  case "histogramme":
    g.histogramme(0,0,xy);
    break;
  case "histauto":
    // bornes inf et sup
    var min=xy[0][0],max=min;
    for (i=0 ; i<xy[0].length ; i++){
      min=Math.min(xy[0][i],min);
      max=Math.max(xy[0][i],max);
    }
    var nclass=Math.min(Math.round(xy[0].length/5),9);
    //alert(nclass);
    var borne, bornes=new Array(), effectifs=new Array(), amplitude=(max-min)/nclass;
    for (i=0; i<nclass ; i++){
      borne=min+i*amplitude;
      bornes.push(borne);
      effectifs.push(0);
    }
    bornes.push(max);
    //alert(min+" "+max+" "+amplitude);
    for (i=0 ; i<xy[0].length ; i++){
      j=0;
      while (xy[0][i]>bornes[j])
	j+=1;
      j=Math.max(0,j-1);
      effectifs[j]+=1;	
    }
    //alert(bornes+"---"+effectifs);
    g.histogramme(0,0,[bornes,effectifs]);
    break;
  case "nuage":
    g.nuage(0,0,xy);
    break;
  default:
    alert("Sélectionnez le type de graphique !");
  }
  // ---- fonctions de service -----
  function cell(obj,i,j){
      return 2*(i*obj.nb_col+j)+1;
  }
  function numCol(lettre){
    var A="A";
    return lettre.charCodeAt(0)-A.charCodeAt(0);
  }
  function zone(xt,ref){
    var t=ref.split(":");
    var c0,c1;
    if (t.length>1){
      // zone de type cellule1:cellule2
      c0=xt.coordonnees(t[0]);
      c1=xt.coordonnees(t[1]);
      c0[0]=numCol(c0[0]);
      c1[0]=numCol(c1[0]);
    } else {
      // ligne ou colonne
      c0=xt.coordonnees(ref); 
      c1=xt.coordonnees(ref); 
      if (c0[0]=="") {
	// ligne
	c0[0]=0;
	c1[0]=xt.nb_col-1;
      } else {
	// colonne
	c0[0]=numCol(c0[0]);
	c0[1]=0;
	c1[0]=numCol(c1[0]);
	c1[1]=xt.nb_lign-1;
      }
    }
    return [c0,c1];
  }
}

xcasAttendre=false;
