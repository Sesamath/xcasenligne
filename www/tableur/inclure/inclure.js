if (!CHEMIN)
  var CHEMIN=chemin("inclure.js");


function chemin(sourceJs){
  var scripts=document.getElementsByTagName("script");
  var adresse="",fin=-1;
  for (var i in scripts){
    adresse=scripts[i].getAttribute("src");
    fin=adresse.indexOf(sourceJs, 0);
    if (fin!=-1)
     break;
  }
  if (fin==-1)
    return "";
  return adresse.substring(0,fin);
}



// inclusion a la volee fichier de programmes js ou de feuilles de style
// ATTENTION : il ne faut pas avoir besoin tout de suite des fonctions javascript import�es
// exemple inclure("mon_rep/mon_fichier.js","mon_rep/mon_fichier.css");

function inclure(){
  // pour ne pas inclure 2 foix le meme fichier
  function testeDejaInclus(fileName,nom){
    var i=0, s="",t="";
    var inclus=document.getElementsByTagName(nom);
    if (nom=="link")
      t="href";
    else
      t="src";
    for (i=0 ; i<inclus.length ; i++){
      s=inclus[i][t];
      if (s.indexOf(fileName, 0)!=-1)
	dejaInclus=true;
    }
    return dejaInclus;
  }
  var filename="";
  var head=document.getElementsByTagName("head");
  if (head.length==0)
    return;
  for (var i in arguments){
    fileName=arguments[i];
    //teste si fileName est une feuille de style
    var css=(fileName.indexOf("css", 0)!=-1);
    var element,nom="", type="",dejaInclus=false;
    if (css){
      nom="link";
      type="text/css"; 
    }
    else {
      nom="script";
      type="text/javascript"; 
    }
    dejaInclus=testeDejaInclus(fileName,nom);
    if (!dejaInclus){
      element=document.createElement(nom);
      element.type=type;
      if (css){
	element.href =CHEMIN+fileName;
	element.rel="stylesheet";   
      } else 
	element.src =CHEMIN+fileName;
      head[0].appendChild(element);
    }
  }
}


