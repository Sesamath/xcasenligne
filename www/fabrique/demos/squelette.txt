xhtml@`Sesamath@`maFonction(x,y):={
// variables locales
local z,t;
// taper les instructions ici :
z:=x^2;
// test
si z==y
 alors 
  retourne &#34;oui&#34;;
sinon
  retourne &#34;non&#34;;
fsi;
}
@`<div style=\"text-align: center;\">
</div>
<h1 style=\"text-align: center;\">
 &#160;Squelette d\'application texte
 <br/>
</h1>
<hr style=\"width: 100%; height: 2px;\"/>
<br/>
<p>
 Taper la consigne ici. Le distributeur affiche une donn&#233;e compl&#233;mentaire ci-dessous :
 <br/>
</p>
<p contenteditable=\"false\" class=\"sortie\" onmousedown=\"return false\" onmouseup=\"fabEcrit(this)\" horsligne=\"1\" id=\"fab0\" title=\"fab0\" oncontextmenu=\"fabEditeAction(this)\" texte=\"4\" style=\"height: 21px;\">
 <math display=\"block\" index=\"undefined\" xmlns=\'http://www.w3.org/1998/Math/MathML\' >
  <mn>
   4
  </mn>
 </math>
</p>
<p>
 L\'&#233;l&#232;ve entre sa r&#233;ponse ici :&#160; 
 <fab contenteditable=\"false\">
  &#160;&#160;
  <input id=\"fab1\" onclick=\"fabEcrit(this)\" onchange=\"\" onkeypress=\"this.focus()\" onkeyup=\"fabChapeau(this,event)\" title=\"fab1\" oncontextmenu=\"fabEditeAction(this)\"/>
  &#160;
 </fab>
 <fab contenteditable=\"false\">
  &#160;&#160;
  <input type=\"button\" id=\"fab2\" value=\"ok\" onclick=\"xc.eval(\'maFonction\',\'fab1\',\'fab0\');xc.imprimeMml(\'fab3\',0,true,true);xc.imprimeSvg(fab.graphique,true);\" title=\"fab2\" oncontextmenu=\"fabEditeAction(this)\" cible=\"fab3\" arguments=\"fab1 fab0\" xcas=\"maFonction\"/>
  &#160;
 </fab>
 &#160;
</p>
<p>
 Retour de la fonction xcas ici :
 <br type=\"_moz\"/>
</p>
<p contenteditable=\"false\" class=\"sortie\" onmousedown=\"return false\" onmouseup=\"fabEcrit(this)\" horsligne=\"1\" id=\"fab3\" title=\"fab3\" oncontextmenu=\"fabEditeAction(this)\" texte=\"\'oui\'\" style=\"height: 21px;\">
  ...
</p>
<p>
 Distributeur :
 <br type=\"_moz\"/>
</p>
<div contenteditable=\"false\" class=\"distrib\" oncontextmenu=\"fabEditeDistributeur()\">
 <p class=\"ovale\">
  <img onclick=\"fabExo(-1)\" src=\"img/bouton_gauche.png\">
  </img>
  <span id=\"numexo\">
  </span>
  <input id=\"listeExos\" onchange=\"fabDistribue()\" onfocus=\"fabEditeExos(true,this)\" title=\"listeExos\" index=\"0\" liste=\"4`9`16`25\" recepteurs=\"fab0\" style=\"background-color: white;\"/>
  <img onclick=\"fabExo(1)\" src=\"img/bouton_droit.png\">
  </img>
 </p>
</div>
<p>
 ...
</p>
@`fabInitialiseSvg(-5,-5,5,5,10);@`4@`false@`false@`