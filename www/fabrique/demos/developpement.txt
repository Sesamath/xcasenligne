xhtml@`Sesamath@`f(a,b):={
  si sommet(a)!=sommet(a+b)
   alors
    retourne &#34;rouge&#34;,&#34;Tu dois entrer une somme&#34;,&#34;rouge&#34;,a;
  fsi
  si normal(a-b)==0 alors
    retourne &#34;vert&#34;,&#34;Bravo&#34;,&#34;vert&#34;,a;
  sinon
    retourne &#34;rouge&#34;,&#34;Essaye encore&#34;,&#34;rouge&#34;,a;
  fsi
}@`<div style=\"text-align: center;\">
</div>
<h1 style=\"text-align: center;\">
 &#160;D&#233;veloppement
</h1>
<hr style=\"width: 100%; height: 2px;\"/>
<p>
 Ton travail consiste &#224; d&#233;velopper l\'expression suivante :
 <br type=\"_moz\"/>
</p>
<p contenteditable=\"false\" class=\"sortie\" onmousedown=\"return false\" onmouseup=\"fabEcrit(this)\" horsligne=\"1\" id=\"fab0\" title=\"fab0\" oncontextmenu=\"fabEditeAction(this)\" texte=\"(a+b)*a\" style=\"\">
 <math display=\"block\" index=\"undefined\" xmlns=\'http://www.w3.org/1998/Math/MathML\' >
  <mo>
   (
  </mo>
  <mi>
   a
  </mi>
  <mo>
   +
  </mo>
  <mi>
   b
  </mi>
  <mo>
   )
  </mo>
  <mi>
   a
  </mi>
 </math>
</p>
<p>
 Entre l\'expression d&#233;velopp&#233;e : 
 <fab contenteditable=\"false\">
  &#160;&#160;
  <input id=\"fab1\" onclick=\"fabEcrit(this)\" onchange=\"\" onkeypress=\"this.focus()\" onkeyup=\"fabChapeau(this,event)\" title=\"fab1\" oncontextmenu=\"fabEditeAction(this)\"/>
  &#160;
 </fab>
 <fab contenteditable=\"false\">
  &#160;&#160;
  <input type=\"button\" id=\"fab2\" value=\"ok\" onclick=\"xc.eval(\'f\',\'fab1\',\'fab0\');xc.imprimeMml(\'fab3\',0,true,true);xc.imprimeSvg(fab.graphique,true);xc.imprimeMml(\'fab4\',1,true,true);xc.imprimeSvg(fab.graphique,true);\" title=\"fab2\" oncontextmenu=\"fabEditeAction(this)\" cible=\"fab3 fab4\" arguments=\"fab1 fab0\" xcas=\"f\"/>
  &#160;
 </fab>
 <fab contenteditable=\"false\">
  &#160;
  <span contenteditable=\"false\" class=\"sortieDL\" onmousedown=\"return false\" onmouseup=\"fabEcrit(this)\" id=\"fab3\" title=\"fab3\" oncontextmenu=\"fabEditeAction(this)\">
    ...
  </span>
  &#160;&#160;
 </fab>
 <br/>
</p>
<p contenteditable=\"false\" class=\"sortie\" onmousedown=\"return false\" onmouseup=\"fabEcrit(this)\" horsligne=\"1\" id=\"fab4\" title=\"fab4\" oncontextmenu=\"fabEditeAction(this)\">
  ...
</p>
<p>
 <br type=\"_moz\"/>
</p>
<div contenteditable=\"false\" class=\"distrib\" oncontextmenu=\"fabEditeDistributeur()\">
 <p class=\"ovale\">
  <img onclick=\"fabExo(-1)\" src=\"img/bouton_gauche.png\">
  </img>
  <span id=\"numexo\">
  </span>
  <input id=\"listeExos\" onchange=\"fabDistribue()\" onfocus=\"fabEditeExos(true,this)\" title=\"listeExos\" index=\"0\" liste=\"(a+b)*a `(a+c/b)*c `(x^2-1)^2\" recepteurs=\"fab0\" style=\"background-color: white;\"/>
  <img onclick=\"fabExo(1)\" src=\"img/bouton_droit.png\">
  </img>
 </p>
</div>
@`fabInitialiseSvg(-5,-5,5,5,10);@`5@`false@`false@`