xhtml@`Sesamath@`Tri():={
local a,b,c;
a:=hasard(10);
b:=hasard(10);
c:=hasard(10);
retourne a*x^2+b*x+c;
}@`<h1 style=\"text-align: center;\">
 D&#233;mo technique : &#233;nonc&#233;s al&#233;atoires
</h1>
<hr style=\"width: 100%; height: 2px;\"/>
<br/>
On peut g&#233;n&#233;rer un &#233;nonc&#233; al&#233;atoire &#224; l\'aide d\'un bouton :
<br/>
<br/>
<fab contenteditable=\"false\">
 &#160;&#160;
 <input type=\"button\" id=\"fab0\" value=\"Générer un trinôme aléatoire\" onclick=\"xc.eval(\'Tri\',\'\');xc.imprimeMml(\'fab1\',0,true,true);xc.imprimeSvg(fab.graphique,true);\" title=\"fab0\" oncontextmenu=\"fabEditeAction(this)\" cible=\"fab1\" arguments=\"\" xcas=\"Tri\"/>
 &#160;
</fab>
<p contenteditable=\"false\" class=\"sortie\" onmousedown=\"return false\" onmouseup=\"fabEcrit(this)\" horsligne=\"1\" id=\"fab1\" title=\"fab1\" oncontextmenu=\"fabEditeAction(this)\" texte=\"8*x^2+6*x+7\" style=\"\">
  ...
</p>
<br/>
On peut aussi utiliser le distributeur (remarquer le $ qui permet d\'&#233;valuer une fonction pass&#233;e au distributeur :
<div contenteditable=\"false\" class=\"distrib\" oncontextmenu=\"fabEditeDistributeur()\">
 <p class=\"ovale\">
  <img onclick=\"fabExo(-1)\" src=\"img/bouton_gauche.png\">
  </img>
  <span id=\"numexo\">
  </span>
  <input id=\"listeExos\" onchange=\"fabDistribue()\" onfocus=\"fabEditeExos(true,this)\" title=\"listeExos\" index=\"0\" liste=\"$Tri()`$Tri()`$Tri()\" recepteurs=\"fab2\" style=\"background-color: white;\"/>
  <img onclick=\"fabExo(1)\" src=\"img/bouton_droit.png\">
  </img>
 </p>
</div>
<p contenteditable=\"false\" class=\"sortie\" onmousedown=\"return false\" onmouseup=\"fabEcrit(this)\" horsligne=\"1\" id=\"fab2\" title=\"fab2\" oncontextmenu=\"fabEditeAction(this)\" texte=\"4*x^2+8*x+8\" style=\"\">
 <math display=\"block\" index=\"undefined\" xmlns=\'http://www.w3.org/1998/Math/MathML\' >
  <mn>
   4
  </mn>
  <msup>
   <mrow>
    <mi>
     x
    </mi>
   </mrow>
   <mrow>
    <mn>
     2
    </mn>
   </mrow>
  </msup>
  <mo>
   +
  </mo>
  <mn>
   8
  </mn>
  <mi>
   x
  </mi>
  <mo>
   +
  </mo>
  <mn>
   8
  </mn>
 </math>
</p>
<br/>
<hr style=\"width: 100%; height: 2px;\"/>
<br/>
Dans les deux cas, cliquez sur l\'objet avec le bouton droit de la souris pour examiner son fonctionnement.
<br/>
<p>
</p>
@`fabInitialiseSvg(-5,-5,5,5,10);@`3@`false@`false@`