/* 
Modele de programmation objet de figures svg
un objet javascript est relie a un objet du dom
Chaque objet javascript admet une propriete nommee dom
qui pointe sur l'objet dom et chaque objet dom possede une propriete nommee js
qui pointe sur l'objet javascript.
*/

// ----- Gestion de la souris -----
/*
// neutralise le drag et drop natif sur les images
document.onmousedown=desactive;
document.onmouseup=desactive;
*/
document.ondragstart=desactive;
function desactive(evt) {
  return false;
}

// retourne les coordonn�es de la souris
function coordSouris(evt){
  if(this==window)
    return;
  base.X=evt.clientX-this.js.departX;
  if (this.js.oij && this.js.magnetisme!=0){
    base.X=Math.round(base.X*this.js.oij.xScale/this.js.magnetisme)/this.js.oij.xScale*this.js.magnetisme;
  }
  base.Y=evt.clientY-this.js.departY;
 if (this.js.oij && this.js.magnetisme!=0){
    base.Y=Math.round(base.Y*this.js.oij.yScale/this.js.magnetisme)/this.js.oij.yScale*this.js.magnetisme;
  }
  if (base.AfficheCoord){
    if (this.js.oij)
      base.AfficheCoord.value="coord : "+this.js.xr().toPrecision(2)+" "+this.js.yr().toPrecision(2);
    else
      base.AfficheCoord.value="coord : "+base.X+" "+base.Y;
  }
}

// mise a jour des centres de rotation

function majCentres(obj,angle){
  if (!obj.hasAttribute)
    return;
  var x=0,y=0,ok=false;
  if (obj.hasAttribute("xCentre")){
    x=parseFloat(obj.getAttribute("xCentre")); 
    ok=true;
  }
  if (obj.hasAttribute("yCentre")){
    y=parseFloat(obj.getAttribute("yCentre")); 
    ok=true;
  }
  if (ok){
    var X=x*Math.cos(angle)-y*Math.sin(angle);
    var Y=x*Math.sin(angle)+y*Math.cos(angle);
    obj.setAttribute("xCentre",X);
    obj.setAttribute("yCentre",Y);
  }
  for (var i in obj.childNodes)
    majCentres(obj.childNodes[i],angle);
}

// action provoquee par le bouton de la souris

function prendre(evt){
  if (this.js.fixe)
    return;
  base.Obj=this.js;
  base.ObjDom=this;
  this.js.allege(this.js.dom,true);
  base.X0=base.X;
  base.Y0=base.Y;
  if (base.Obj.translate){
    var action=this.getAttribute("bouger");
    switch(action) {
    case "T":
      base.Horloges.push(setInterval(function(){base.Obj.translate();},10));
      break;
    case "R" :
      if (this.hasAttribute("xCentre"))
	this.js.xCentre=parseFloat(this.getAttribute("xCentre"));
      else
	this.js.xCentre=0;
      if (this.hasAttribute("yCentre"))
	this.js.yCentre=parseFloat(this.getAttribute("yCentre"));
      else
	this.js.yCentre=0;
      base.Angle=Math.atan2(base.X-this.js.x-this.js.xCentre,base.Y-this.js.y-this.js.yCentre);
      base.Angle0=this.js.angle;
      base.Horloges.push(setInterval(function(){base.Obj.tourne();},50));
      break;
    }
  }
}

// relachement du bouton souris

function lacher(evt){
  if (base.Obj){
    base.Obj.allege(base.Obj.dom,false);
    base.Obj.x+=base.Obj.xAngle;
    var angle=base.Obj.angle*Math.PI/180;
    majCentres(base.Obj.dom,angle);
    base.Obj.xAngle=0;
    base.Obj.y+=base.Obj.yAngle;
    base.Obj.yAngle=0;
    base.Obj.angle0+=base.Obj.angle;
    base.Obj.angle=0;
    base.Obj=null;
  }
  for (i in base.Horloges)
    clearInterval(base.Horloges[i]);
  base.Horloges=new Array();
}


// ----- classe de base ------

function base(){
  this.dom;
  base.AfficheCoord=document.getElementById("afficheCoord");
  base.AfficheAngle=document.getElementById("afficheAngle");
  this.inclureStyle("svg.css");
}

// proprietes statiques de base

base.NS="http://www.w3.org/2000/svg";
base.AfficheCoord=null;
base.AfficheAngle=null;
// coord souris
base.X=0;
base.Y=0;
// coord initiales souris
base.X0=0;
base.Y0=0;
// regulation du glisser deposer
base.Horloges=new Array();
// objet en mouvement souris
base.Obj=null;
// objet selectionne
base.ObjSel=null;
base.StyleSel=null;
//feuille de style chargee
base.Style=false;
// pointeur sur fonction
base.f=null;

//destructeur d'objet

base.detruit=function(){
  if (!base.ObjSel){
    alert("S�lectionner d'abord un objet par un double clic");
    return;
  }
  base.ObjSel.dom.parentNode.removeChild(base.ObjSel.dom);
  delete(base.ObjSel);
  base.ObjSel=null;
}

// modification d'attributs

base.attribue=function(){
   if (!base.ObjSel){
    alert("S�lectionner d'abord un objet par un double clic");
    return;
  }
   if (!base.ObjSel.attributs)
     return;
   base.ObjSel.attribue(arguments);
   desel()
}

// creation d'un objet par la souris 

base.objet=function(s,obj){
  s.dom.addEventListener('click',base.f=function(){base.faitObjet(s,obj)},false);
}

base.faitObjet=function(s,objet){
  s.dom.removeEventListener("click",base.f,false);
  switch (objet){
  case cercle:
    var r=prompt("rayon");
    x=new cercle(r);
    break;
  case rect:
    var L=prompt("largeur");
    var H=prompt("hauteur");
    x=new rect(L,H);
    break;
  default:
    var x=new objet();
    break;
  }
  x.dessineDans(s,base.X,base.Y);
  //base.ObjSel=x;
}

// inclusion feuille de style

base.prototype.inclureStyle=function(nomFichier){
  if (!base.Style){
    var link= document.createElement("link");
    link.type = "text/css"; 
    link.href =nomFichier;
    link.rel="stylesheet";
    var head=document.getElementsByTagName("head");
    if (head[0]){
      head[0].appendChild(link);
      base.Style=true;
    } else 
      alert("La page html doit avoir une section head");
  }
}

// construction d'un objet svg

base.prototype.construire=function(type,attributs){
  var objet=document.createElementNS(base.NS,type);
  objet.js=this;
  if (attributs){
    var s;
    for (var a in attributs){
      // remplace les _ par des -
      s=a.replace(RegExp("_","g"),"-");
      if (s=="classe")
	s="class";
      if (s!="bouger" || !this.fixe)
	objet.setAttribute(s,attributs[a]);
      if (s=="bouger" && !this.fixe)
	objet.addEventListener("mousedown",prendre,false);
    }
  }
  return objet;
}

// inclusion de sous-objets svg

base.prototype.inclureSsObj=function(type,attributs,parent){
  if (!parent)
    var parent=this.dom;
  var ssObj=this.construire(type,attributs);
  ssObj.js=this;
  parent.appendChild(ssObj);
  return ssObj;
}


// ----- classe svg  qui herite de base -----

// conteneur de figures

svg.prototype=new base;
function svg(id,L,H,mH,mR,mB,mL){
  this.parent=base;
  this.parent();
  this.X=L;
  this.Y=H;
  attr={
    width:""+L,
    height:""+H
  }
  this.dom=this.construire("svg",attr);
  var par=document.getElementById(id);
  while (par.firstChild)
    par.removeChild(par.firstChild)
  par.appendChild(this.dom);
  this.dom.js=this;
  this.departX=par.offsetLeft+mL;
  this.departY=par.offsetTop+mH;
  this.marginL=mL;
  this.marginR=mR;
  this.marginH=mH;
  this.marginB=mB; 
  this.oij;

  // trace un rectangle autour du canevas
  var recta=new rect(this.X-this.marginR-this.marginL,this.Y-this.marginH-this.marginB);
  recta.fixe=true;
  recta.dom.setAttribute("transform","translate("+this.marginL+","+this.marginH+")");
  this.dom.appendChild(recta.dom);
  // definit le canevas (partie ou on dessine)
  attr={
    x:this.marginL,
    y:this.marginH,
    width:this.X-this.marginL-this.marginR,
    height:this.Y-this.marginH-this.marginB
  }
  this.canevas=this.construire("svg",attr);
  this.dom.appendChild(this.canevas);
  this.dom.addEventListener("mouseup",lacher,false);
  this.dom.addEventListener("mousemove",coordSouris,false);

  // quadrillage
  this.grille=this.inclureSsObj("g",null,this.canevas);
  this.legende=this.inclureSsObj("g",null);
  this.axes=this.inclureSsObj("g",null,this.canevas);
  this.magnetisme=0;
}

// construction de la grille
svg.prototype.visible=function(){
  var visible=arguments[0];
  var d="block";
  if (!visible)
    d="none";
  for (var i=1;i<arguments.length;i++)
    arguments[i].style.display=d;
}

svg.prototype.traceGrille=function(couleur){
  if (!couleur)
    var couleur="navy";
  function calcIgras(debut,d){
    iGras=Math.floor(debut/d)%5;
    if (iGras<0)
      iGras=5+iGras;
    if (iGras>0)
      iGras=5-iGras;
    return iGras;
  }
  var dX=(this.oij.xMax-this.oij.xMin)/12.5;
  var dY=(this.oij.yMax-this.oij.yMin)/12.5;
  dX=this.pas(dX);
  dY=this.pas(dY);
  var debutX=dX*Math.ceil(this.oij.xMin/dX);
  var debutY=this.oij.yMin;//dY*Math.ceil(this.oij.yMin/dY)-this.oij.yMin;
  var i, x,y,legendeX,legendeY,ligne,points;
  var attr;
  // ligne verticales
  var iMax=(this.oij.xMax-this.oij.xMin)/dX;
  if (debutX==0)
    iMax++;
  iGras=calcIgras(debutX,dX);
  for (i=0; i<=iMax  ; i++){
    attr=new Object();
    legendeX=i*dX+debutX;
    x=this.xr2x(legendeX);
    pointsV=x+" 0,"+x+" "+this.Y;
    attr.points=pointsV;
    attr.stroke=couleur;
    if (i%5!=iGras)
      attr.stroke_width="0.15";
    else {
      if (legendeX==0)
	attr.stroke_width="1";
      else
	attr.stroke_width="0.4"; 
      this.insereNombre(this.marginL+x,this.Y-this.marginB+20,"middle",legendeX);  
    }
    if (i%5==iGras)
      this.inclureSsObj("polyline",attr,this.axes);
    else
      this.inclureSsObj("polyline",attr,this.grille);
  }
  // lignes horizontales
  iMax=(this.oij.yMax-this.oij.yMin)/dY;
  iGras=calcIgras(debutY,dY);
  if (debutY==0)
    iMax++;
  for (i=0; i<=iMax ; i++){
    attr=new Object();
    attr.stroke=couleur;
    legendeY=i*dY+debutY;
    y=this.yr2y(legendeY);
    pointsH="0 "+y+", "+this.X+" "+y;
    attr.points=pointsH;
    if (i%5!=iGras)
      attr.stroke_width="0.15";
    else {
      if (legendeY==0)
	attr.stroke_width="1";
      else
	attr.stroke_width="0.4";
      this.insereNombre(this.marginL-6,y+3+this.marginH,"end",legendeY);
    }
    if (i%5==iGras)
      this.inclureSsObj("polyline",attr,this.axes);
    else
      this.inclureSsObj("polyline",attr,this.grille);

  }
  return;
}

svg.prototype.insereNombre=function(x,y,align,nombre){
  var n;
  var attr={
    x:x,
    y:y,
    text_anchor:align,
    font_size:"12px"
  }
  var text=this.inclureSsObj("text",attr,this.legende);
  if (Math.abs(nombre)>10000 ||
      (Math.abs.nombre<0.0001 && nombre!=0))
      n=nombre.toExponential(1);
  else 
    n=nombre;
  t=document.createTextNode(n);
  text.appendChild(t);
}

// calcul du pas de la grille
svg.prototype.pas=function(dL){
  function log(x){
    return Math.log(x)/Math.LN10;
  }
  var a,b,dixN;
  a=log(dL);
  b=a-Math.floor(a);
  dixN=Math.pow(10,a-b);
  if (b<log(2))
    return dixN;
  else if (b<log(4))
    return 2*dixN;
  else if (b<log(8))
    return 5*dixN;
  else
    return 10*dixN;
}



svg.prototype.repere=function(xMin,yMin,xMax,yMax){
  this.oij=new Object();
  this.oij.xMin=parseFloat(xMin);
  this.oij.xMax=parseFloat(xMax);
  this.oij.yMin=parseFloat(yMin);
  this.oij.yMax=parseFloat(yMax);
  this.oij.xScale=(xMax-xMin)/(this.X-this.marginL-this.marginR);
  this.oij.yScale=(yMax-yMin)/(this.Y-this.marginH-this.marginB);
  this.traceGrille();
}

svg.prototype.dansCadre=function(x,y){
  return x>=this.oij.xMin && x<=this.oij.xMax &&
  y>=this.oij.yMin && y<=this.oij.yMax;
}

svg.prototype.x2xr=function(x){
  if (!this.oij)
    return x;
  var xr=this.oij.xMin+x*this.oij.xScale;
  return xr;
}

svg.prototype.xr=function(){
  return this.x2xr(base.X);
}

svg.prototype.y2yr=function(y){
  if (!this.oij)
    return y;
  return this.oij.yMax-y*this.oij.yScale;
}

svg.prototype.yr=function(){
  return this.y2yr(base.Y);
}

svg.prototype.xr2x=function(xr){
  if (!this.oij)
    return xr;
  return (xr-this.oij.xMin)/this.oij.xScale;
}

svg.prototype.yr2y=function(yr){
 if (!this.oij)
    return yr;
  return (this.oij.yMax-yr)/this.oij.yScale;
}

// ----- classe fig qui herite de la classe  base ----- 

fig.prototype=new base;
function fig(s,type,attr){
  this.parent=base;
  this.parent();
  // cadre svg recevant la figure
  this.svg;
  this.nom=s;
  // non deplacable a la souris si true
  this.fixe=false;
  //objet princpal
  if (!type)
    type="g";
  this.dom=this.construire(type,attr);
  if (type=="g")
    this.dom.setAttribute("class",s);
  this.dom.addEventListener('dblclick',sel,false);
  // coordonnees par rapport au coin sup gauche
  this.x=0;
  this.y=0;
  // angle initial
  this.angle=0;
  this.angle0=0;
  // centre �ventuel de rotation
  this.xCentre=0;
  this.yCentre=0;
  // angle eventuel de rotation
  this.xAngle=0;
  this.yAngle=0;
  // liste d'attributs sensibles a l'echelle
  this.adaptableX;
  this.adaptableY;
  // liste d'attributs parametrables
  this.attributs;
/*
  var attr={
    begin:"0",
    dur:"0.1s",
    path:"M 0 0",
    repeatCount:"indefinite"
  }
  this.anim=this.inclureSsObj("animateMotion",attr);
*/
}

// methodes de l'objet fig

// s�lection de la figure
sel=function(){ 
  if (!base.ObjSel || base.ObjSel!=this.js){
    // restaure le style de la selection precedente
    if (base.ObjSel && base.ClasseSel)
      base.ObjSel.dom.setAttribute("class",base.ClassSel);
    base.ClasseSel=this.js.dom.getAttribute("class");
    this.js.dom.setAttribute("class","selection");
    base.ObjSel=this.js;
  }
}

// deselectionne
desel=function(){
  // restaure le style de la selection
  if (base.ObjSel && base.ClasseSel)
    base.ObjSel.dom.setAttribute("class",base.ClasseSel);
  base.ObjSel=null;
}


// avec coordonnees originales (pour construction a la souris)
fig.prototype.dessineDans=function(cible,x,y){
  if (this.xf){ // courbes
    var d=cible.makeCurvePath(this.xf, this.yf, this.tMin, this.tMax, 1000,this.finesse);
    this.dom.setAttribute("d",d);
  }
  this.svg=cible;
  // mise a l echelle eventuelle d'attributs
  if (this.svg.oij){
    this.echelle(this.adaptableX,this.svg.oij.xScale);
    this.echelle(this.adaptableY,this.svg.oij.yScale);
  }
  cible.canevas.appendChild(this.dom);
  this.x=x;
  this.y=y;
  this.dom.setAttribute("transform","translate("+x+","+y+")");
}

// avec ooordoonnees du repere (pour construction par programme)
fig.prototype.dessine=function(cible,x,y){
  if (this.oij)
    this.dessineDans(cible,this.x2xr(x),this.y2yr(y));
  else
    this.dessineDans(cible,x,y);
}

fig.prototype.echelle=function(tab,scale){
  if (!tab)
    return;
  var at;
  for (var i in tab){
    at=this.dom.getAttribute(tab[i]);
    this.dom.setAttribute(tab[i],at/scale);//this.svg.oij.xScale);
  }
}

// precise les attributs de style
  fig.prototype.attribue=function(valeurs){
  for (var i in this.attributs)
    if (valeurs[i]){
      this.dom.setAttribute(this.attributs[i],valeurs[i]);
    }
}


// construction de l'attribut transform

fig.prototype.transform=function(){
    var x=this.x,y=this.y;
    var angle=(this.angle)*Math.PI/180;
    if (angle!=0){
      this.xAngle=this.xCentre-(this.xCentre*Math.cos(angle)-this.yCentre*Math.sin(angle));
      this.yAngle=this.yCentre-(this.xCentre*Math.sin(angle)+this.yCentre*Math.cos(angle));
      x=this.x+this.xAngle;
      y=this.y+this.yAngle;
    }
    angle=angle/Math.PI*180+this.angle0;
  return "translate("+x+" "+y+") rotate("+angle+")";
}


// rotation pilotee par la souris

fig.prototype.tourne=function(){
  var x=base.X-this.x-this.xCentre, y=base.Y-this.y-this.yCentre;
  this.angle=base.Angle0-57.3*(Math.atan2(x,y)-base.Angle);
  var s=this.transform();
  if (base.AfficheAngle){
    var alpha=(Math.floor(this.angle)+360)%180;
    var beta=180-alpha;
    base.AfficheAngle.value="Angle : "+alpha+" "+beta;
  }
  this.dom.setAttribute("transform",s);
}

// translation pilotee par la souris

fig.prototype.translate=function(){
  var dx=base.X-base.X0;
  var dy=base.Y-base.Y0;
  base.X0=base.X;
  base.Y0=base.Y;
  this.x+=dx;
  this.y+=dy;
  var s=this.transform();
  this.dom.setAttribute("transform",s);
  if (this.change)
    this.change();
}


// allegement eventuel de la figure pour un deplacement fluide

fig.prototype.allege=function(node,efface){
   for (var i in node.childNodes){
    if (node.childNodes[i].eff){
      if (efface){
	node.childNodes[i].eff="n";
	node.childNodes[i].style.display="none";
      } else {
	node.childNodes[i].eff="b";
	node.childNodes[i].style.display="block";
      }
    }
    this.allege(node.childNodes[i],efface);
  }
}


///deplacement d'une figure le long d'une courbe parametree
/// xf : fonction javascript permettant de calculer x en fonction de t
/// yf : fonction javascript permettant de calculer y en fonction de t
/// v : vitesse (en unites du repere par seconde
/// t0 : valeur initiale du parametre
fig.prototype.animateAlongParametricCurve=function(c,v,t0,t1,repeter){
  var repetition=1;
  t0=parseInt(t0); t1=parseInt(t1);
  var abscisse=this.x
  var ordonnee=this.y;
  var figure=this;
  var xAttribute, yAttribute;
  var t=parseInt(t0);
  // persistance retinienne en millisecondes
  var dT=50;
  var dt=0.01;
  var x0=c.xf(t),y0=c.yf(t),dx,dy, L, dtCorrige;
  var obj=this;
  var anim=setInterval(function(){newXY(obj)},dT);

  function step(obj){
    var continuer=true;
    while(continuer && t<t1){
      try {
	var x=c.xf(t+dt);
	var y=c.yf(t+dt);
	continuer = false;
      }
      catch(err){
	t+=dt;
      }
    }
    if (!obj.svg.dansCadre(x,y)){
      t=t+dt;
    } else {
      dx=x-x0;
      dy=y-y0;
      L=Math.sqrt(dx*dx+dy*dy);
      if (Math.abs(L)>1e-4)
	dtCorrige=dt*v*dT/L/1000;
      else
	dtCorrige=dt;
      t+=dtCorrige;
    }
    if (t>t1){
      t=t0;
      if (repeter>-1){
	repetition+=1;
	if (repetition>repeter)
	  clearInterval(anim);
      }
    }
  }
 
  function newXY(obj){
    step(obj);
    var x1=c.xf(t);
    var y1=c.yf(t);
    x0=x1;
    y0=y1;
    obj.x=obj.svg.xr2x(x0);
    obj.y=obj.svg.yr2y(y0);
    obj.dom.setAttribute("transform","translate("+obj.x+","+obj.y+")");

  }
}

// ----- classe zone de saisie ------

saisie.prototype=new fig;
function saisie(texte,valeur){
  this.parent=fig;
  this.parent("saisie");
  var attr={
    width:"150",
    height:"20",
    font_size:"16",
    font_family:"'Arial'",
    style:"text-anchor:start",
    fill:"navy"
  }; 
  this.texte=texte;
  this.valeur=valeur
  this.text=this.inclureSsObj("text",attr);
  elttxt = document.createTextNode(valeur);
  this.text.appendChild(elttxt);
  this.text.addEventListener("click",this.saisir,false);
}

saisie.prototype.saisir=function(){
  var valeur=prompt(this.js.texte,this.js.valeur);
  if (!valeur)
    return;
  if (valeur=="")
    valeur="...";
  this.js.valeur=valeur;
  var  elttxt = document.createTextNode(this.js.valeur);
  this.removeChild(this.firstChild);
  this.appendChild(elttxt);
}

// ----- classe surface qui herite de fig -----

surface.prototype=new fig;

function surface(s,type,attr){
  this.parent=fig;
  if (attr){
    attr.classe="sensiblesurvol";
    attr.stroke="none";
    attr.fill="black";
    attr.bouger="T";
  }
  this.parent(s,type,attr);
  this.attributs=["fill"];
}

// ----- classe point qui herite de surface -----

point.prototype=new surface;
function point(){
  this.parent=surface;
  var attr={
    cx:0,
    cy:0,
    r:3
  }
  this.parent("point","circle",attr);
}


// ----- classe ligne qui herite de fig -----

ligne.prototype=new fig;

function ligne(s,type,attr){
  this.parent=fig;
  if (attr){
    attr.classe="tressensiblesurvol";
    attr.stroke="black";
    attr.fill="none";
    // les evenements souris n'auront lieu que sur les parties visibles 
    attr.pointer_event="visibleFill";
    attr.stroke_width="1";
    attr.bouger="T";
  }
  this.parent(s,type,attr);
  this.attributs=["stroke","stroke-dasharray"];
}


// ----- classe cercle qui herite de ligne -----

cercle.prototype=new ligne;
function cercle(r){
  this.parent=ligne;
  var attr={
    cx:0,
    cy:0,
    r:r,
  }
  this.parent("cercle","circle",attr);
  this.adaptableX=["r"];
}


// ----- classe rect qui h�rite de ligne -----

rect.prototype=new ligne;

function rect(L,H){
  this.parent=ligne;
  var attr={
    width:L,
    height:H,
  }
  this.parent("rect","rect",attr);
  this.adaptableX=["width"];
  this.adaptableY=["height"];
}



// ---- class path qui herite de ligne

path.prototype=new ligne;
function path(d){
  this.parent=ligne;
  var attr={
    d:d
  }
  if (d)
    this.parent("path","path",attr);
}


// ----- essai d'une classe "regle" -----
//     avec deux centres de rotation

regle.prototype=new fig;

function regle(){
  this.parent=fig;
  this.parent("regle");
  var attr={
    classe:"sensiblesurvol",
    width:400,
    height:30,
    fill:"#B2BBCE",
    opacity:0.6,
    bouger:"T"
  }
  var ssObj=this.inclureSsObj("rect",attr);
  attr={
    width:400,
    height:20,
    y:30,
    fill:"#B2BBCE",
    opacity:0.6
  }
  ssObj=this.inclureSsObj("rect",attr);
  attr={
     classe:"sensiblesurvol",
     cx:20,
     cy:15,
     r:13,
     fill:"gray",
     bouger:"R",
     xCentre:"400",
     yCentre:"50"
  }
  ssObj=this.inclureSsObj("circle",attr);
  attr.cx="380";
  attr.xCentre="0";
  ssObj=this.inclureSsObj("circle",attr);
}

// ----- classe rapporteur qui herite de fig -----
//            d'apres Laurent ZAMO

rapporteur.prototype=new fig;

function rapporteur(){
  this.parent=fig;
  this.parent("rapporteur");
  var ssObj;
  // la barre
  var attr={
    classe:"sensiblesurvol",
    opacity:"0.6",
    d:"M-150,0v20h300v-20z",
    fill:"#B2BBCE",
    stroke:"#000000",
    stroke_width:"0.5",
    bouger:"T"
  }
  ssObj=this.inclureSsObj("path",attr);
  // le centre
  attr={
    fill:"none",
    stroke:"#000000",
    stroke_width:"0.5",
    d:"M-7,0h14M0,-7v14"
  }
  ssObj=this.inclureSsObj("path",attr);
  // le demi-cercle
  attr={
    classe:"sensiblesurvol",
    opacity:"0.6",
    d:"M-150,0a150,150,0,0,1,300,0h-60a90,90,0,1,0,-180,0z",
    fill:"#B2BBCE",
    stroke:"#000000",
    stroke_width:"0.5",
    bouger:"R"
  }
  var demiCercle=this.inclureSsObj("path",attr);

  //graduations exterieures
  var txt="";
  var c;
  var s;
  var elttxt;
  for(var i=0 ; i<=180 ;i++){
    c=Math.cos(i*Math.PI/180);
    s=-Math.sin(i*Math.PI/180);
    if(i%5){
      txt+="M"+(150*c)+","+(150*s)+"L"+(142*c)+","+(142*s);                        
      txt+="M"+(90*c)+","+(90*s)+"L"+(95*c)+","+(95*s); 
    }else if(i%10){
      txt+="M"+(150*c)+","+(150*s)+"L"+(137*c)+","+(137*s);                        
      txt+="M"+(90*c)+","+(90*s)+"L"+(99*c)+","+(99*s);
    }else{
      txt+="M"+(150*c)+","+(150*s)+"L"+(135*c)+","+(135*s);
      txt+="M"+(90*c)+","+(90*s)+"L"+(102*c)+","+(102*s); 
      attr={
	font_size:"8",
	font_family:"'Arial'",
	style:"text-anchor:middle",
	transform:"translate("+(105*c)+" "+(105*s)+") rotate("+(90-i)+")"
      }
      ssObj=this.inclureSsObj("text",attr);
      ssObj.eff="b";
      elttxt = document.createTextNode(i);
      ssObj.appendChild(elttxt);
      //exterieur
      attr={
	font_size:"10",
	font_family:"'Arial'",
	style:"text-anchor:middle",
	transform:"translate("+(125*c)+" "+(125*s)+") rotate("+(90-i)+")"
      }
      ssObj=this.inclureSsObj("text",attr);
      ssObj.eff="b";
      elttxt = document.createTextNode(180-i);
      ssObj.appendChild(elttxt);

    }
  }
  attr={
    fill:"none",
    stroke:"#000000",
    stroke_width:"0.5",                
    d:txt
  }
  ssObj=this.inclureSsObj("path",attr);
}

/* --------- COURBES PARAMETREES ------------

           ISSUES DU DEFUNT PROJET WEBGEOM
  ------------------------------------------*/



/// transforme une chaine de caracteres en fonction javascript
function wgMakeFunction(functionString){
  // functionString doit �tre une chaine valide en javascript ex Math.pow(x,2)
  function f(x){return eval("1*"+functionString);};
  return f;
}

function wgMakeFunctionEnT(functionString){
  // functionString doit �tre une chaine valide en javascript ex Math.pow(t,2)
  function f(t){return eval("1*"+functionString);};
  return f;
}


// ---  courbes parametrees herite de path ----------


parametricCurve.prototype=new path();
// xFunctionString : chaine qui donne la fonction t-->x(t)
// yFunctionString : chaine qui donne la fonction t-->y(t)
// tMin tMax valeurs minimale et maximale du parametre
function parametricCurve(xFunctionString, yFunctionString, tMin, tMax ){
  this.parent=path;
  var tabX=wgParseExpression(xFunctionString);
  var tabY=wgParseExpression(yFunctionString);
  this.xf=wgMakeFunctionEnT(tabX[0]);
  this.yf=wgMakeFunctionEnT(tabY[0]);
  this.tMin=tMin;
  this.tMax=tMax;
  this.parent("M 0 0");
  this.fixe=true;
  this.finesse=0.5;
}

// representation graphique de fonctions
cf.prototype=new parametricCurve;
function cf(f,xMin,xMax,idF){
  this.parent=parametricCurve;
  this.f=f;
  this.f0=f;
  this.parent("t",0,xMin,xMax);
  // remplace la fonction en  x par une fonction en t
  var tab=wgParseExpression(f+"");
  this.yf=wgMakeFunction(tab[0]);
  if (idF)
    this.idF=document.getElementById(idF);
}

 // on rafraichit eventuellement la fonction numerique representee
cf.prototype.change=function(){
  var x=-this.svg.oij.xScale*this.x;
  x=x.toPrecision(2)
  var y=-this.svg.oij.yScale*this.y;
  y=y.toPrecision(2)
  this.f=this.f0;
  if (y>0)
    this.f+="+";
  if (y!=0)
    this.f+=y;
  if (x>0)
      this.f=this.f.replace(RegExp("x","g"),"(x+"+x+")");
  if (x<0)
    this.f=this.f.replace(RegExp("x","g"),"(x"+x+")");
  this.f=wgParseExpression(this.f)[1]; 
  if (this.idF && this.idF.value)
    this.idF.value=this.f;
}



function wgNoDefined(x,min,max){
  return (isNaN(x) || x<min || x>max || 1/x==0);  
}

/// fonction privee
// recherche par dichotomie de points dans le cadre
// quand f(x) est dans le cadre mais f(x+dx) ne l'est pas
function wgFindLastPoint(xf,yf,t,dt,xMin,xMax,yMin,yMax){
  var forwards=true;
  var tLast=t;
  var x=xf(t);
  var y=yf(t);
  for(var i=0 ; i<100 ; i++){
    dt=dt/2;
    if (forwards)
      t+=dt;
    else
      t-=dt;
    x=xf(t)
      y=yf(t);
    if(!wgNoDefined(x,xMin,xMax)
       && !wgNoDefined(y,yMin,yMax)){
      tLast=t;
      forwards=true;
    } else
      forwards=false;
  }
  return tLast; 
}

function wgFindExtremum(f,t,dt,max){
  //if (t>-4 && t<-3)
  //alert("debut "+t+" "+dt+" "+f(t));
  dt/=10;
  var z0=f(t);
  for (var i=0 ; i<10 ; i++){
    t+=dt;
    z1=f(t);
    if (max && z1<=z0){
      // alert("fin "+t+" "+dt+" "+f(t));
      return t;
    }
    if (!max && z1>=z0){
      //  if (t>-4 && t<-3)
      //alert("fin "+t+" "+dt+" "+f(t));
      return t;
    }
    z0=z1;
  }
  return t;
}

	

/// Fonction privee : cherche le prochain point significatif
function wgNextPoint(xf, yf, t, dt, tMax,xMin,xMax,yMin,yMax, n, finesse){
  var x0=xf(t), y0=yf(t), x=x0, y=y0, x1, y1;
  do {
    t+=dt;
    x1=xf(t);
    y1=yf(t);
  } while (x1==x0 && y1==y0 && t<=tMax); 
  var xU=x1-x0, yU=y1-y0; // vecteur directeur
  var xV, yV, xVold,yVold, sinus, sin, testSin=true,signe;
  var EcartMax=Math.abs(xMax+yMax-xMin-yMin)/1000,ecart, Ecart=0;
  var inflexion=false;
  var normeU=Math.sqrt(xU*xU+yU*yU),normeV=0,normeVold;
  var d;
  var i=-1;
  do {
    i++;
    x=x1;
    y=y1;
    t+=dt;
    x1=xf(t);
    y1=yf(t);
    xVold=xV;
    yVold=yV;
    xV=x1-x;
    yV=y1-y;
    normeVold=normeV;
    normeV=Math.sqrt(xV*xV+yV*yV);
    if (normeV>0){ 
      sinus=(xU*yV-yU*xV)/(normeU*normeV);
      if (normeVold!=0){
	ecart=(xVold*yV-yVold*xV)/normeVold;
	Ecart+=ecart;
	sin=ecart/normeV;
      }
      else
	sin=0;
    }
    else
      sinus=0;
    if (sin && signe==null)
      signe=sin>0;
    inflexion=(signe && sin<-1e-7) || (!signe && sin>1e-7);
    sinOld=sin;
  } while( !inflexion
	   && Math.abs(Ecart)<EcartMax
	   && Math.abs(sinus)<finesse
	   && t<=tMax && !wgNoDefined(x1,xMin,xMax) 
	   && !wgNoDefined(y1,yMin,yMax));
  if (wgNoDefined(x1,xMin,xMax) || wgNoDefined(y1,yMin,yMax))
    return [wgFindLastPoint(xf,yf,t-dt,dt,xMin,xMax,yMin,yMax),false];
  if (t>tMax)
    return [tMax,false];
  return [t,true]; //continuit�
}

svg.prototype.findSpecialPoint=function(xf, yf, t, dt, tMax, xMin, xMax, yMin, yMax){
  var x0=xf(t), y0=yf(t);
  var t0=t;
  t+=dt;
  var x1=xf(t), y1=yf(t);
  var xU=x1-x0, yU=y1-y0;
  var changeX=false, changeY=false;
  var passBox=false;
  var xInBox= this.canvasXmin<=x0 && x0<=this.canvasXmax;
  var yInBox= this.canvasYmin<=y0 && y0<=this.canvasYmax;
  var xMin=2*this.canvasXmin-this.canvasXmax;
  i=0;
  do {
    t+=dt;
    x0=x1;
    y0=y1;
    if (xInBox != (this.canvasXmin<=x0 && x0<=this.canvasXmax))
      passBox=true;
    if (yInBox!=(this.canvasYmin<=y0 && y0<=this.canvasYmax))
      passBox=true;
    x1=xf(t);
    y1=yf(t);
    changeX=((x1-x0)*xU<0);
    changeY=((y1-y0)*yU<0);
  } while (!passBox
	   && !changeX 
	   && !changeY 
	   && !wgNoDefined(x1,xMin,xMax)
	   && !wgNoDefined(y1,yMin,yMax)
	   && t<=tMax);
  //alert(changeX+" "+changeY+" "+t+" "+dt);
  if (t>tMax)
    return [tMax,false];
  //if (changeX || changeY)
  //return [t-3*dt,true];
  //if (changeY && !changeX)
  //return [wgFindExtremum(yf, t,-2*dt, yU>0)-dt,true];
  if (changeX)
    return [wgFindExtremum(xf, t,-2*dt, xU>0)-dt, true];
 if (changeY)
    return [wgFindExtremum(xf, t,-2*dt, xU>0)-dt, true];
  return [t, false];
}


/// donne l'abscisse du point d'intersection
/// de la droite passant par (x1, y1) de vect direct (a, b)
/// avec la droite passant par (x2, y2) de vect direct (c, d) 
function wgIntersecTangent(a,b,c,d,x1,y1,x2,y2){
  var determinant=a*d-b*c;
  // on elimine les valeurs proches de 0
  var nearZero=Math.min(Math.abs(a)+Math.abs(b),Math.abs(c)+Math.abs(d))/1000;
  if (a==0 && b==0)
    return null;
  if (c==0 && d==0)
    return null;
  if (Math.abs(determinant)<nearZero)
    return null;
  return (a*c*(y1-y2)-b*c*x1+a*d*x2)/determinant;
}


/// fonction priv�e
/// retourne un "path" pour dessiner la courbe parametree
svg.prototype.makeCurvePath=function(xf, yf, tMin, tMax,n,finesse){
  // a supprimer
  var pc=new point()
  var dt=(tMax-tMin)/n;
  var continuity=false;
  var extremum=false, extremumFlag=false;
  var xMin,xMax,yMin,yMax;
    // on dessine aussi en dehors du cadre
  xMin=2*this.oij.xMin-this.oij.xMax;
  xMax=2*this.oij.xMax-this.oij.xMin;
  yMin=2*this.oij.yMin-this.oij.yMax;
  yMax=2*this.oij.yMax-this.oij.yMin;
  var t=tMin-dt, tNext, tOld, x, y, xOld, yOld, tC, xC, yC, tab;
  var a,b,c,d;
  var i;
  var path="", path1="";
  var nPoints=0;
  while (t<tMax){  
    i=0;
    do {
      i++;
      t+=dt;
      x=xf(t);
      y=yf(t);
    } while (t<tMax 
	     && (wgNoDefined(x,xMin,xMax) || wgNoDefined(y,yMin,yMax)));
    continuity = (i==1 && continuity);
    if (!continuity)
      t=wgFindLastPoint(xf,yf,t,-dt,xMin,xMax,yMin,yMax);
    x=xf(t);
    y=yf(t);
    if (!continuity){
      path+="M"+this.xr2x(x)+" "+this.yr2y(y);
      continuity=true;
      nPoints++;
    } else { 
      tab= this.findSpecialPoint(xf, yf, t, dt, tMax, xMin, xMax, yMin, yMax);
      tNext=tab[0];
      extremum=tab[1];
      extremumFlag=false;
      //alert("tNext "+tNext+" extremum "+extremum);
      //flagExtremum=!extremum;
      c=xf(t+dt/2)-xf(t);
      d=yf(t+dt/2)-yf(t);
      while (t<tNext && continuity){
	tOld=t;
	xOld=x;
	yOld=y;
	if (tNext-t==2*dt)
	  t=tNext;
	else {
	  tab=wgNextPoint(xf, yf, t, dt,tNext,xMin,xMax,yMin,yMax,n,finesse);
	  t=tab[0];
	  continuity=tab[1];
	}	
	// point courant
	x=xf(t);
	y=yf(t);
	// vecteurs directeurs
	a=c;
	b=d;
	if (t<tNext){
	  c=xf(t+dt/2)-xf(t-dt/2);
	  d=yf(t+dt/2)-yf(t-dt/2);
	} else {
	  c=xf(t)-xf(t-dt/2);
	  d=yf(t)-yf(t-dt/2);
	}
	// point de controle
	xC= wgIntersecTangent(a,b,c,d,xOld,yOld,x,y);
	yC= wgIntersecTangent(b,a,d,c,yOld,xOld,y,x);

	if (xC){
	  if (extremumFlag){
	    path+="L"+this.xr2x(xC)+" "+this.yr2y(yC)+"L"+
	    this.xr2x(x)+" "+this.yr2y(y);
	  }
	  else {
	    // a corriger !
	    path+="Q"+this.xr2x(xC)+" "+this.yr2y(yC)+" "+
	    this.xr2x(x)+" "+this.yr2y(y);	   
	  }
	} else {
	  path+="L"+this.xr2x(x)+" "+this.yr2y(y);
	} 


	if (extremum && t>=tNext-dt){
	  continuity=true;
	  extremum=false;
	  extremumFlag=true;
	  t=tNext
	  tNext+=2*dt;	  
	}
/* pour faire apparaitre les points de controle
   si le svg se nomme s 
	pc=new point();
	pc.attribue(["red"]);
	pc.dessineDans(s,this.xr2x(xC),this.yr2y(yC));
	pc=new point();
	pc.dessineDans(s,this.xr2x(x),this.yr2y(y));
*/
      }
      nPoints++;
    }
  }
  //alert(nPoints+" points");
  //   alert(path1);
  return path;
}


/* --------------------------------------------------------------------
 mini parser convertit la syntaxe calculatrice en syntaxe javascript
 et effectue les additions : x^2+1+2 devient Math.pow(x,2)+3
 C'est bien long ...
 --------------------------------------------------------------------
*/

// fonctions de service

  function wgappartient(a,b){
    if (!(b.length>0))
      return false;
    var i;
    for (i=0 ; i<b.length; i++){
      if (a==b[i])
	return true;
    }
    return false;
  }


function wgop(x){
  var liste=["*","/","+","-"]
    return wgappartient(x,liste);
}

function wgparenthese(chaine,liste){
  var i;
  var op=false;
  for (i=0; i<chaine.length ; i++){
    if (wgappartient(chaine.charAt(i),liste))
      op= true;
  }
  if (op)
    chaine="("+chaine+")";
  return chaine;
}


function wgchiffre(x){
  var liste=[0,1,2,3,4,5,6,7,8,9,"."];
  return wgappartient(x,liste);
}

function wgcode(x){
  var liste=["*","/","+","-","^","_","(",")",",",";","=","<",">"]
    return wgappartient(x,liste);
}

function wgestNomDeFonction(f)
{
  var liste=[
	     "sqrt",
	     "abs",
	     "log",
	     "ln",
	     "exp",
	     "sin",
	     "cos",
	     "tan",
	     ]
    return wgappartient(f,liste);
}

// mini parser proprement dit

function wgparseNum(chaine, debut){
  var s=chaine.slice(debut);
  var x=parseFloat(s);
  //alert("parsefloat "+s+" "+debut+" "+x);
  var i=debut;
  if (chaine.charAt(i)=="+" || chaine.charAt(i)=="-")
    i++;
  while  (i<chaine.length && wgchiffre(chaine.charAt(i))) 
    i++;
  return [x,i,x];
}   
	 


function wgparseNom(chaine, debut){
  var n=chaine.length;
  var i=debut;
  var nom="", nom2="";
  while (!wgcode(chaine.charAt(i)) && i<n){
    nom=nom+chaine.charAt(i);
    i++;
  }
  nom2=nom;
  if (nom=="PI" || nom=="Pi" || nom=="pi")
    nom="Math.PI";
  return[nom,i,nom2];
}

function wgparseAtome(chaine,debut){
  //alert("parseAtome "+chaine+" "+debut+chaine.charAt);
  var i=debut;
  var tab;
  if (chaine.charAt(i)=="("){
    i++;
    tab=wgparseSomme(chaine,i);
    i=tab[1];
    if (i+1<chaine.length)
      i++;
    return [tab[0],i,tab[2]];
  } 
  if (wgchiffre(chaine.charAt(i))
      || chaine.charAt(i)=="-" && wgchiffre(chaine.charAt(i+1))
      || chaine.charAt(i)=="+"){
    return wgparseNum(chaine,i);
  }
  tab=wgparseNom(chaine,i);
  i=tab[1];
  var s,s1;
  if (wgestNomDeFonction(tab[0])){
    if (tab[0]=="ln")
      s="Math.log";
    else
      s="Math."+tab[0];
    s1=tab[2];
    if (chaine.charAt(i)=="(") {
      i++;
      tab=wgparseSomme(chaine,i);
      s+="("+tab[0]+")";
      s1+="("+tab[2]+")";
      i=tab[1]+1;
     //alert("..."+chaine+" "+i+" "+tab);
    }
  } else {
    s=tab[0];
    s1=tab[2];
  }
  return [s,i,s1];
}


function wgparsePuissance(chaine,debut){
  //alert("parsePuissance "+chaine+" "+debut);
  var s="",s1="";
  var tab=wgparseAtome(chaine,debut);
  var i=tab[1];
 //alert(chaine+" i "+i+"chaine.charAt(i): "+chaine.charAt(i)+" tab  "+tab);
  if (i>=chaine.length || chaine.charAt(i)!="^")
    return tab;
  if (tab[0]!="e")
    s="Math.pow("+tab[0]+",";
  else
    s="Math.exp(";
 //alert("1 s="+s);
  s1=wgparenthese(tab[2],["+","-","*","/","^"]);
  i++;
  tab=wgparseAtome(chaine,i,false);
  s+=tab[0]+")";
 //alert("2 s="+s);
  s1+="^"+wgparenthese(tab[2],["+","-","*","/","^"]);
  return [s,tab[1],s1];
}


function wgproduitImplicite(chaine,i){
  var liste=["*","/","+","-","^",")"];
  var listeOp=["*","/","+","-","^"];
  var car=chaine.charAt(i);
  var carPrecedent=chaine.charAt(i-1);
  if (i==0)
    return false;
  if (wgchiffre(carPrecedent) && !wgchiffre(car) && !wgappartient(car,liste))
    return true;
  if (carPrecedent==")" && !wgchiffre(car) && !wgappartient(car,liste))
    return true;
  if (!wgappartient(carPrecedent,listeOp) && car=="(")
    return true;
  return false;
}

function wgparseProduit(chaine,debut){
  //alert("parseProduit "+chaine+" "+debut);
  var s="",s1="";
  var tab=wgparsePuissance(chaine,debut);
  s=tab[0];
  s1=tab[2];
  var i=tab[1];
  var op=chaine.charAt(i);
  if (op=="*"|| op=="/"
      || wgproduitImplicite(chaine,i)){
    s=wgparenthese(s,["+","-"]);
    s1=wgparenthese(s1,["+","-"]);
  }
  while(chaine.charAt(i)=="*" 
	|| chaine.charAt(i)=="/"
	|| wgproduitImplicite(chaine,i)){
    op=chaine.charAt(i);
    if (op!="*" && op!="/")
      op="*";
    else
      i++;
    s+=op;
    s1+=op;
    tab=wgparsePuissance(chaine,i);
    s+=wgparenthese(tab[0],["+","-"]);
    s1+=wgparenthese(tab[2],["+","-"]);
    i=tab[1];
  }
 //alert("fin parseProduit "+s+" i="+i);
  return [s,i,s1];
}


function wgparseSomme(chaine, debut){
  //alert("parsesomme "+chaine+" "+debut);
  var s="",s1="";
  var num=0;
  var first=true;
  var tab=wgparseProduit(chaine,debut);
  var n=tab[0]*1;
  if (isNaN(n)){
    s=tab[0];
    s1=tab[2];
    first=false;
  } else {
    num=n;
  }
  var i=tab[1];
  var op=chaine.charAt(i);
  while(op=="+" || op=="-") {
    i++;
    tab=wgparseProduit(chaine,i);
    if (typeof(tab[0])=="number")
      n=parseFloat(tab[0]);
    else
      n=parseFloat("NaN");
    if (isNaN(n)){
      if (!first || op=="-"){
	s+=op;
	s1+=op;
      }
      first=false;
      s+=tab[0];
      s1+=tab[2];
    } else {
      if (op=="+")
	num+=n;
      else
	num-=n;
    }
    i=tab[1];
    op=chaine.charAt(i);
  }
  if (num>0){
    if (s==""){
      s=num.toString();
      s1=num.toString();
    } else {
      s+="+"+num;
      s1+="+"+num;
    }
  }
  else if (num<0){
    s+=num;
    s1+=num;
  }
 //alert("fin parse somme "+s);
  return [s,i,s1];
}

function wgParseExpression(chaine){
  chaine+="";
  // pris en compte des touches d'exposants
  var reg;
  var charCarre=String.fromCharCode(178);
  reg=RegExp(charCarre,"g");
  chaine=chaine.replace(reg,"^2");
  var charCube=String.fromCharCode(179);
  reg=RegExp(charCube,"g");
  chaine=chaine.replace(reg,"^3");
  for(i=4 ;i<10;i++){
    reg=RegExp(String.fromCharCode(i+8304),"g");
    chaine=chaine.replace(reg,"^"+i);
  }
  var tab=wgparseSomme(chaine,0);
 //alert("fin parseSomme : "+tab[0]+" "+tab[2]);
  chaine=tab[2];
  tab=wgparseSomme(chaine,0);
 //alert(tab[2])
  return [tab[0],tab[2]];
}







