function supprime(sID){
  var id=document.getElementById(sID);
  if(id.parentNode)
	id.parentNode.removeChild(id);
}
function insere(str,sID,isIE){
  var id=document.getElementById(sID);
  if (!id)
	return;
  var svgElement;
  if (isIE)
    svgElement=parseXML(str,document);
  else {
   var range = document.createRange();
    range.selectNodeContents(id); //(svg)
    svgElement = range.createContextualFragment(str);   
  }
  id.appendChild(svgElement);
}

