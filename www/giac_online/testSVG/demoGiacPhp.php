<?php
header("Content-Type:application/xhtml+xml ");
echo "<?xml version='1.0' encoding='UTF-16'?>\n";
?>

<!DOCTYPE html PUBLIC
    "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN"
    "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd">




<html xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:svg="http://www.w3.org/2000/svg"
  xml:lang="en">

<head>
  <title>Xcas en ligne</title>
<?php  
      //for inline Adobe SVG:
  echo "<object id='AdobeSVG' classid='clsid:78156a80-c6a1-4bbf-8e6a-3cd390eeb4e2'></object><?import namespace='svg' implementation='#AdobeSVG'?>";
?>
  <script   type="text/javascript"  src="demoGiacPhp.js"></script>
  <link rel="stylesheet" href="demoGiacPhp.css" type="text/css" /> 
  <!--[if !IE]> <-->   
      <link rel="stylesheet" href="correctifFF.css" type="text/css" />
  <!--> <![endif]-->
   
</head>
  
<body onload="prog();recadre();svgGrid(-5,-5,5,5);initialiseEditeur()">

<div class="haut">
  <p class="doc">
  <img src="img/info.gif"/>&nbsp;
  <select id="doc" onchange="aide(this)">
    <option>Exemples</option>
    <option>Calcul formel</option>
    <option>Programmation</option>
    </select>
  </p>
    <p class="choix">
      <a id="t0" href="javascript:afficheCadre(0)" title="console" style="color:orange">
         <img src="img/konsole.gif" />
      </a>
      <a id="t1" href="javascript:afficheCadre(1)"  title="programmation">
         <img src="img/run.gif" /> 
      </a>
      <a id="t2" href="javascript:afficheCadre(2)"  title="tableur">
        <img src="img/kspread.gif" />
      </a>
      <a id="t3" href="javascript:afficheCadre(3)"  title="graphiques">
        <img src="img/graph.gif" />
      </a>
      <a id="t4" href="javascript:afficheCadre(4)"  title="suites">
        <img src="img/suite.gif" />
      </a>
      <a id="t5" href="javascript:afficheCadre(5)"  title="edition scientifique">
        <img src="img/kate.gif" />
      </a>
      <a id="t5" href="javascript:imprimer()" title="imprimer ou enregistrer">
        <img src="img/impr.gif" />
      </a>
    </p>
    <p class="syntaxe">
	Syntaxe
        <select id="syntaxe">
          <option value="0">Xcas</option>
          <option value="1">Maple</option>
          <option value="2">Mupad</option>
          <option value="3">TI89</option>
        </select>&nbsp;
	<img src="img/c.gif" />
        <input type="checkbox" onclick="complex_mode(this)"/>
    </p> 
</div>

<div id="milieu" class="milieu">

<div  class="aide" id="aide">
  <object data="http://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/fr/casrouge/casrouge.html" 
  id="tutoProg" type="text/html" style="display:none">
  </object>

  <object data="http://www-fourier.ujf-grenoble.fr/~parisse/giac/doc/fr/casflan/casflan.html" 
  id="tutoCalc" type="text/html" style="display:none">
  </object>

  <object data="aide_fr.html" id="exemples" type="text/html" style="display:block">
  </object>
</div>

<div class="tableur" id="tableur" >
<div class="entree">
<form action="javascript:fill_cell()" style="padding:0px; margin:0px;">
  &nbsp;<b id="tab_cell">A0</b> : 
<input onkeyup="modeSaisie(event,this)" onkeydown="ctrl(event,this)" style="width:300px;" id="tab_entry" />
 &nbsp;
</form>
</div>
<div class="outils">
<a href="javascript:fill_bottom()" title="Remplir en bas">
<img src="img/bas.gif" />
</a>
<a href="javascript:fill_right()" title="Remplir &agrave; droite">
<img src="img/droit.gif" />
</a>
<a href="javascript:annule()" title="Annuler" style="color:red">
<img src="img/annul.gif" />
</a>
<a href="javascript:vide()" title="Vide la feuille" style="color:red">
<img src="img/initial.gif" />
</a>
</div>
<div class="conteneur" id="conteneur">
<?php
  $nb_line=20;
  $nb_col=7;
  print "<script>n_lign=$nb_line; n_col=$nb_col;nb_lign=1; nb_col=1;</script>\n";
  print "<div class='tableau'>\n";
  print "<table id='table'>\n";
  print "<tbody>\n";
  print "<tr >\n";
  print "<td  class='col1'></td>\n";
  $ord = ord("A");
  for ($j=0;$j<$nb_col;$j++) {
    print "<td bgcolor=\"lightGrey\" align=\"center\" >";
    print chr($ord+$j);
    print "</td>\n";
  }
  print "</tr>\n";
  for ($i=0; $i<$nb_line ; $i++) {
    print "<tr><td class='col1'>".$i."</td>\n";
    for ($j=0 ; $j<$nb_col ; $j++) {
      $ord = ord("A");
      $col =chr($ord+$j);
      print "<td  id=\"".$col.$i."\"  formule='' result='' onclick=\"cell('".$col.$i."')\" ></td>";
    }
    print "</tr>\n";
  }
  print "</tbody>\n";
?>
</table>
<script> cell_active='A0'; cell('A0')</script>
</div>
</div>
</div>

<div  id="progr" class="programme">
<textarea  id="script" onclick="javascript:nouveauScript()">
//taper votre programme ici :
</textarea> 
</div>

<div class="console" id="console"
onclick="document.getElementById('in').focus()">
<p>
<b>Xcas en ligne</b>. <i>Version 2 alpha</i>.
Tapez une instruction dans la console :
</p>
<p id="histo">
</p>
<form action="javascript:giac(document.getElementById('in'))">
      <input type='text' id='in' size='50' onkeydown="javascript:fleche(event);"/>    
</form>
<p id="fin">
</p>
</div>

<div class="svg" id="svg">

<!--[if IE]>
 <div><svg:svg id="cadreSVG"  version="1.0" baseProfile="full" width="13cm" height="13cm"></svg:svg></div>
<![endif]-->

<!--[if !IE]> <-->
 <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"  baseProfile="full"  width="13cm" height="13cm" id="cadreSVG">
 </svg>
<!--> <![endif]-->

<div class="histoSVG">
<input class="combo" id="combo"  onchange="giac(this)"/>
<select id="histSVG" size="5" onchange="comboSVG(this)">
<option>
aaaaaa
</option>
</select>
<input type="button" value="OK" onclick="document.getElementById('combo').value+=''"
style="position:absolute; left:265px;width:40px" />
<input type="button" value="Effacer" onclick="supprimeSVG()"
style="position:absolute; left:310px;width:70px;" />
<input type="button" value="Fen&ecirc;tre" 
onclick="document.getElementById('fenetre').style.display='block'"
style="position:absolute; left:385px" />
</div>

<form id="fenetre">
<table>
<tr>
<td>x<sub>min</sub></td><td><input id="xm" value="-5" /></td>
</tr><tr>
<td>y<sub>min</sub></td><td><input id="ym" value="-5" /></td>
</tr><tr>
<td>x<sub>max</sub></td><td><input id="xM" value="5" /></td>
</tr><tr>
<td>y<sub>max</sub></td><td><input id="yM" value="5" /></td>
</tr>
<tr>
<td colspan="2">
<input type="button" value="ok" onclick="newSvgGrid(this.parent)"/>
<input type="reset" value="d&eacute;faut" />
<input type="button" value="fermer" 
 onclick="document.getElementById('fenetre').style.display='none'" />
</td>
</tr>
</table>
</form>
</div>

<div class="svg" id="suite">
<div class="saisieSuite">
<p>Vous pouvez g&eacute;n&eacute;rer une ou deux suites (crois&eacute;es ou non) par r&eacute;currence <b>simple</b>.
<br />
<i>Dans les formules, taper u_n pour u<sub>n</sub> et v_n pour v<sub>n</sub></i>
</p>
<table style="width:100%">
<tr>
<td>u<sub>0</sub>=<input id="u0" size="6"/></td>
<td>u<sub>n+1</sub>=<input id="uRec" size="30" />&nbsp;&nbsp;
</td>
</tr>
<tr>
<td>v<sub>0</sub>=<input id="v0" size="6" /></td>
<td>v<sub>n+1</sub>=<input id="vRec" size="30" />&nbsp;&nbsp;
</td>
</tr>
</table>
<p>
Calcul jusqu'&agrave; l'indice 
<input id="nDer" value="10" size="2"/>
Ecriture d&eacute;cimale
<input type="checkbox" id="decimale" />
<input type="button" value="Calculer" onclick="suiteGen()" />
</p>
</div>
<div class="sortieSuite" id="sortieSuite">
</div>
</div>

<div class="svg" id="texte">
<div class="histoSVG">
 <select id="formatBlock" style="width:125px;" onchange="appliqueStyle(this.options[this.selectedIndex].value)">
    <option value="formatBlock;p">Paragraphe</option>
    <option value="formatBlock;h1">Titre</option>
    <option value="formatBlock;h2">Sous-titre</option>
    <option value="formatBlock;h3">Sous-sous-titre</option>
    <option value="insertUnorderedList">Liste puces</option>
    <option value="insertOrderedList">Liste num</option>
    <option value="bold;">gras</option>
  </select>
<button onclick="doFormat('bold');"><strong>B</strong></button>
<button onclick="doFormat('italic');"><em>I</em></button>
<button onclick="doFormat('underline');"><span style="text-decoration:underline">U</span></button>
<button onclick="doFormat('overline');">
<span style="text-decoration:overline">Z</span>
</button>
<img src="img/align-left.gif"  onclick="doFormatFocus('justifyLeft');"/>
<img src="img/align-center.gif" onclick="doFormatFocus('justifyCenter');"/>
<img src="img/align-right.gif" onclick="doFormatFocus('justifyright');"/>
<button onclick="modeCAS()">XCAS</button> 
<select id="modeMath" style="width:100px;">
  <option value="m">Non eval</option>
  <option value="g">Eval</option>
  <option value="=">Les deux</option>
</select>
<img src="img/math-display.gif" />
</div>
</div>
</div>

<!-- A part car firefox n aime pas contenteditable dans les div emboitees -->
<div id="editeur" contenteditable="true"  onkeyup="detecteCtrl(event,this)">
Contenu a modifier
</div>

<p class="bas">
&nbsp;
<a href="http://www-fourier.ujf-grenoble.fr/~parisse/giac_fr.html">
  T&eacute;l&eacute;charger Xcas
</a>
<span class="bouton" onclick="touche('^')">
x<sup>n</sup>
</span>
<span class="bouton"  onclick="touche('sqrt(')">
<img src="img/sqrt.gif" />
</span>
<span class="bouton"  onclick="touche('exp(')">
e<sup>x</sup>
</span>
<span class="bouton" onclick="touche('arccos(')">
cos<sup>-1</sup>
</span>
<span class="bouton" onclick="touche('arcsin(')">
sin<sup>-1</sup>
</span>
<span class="bouton"  onclick="touche('arctan(')">
tan<sup>-1</sup>
</span>
<span class="bouton"  onclick="touche('pi')">
&#960;
</span>
<span class="bouton"  onclick="touche('infinity')">
&#8734;
</span>
<span class="bouton"  onclick="touche(ans)">
R&eacute;p
</span>
<input type="button" 
value="Effacer la console"
onclick="javascript:effaceHisto()" />     
</p>
</body>
</html>