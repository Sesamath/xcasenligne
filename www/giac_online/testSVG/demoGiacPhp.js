//   Copyright (C) 2008 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


var isIE = window.ActiveXObject;
var isOpera=(navigator.appName=="Opera");
var mathmlURL = "http://www.w3.org/1998/Math/MathML";
var svgURL="http://www.w3.org/2000/svg";
var fenetres=["console","progr"];
var changeScript=true;
var htmlSymbols=["&int;","&Integral;","&Sigma;","&part;","&nbsp;",
		 "&rarr;","&VerticalBar;","&deg;","&infin;","&InvisibleTimes;",
		 "&pi;","&le;","&ge;"];
var utfSymbols=["\u222B","\u0222B","\u03A3","\u2202","\u0020",
		"\u2192","|","\u000B0","\u221E","", 
		"\u03C0","\u2264","\u2265"];
var FLECHE_HAUTE=38,FLECHE_BASSE=40, FLECHE_GAUCHE=37, FLECHE_DROITE=39;
var input=document.getElementById("in");
var tabHisto=new Array();
var activeInput;
var indexTabHisto=0;
var ans;
var indexSVG=0;

function fleche(ev){
  if (ev.keyCode!=FLECHE_HAUTE && ev.keyCode != FLECHE_BASSE)
    return;
  if (ev.keyCode==FLECHE_HAUTE && indexTabHisto>0){
    indexTabHisto--;
    input.value=tabHisto[indexTabHisto];
  }
  if (ev.keyCode==FLECHE_BASSE && indexTabHisto<tabHisto.length){
    indexTabHisto++;
    if (indexTabHisto<tabHisto.length)
      input.value=tabHisto[indexTabHisto];
    else 
      input.value="";
  }
  return;
}

function touche(instruction){
  activeInput.value+=instruction;
  activeInput.focus();
  return;
}

function aide(select){
  var t=["exemples","tutoCalc","tutoProg"];
  var i;
  var id;
  for (i=0; i<t.length ; i++){
    id=document.getElementById(t[i]);
    if (i==select.selectedIndex)
      id.style.display="block";
    else
      id.style.display="none";
  }
  return;
}


// pour Ie
function recadre(){
  activeInput=document.getElementById("in");
  activeInput.focus();
  // sort si pas internet explorer
  if (!isIE) 
    return;
  var elem;
  elem=document.getElementById("milieu");
  elem.style.height=document.body.clientHeight-66;
  elem=document.getElementById("conteneur");
  elem.style.height=document.body.clientHeight-95;
  elem=document.getElementById("editeur");
  elem.style.height=document.body.clientHeight-98;
  elem.style.position="absolute";
  //81;
  return;
}

window.onresize=recadre;

function ie(){
document.getElementById("contenu").style.right="10px";
}

function createElementMathML(t) {
  if (isIE) return document.createElement("mml:"+t);
  else return document.createElementNS(mathmlURL,t);
}

function createElementSVG(t) {
  if (isIE) return document.createElement("svg:"+t);
  else return document.createElementNS(svgURL,t);
}

function nouveauScript(){
  changeScript=true;
}

function reqInit(){
  var req;
  if(window.XMLHttpRequest)
    req = new XMLHttpRequest(); 
  else if(isIE) 
    req = new ActiveXObject("Microsoft.XMLHTTP"); 
  else { 
    alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
    return; 
  } 
  return req;
}

function reqG(req, s, url){
  req.open('post',url, false);
  req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  req.send("in="+encodeURIComponent(s));
  var i;
  var s=req.responseText;
  var reg;
  for(i=0;i<htmlSymbols.length;i++){
    reg=RegExp(htmlSymbols[i],"g");
    s=s.replace(reg,utfSymbols[i]);
  }
  return s;
}


function reqGIAC(req, s){
  return reqG(req,s,"giac_text.php");
}

function reqMath(req, s){
 //alert("reqMath");
  return reqG(req,s,"giac_math.php");
}

function reqMathGIAC(req, s){
  return reqG(req,s,"giac_math_eval.php");
}
 
function reqSPREAD(req,s){
  return reqG(req,s,"giac_spread.php");
}

function reqSVG(req, s){
  return reqG(req,s,"giac_svg.php");
}

function reqSuiteMathml(req, s){
  return reqG(req,s,"giac_suite_mathml.php");
}

function reqSuite(req, s){
  return reqG(req,s,"giac_suite.php");
}


function reqSVGgrid(req,xmin,ymin,xmax,ymax){
  return reqG(req,"","giac_svg_grid.php")
}

// envoie le contenu de la fenetre "programmation"
function prog(){
  if (!changeScript)
    return;
  var id=document.getElementById("script");
    // echange avec le serveur
  var req=reqInit();
  reqGIAC(req,id.value);
  changeScript=false;
  return;
} 

// mode complexe ou reel
function complex_mode(cbox){
  var mode=0;
  if (cbox.checked)
    mode=1;
  var req=reqInit();
  reqGIAC(req,"complex_mode:="+mode);
  return;
}

// on affiche le resultat en mathml
function insereMathML(id,s,inline,remplace){
  if (inline)
    display="inline";
  else
    display="block";
  if(isIE || isOpera){
    var reg=RegExp("<","g");
    s=s.replace(reg,"<mml:");
    reg=RegExp("<mml:/","g");
    s=s.replace(reg,"</mml:");
    var mml = document.createElement("mml:math");
    mml.innerHTML+=s;
  } else {
    var range = document.createRange();
    var mml=createElementMathML("math");
    range.selectNodeContents(mml);
    var fragment = range.createContextualFragment(s);
    mml.appendChild(fragment);
  }
  mml.setAttribute("display",display);
  if (isIE)
    mml.update();
  if (remplace)
    id.parentNode.replaceChild(mml,id);
  else
    id.appendChild(mml);
  return mml;
}

// on affiche le resultat en SVG
function insereSVG(id,s){
  alert(id);
  //var reg=RegExp("anchor:end","g");
  //s=s.replace(reg,"anchor:middle");
  if (indexSVG>0)
    s="<g id='svg"+indexSVG+"'>"+s+"</g>";
  if(isIE){
    reg=RegExp("xmlns=\"http://www.w3.org/2000/svg\"","g");
    s=s.replace(reg,"");
    reg=RegExp("<","g");
    s=s.replace(reg,"<svg:");
    reg=RegExp("<svg:/","g");
    s=s.replace(reg,"</svg:");
    var nooeud=parseXML(s,document);
    alert("noeud:"+noeud);
    id.appendChild(noeud);
    //id.innerHTML+=s;
    //alert(id);
  } else {
    var range = document.createRange();
    range.selectNodeContents(id); //(svg)
    var fragment = range.createContextualFragment(s);
    id.appendChild(fragment);
  }
    alert(s);

}

function supprimeSVG(){
  var hist = document.getElementById('histSVG');
  var n=hist.options[hist.selectedIndex].getAttribute("isvg");
  //alert(n);
  var objSVG=document.getElementById("svg"+n);
  var parent =objSVG.parentNode;
  parent.removeChild(objSVG);
  var i;
  hist.remove(hist.options[hist.selectedIndex]);
  document.getElementById("combo").value="";
  hist.selectedIndex=0;
  comboSVG(hist);
  return;
}

function svgGrid(xmin,ymin,xmax,ymax){
  var req=reqInit();
  var fenetre="xyztrange("+xmin+","+xmax+","+ymin+","+ymax+",-5,5,"+xmin+","+xmax+","+ymin+","+ymax+",-2,2,1,0,1)";
  //alert(fenetre);
  reqGIAC(req, fenetre);
  var idSVG=document.getElementById("cadreSVG");
 //alert(idSVG);
  if (idSVG.firstChild)
    idSVG.removeChild(idSVG.firstChild);
  var sr=reqSVGgrid(req);
  insereSVG(idSVG,sr);
  return;
}

function newSvgGrid(form){
  var xm=document.getElementById("xm").value;
  var ym=document.getElementById("ym").value;
  var xM=document.getElementById("xM").value;
  var yM=document.getElementById("yM").value;
  svgGrid(xm,ym,xM,yM);
  form.style.display="none";
  var req=reqInit();
  var s;
  var idSVG=document.getElementById("figures");
  var histSVG=document.getElementById("histSVG");
  for (var i=0 ; i<histSVG.options.length ; i++){
    s=reqSVG(req,histSVG.options[i].text);
    insereSVG(idSVG,s);
  }
  return;
}

// mise a jour de l'input "combo"
function comboSVG(hist){
  var combo=document.getElementById("combo");
  combo.value=hist.options[hist.selectedIndex].text;
  hist.blur();
  combo.focus();
  return;
}

function giac(input){
  prog(); 
  // syntaxe
  var req=reqInit();
  var id=document.getElementById("syntaxe");
  var syntaxe="maple_mode("+id.selectedIndex+")";
  reqGIAC(req,syntaxe);
  
  //input=document.getElementById("in");
  s=input.value; 
  if (s == "")
    return;
  
  // on enregistre la requete
  tabHisto.push(s);
  indexTabHisto=tabHisto.length;
  
  
  input.value="";
  input.focus();
  var t=document.createTextNode(s+" ");
  
  
  // on affiche le r�sultat svg
  var idSVG=document.getElementById("figures");
  alert("idsvg:"+idSVG);
  var sr=reqSVG(req,s);
  if (sr != "E"){
    indexSVG++;
    var hist=document.getElementById("histSVG");
    var t=sr.split(new RegExp("`","g"));
    alert(t);
    insereSVG(idSVG,t[0]);
    insereSVG(document.getElementById("cadreSVG").firstChild,t[1]);
    // on affiche l'instruction
    var option = new Option(hist.options[hist.options.length-1].text);
    hist.options[hist.length] = option;
    for (var i=hist.options.length-2; i>0 ; i--){
      hist.options[i].text=hist.options[i-1].text;
      hist.options[i].setAttribute("isvg",hist.options[i-1].getAttribute("isvg"));
    }
    hist.options[0].text=s;
    hist.options[0].setAttribute("isvg",indexSVG);
    hist.selectedIndex=0;
    comboSVG(hist);
  }
  else {
    sr=reqGIAC(req,s);
   //alert(sr);
    // on affiche l'instruction
    id=document.getElementById("histo");
    id.appendChild(t);
    // on affiche le resultat en mathml
    var t=sr.split(new RegExp("`","g"));
    ans=t[0];
    insereMathML(id,t[1]);
    // on scrolle en bas
    var fin=document.getElementById("fin");
    fin.scrollIntoView(false); 
  }  
  return;
}

  
function effaceHisto(){
  // sur le serveur
  var req=reqInit();
  var url="giac_text.php";
  req.open('post',url, false);
  req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  req.send("efface="+encodeURIComponent("on"));
  // sur le poste local 
  var histo=document.getElementById("histo");
  if (!histo.firstChild)
    return;
  while (histo.childNodes.length>0)  
    histo.removeChild(histo.lastChild);
  return;
}


function afficheCadre(choix){
  var cadre=["console","progr","tableur","svg","suite","texte"];
  var cadreInput=["in","script","tab_entry","combo","u0","editeur"];
  var elem;
  var i;
  for (i=0; i<cadre.length ; i++){
    elem=document.getElementById(cadre[i]);
    if (i==choix){
      if (choix==0)//console
	elem.style.height="100%";
      elem.style.display="block";
    }
    else if (i==0 && choix==1){
      elem.style.height="50%";
      elem.style.display="block";
    }
    else
      elem.style.display="none"
	}
  var activeInput=document.getElementById(cadreInput[choix]);
  //alert(document.getElementById("combo"));
  if (cadre[choix]=="texte")
    activeInput.style.display="block";
  else
    document.getElementById("editeur").style.display="none";
  activeInput.focus();
  return;
}


//--------- gestion du tableur ----------

// initialisation dans la page :
var cell_active;
var cell_marquees=new Array();
var n_lign, nb_lign, nb_lign_sauv;
var n_col, nb_col, nb_col_sauv;
var lettre=['A','B','C','D','E','F','G'];
var saisie=false;

function ctrl(ev,tab_entry){
 if (ev.keyCode==17)
    tab_entry.select();
 return;
}

function modeSaisie(ev,tab_entry){
  if (ev.ctrlKey){
    if (ev.keyCode==67){
      copy();
      saisie=false;
    } else if (ev.keyCode==86)
      paste();
    return;
  }
  if (ev.keyCode==13){
    saisie=false;
    return;
  }
  if (tab_entry.value.charAt(0)!="=")
    return;
  if (ev.keyCode!=17)
    saisie=true;	
  return;
}

function coordonnees(cellule){
  var s=cellule.slice(1);
  return [cellule.charAt(0), parseInt(s)];
}

function cell_max(cellule){
  var t=coordonnees(cellule);
  var A="A";
  nb_col_sauv=nb_col;
  nb_lign_sauv=nb_lign;
  nb_lign=Math.max(nb_lign,t[1]+1);
  nb_col=Math.max(nb_col,Number(t[0].charCodeAt(0))-A.charCodeAt(0)+1);
  return;
}

function cell(cellule){
  var tab_entry=document.getElementById("tab_entry");
  if (saisie){
    tab_entry.focus();
    tab_entry.value+=cellule;
    return;
  }
  var dom_cellule=document.getElementById(cell_active);
  dom_cellule.style.backgroundColor="white";
  dom_cellule.style.color="black";
  cell_active=cellule;
  dom_cellule=document.getElementById(cell_active);
  dom_cellule.style.backgroundColor="#ffff99";
  var tab_cell=document.getElementById("tab_cell");
  tab_cell.firstChild.nodeValue=cellule;
  tab_entry.focus();
  tab_entry.value=dom_cellule.getAttribute("formule");
}

function annule(){
  var i,t,dom_cellule;
  nb_lign=nb_lign_sauv;
  nb_col=nb_col_sauv;
  for (i=0; i<cell_marquees.length;i++){
      dom_cellule=document.getElementById(cell_marquees[i]);
      t=dom_cellule.getAttribute("ancienne");
      dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
      dom_cellule.setAttribute("formule",t);
    }
  calculeTab();
  return;
}

function subst_cell(){
 var dom_cellule=document.getElementById(cell_active);
 var tab_entry=document.getElementById("tab_entry");
 dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
 dom_cellule.setAttribute("formule",tab_entry.value);
 return;
}

function vide(){
  var i,j, dom_cellule, t,cell;
  cell_marquees=[];
  for (i=0; i<nb_col; i++)
    for (j=0; j<nb_lign ; j++){
      cell=lettre[i]+j;
      dom_cellule=document.getElementById(cell);
      dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
      dom_cellule.setAttribute("formule","");
      cell_marquees.push(cell);
      if (dom_cellule.firstChild)
	dom_cellule.removeChild(dom_cellule.firstChild);
    }
  //calculeTab();
  //cell_active="A0";
  // cell_max("A0");
  return;
}

function fill_cell(){
  cell_marquees=[cell_active];
  cell_max(cell_active);
  subst_cell();
  var t=coordonnees(cell_active);
  calculeTab();
  if (t[1]<n_lign-1){
    t[1]+=1;
    saisie=false;
    cell(t[0]+t[1]);
  }
  return;
}


// translation de coordoon�es de cellules
function translation(chaine,k,l){
  var new_ref=new Array();
  var old_ref=new Array();
  var new_string="";
  var n=0;
  var i=0; 
  var j=0;
  var reg;
  var reg_lettre=RegExp(".[A-E]","g");
  var reg_nombre=RegExp("[0-9]","g");
  var letter="";
  var dollar="$";
  //on translate A$0, A$1 ...
  reg=RegExp(".[A-E]\\$(?=[0-9])","g");
  if (reg.test(chaine)){
    old_ref=chaine.match(reg);
    for (i=0 ; i!=old_ref.length ; i++){ 
      letter=String(old_ref[i].match(reg_lettre));
      dollar=letter.charAt(0);
      if (dollar!='$'){
	code=letter.charCodeAt(1);
	letter=String.fromCharCode(code+k)+"$";
	new_ref.push(dollar+letter);
      }
    }
    for (i=0 ; i!=new_ref.length ; i++){
      chaine=chaine.replace(reg,new_ref[i]+"ref");
    }
  }
  //on translate A0 ... A9
  reg_lettre=RegExp(".[A-E]","g");
  reg_nombre=RegExp("[0-9]","g");
  reg=RegExp(".[A-E][0-9](?![0-9])","g");
  if (reg.test(chaine)){
    old_ref=chaine.match(reg);
    for (i=0 ; i!=old_ref.length ; i++){
      letter=String(old_ref[i].match(reg_lettre));
      dollar=letter.charAt(0);
      if (dollar!='$'){
	code=letter.charCodeAt(1);
	letter=dollar+String.fromCharCode(code+k);
      }
      n=Number(old_ref[i].match(reg_nombre));
      n=n+l;
      new_ref.push(letter+n);
    } 
    for (i=0 ; i!=new_ref.length ; i++){
      reg=RegExp("\\"+old_ref[i]+"(?!(ref|[0-9]))","g");
      chaine=chaine.replace(reg,new_ref[i]+"ref");
    }
  }
  // on translate A10 ...
  reg_nombre=RegExp("[0-9][0-9]","g");
  reg=RegExp(".[A-E][0-9][0-9]","g");
  new_ref=new Array();
  if (reg.test(chaine)){
    old_ref=chaine.match(reg);
    for (i=0 ; i!=old_ref.length ; i++){     
      letter=String(old_ref[i].match(reg_lettre));
      dollar=letter.charAt(0);
      if (dollar!='$'){
	code=letter.charCodeAt(1);
	letter=dollar+String.fromCharCode(code+k);
      }
      n=Number(old_ref[i].match(reg_nombre));
      n=n+l;
      new_ref.push(letter+n);
    }
    for (i=0 ; i!=new_ref.length ; i++){
      reg=RegExp("\\"+old_ref[i]+"(?!ref)","g");
      chaine=chaine.replace(reg,new_ref[i]+"ref");
    }
  } 
  // on enl�ve les refs
  reg=RegExp("ref","g");
  chaine=chaine.replace(reg,"");
  return chaine;
}

var ref_copy;
var form_copy;
function copy(){
  saisie=false;
  ref_copy=cell_active;
  dom_cellule=document.getElementById(cell_active);
  var t=document.getElementById("tab_entry");
  form_copy=t.value;
  dom_cellule.setAttribute("formule",t.value);
  return;
}

// pose probleme : si la cellule de d�part est modifiee
// la cellule cible n'est pas actualisee
// mettre en place un repertoire des cellules cibles
// et prevoir la mise a jour
function paste(){
  cell_marquees=[cell_active];
  cell_max(cell_active);
  var reg_lettre=RegExp("[A-E]","g");
  var lettre_cell=String(cell_active.match(reg_lettre));
  var lettre_ref=String(ref_copy.match(reg_lettre));
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre_cell=Number(cell_active.match(reg_nombre));
  var nombre_ref=Number(ref_copy.match(reg_nombre));
  dom_cellule=document.getElementById(cell_active);
  var c=translation(form_copy, lettre_cell.charCodeAt(0)-lettre_ref.charCodeAt(0) ,nombre_cell-nombre_ref );
  dom_cellule.setAttribute("formule",c);
  var tab_entry=document.getElementById("tab_entry");
  tab_entry.value=c;
  calculeTab();
  return;
}

function fill_bottom() {
  cell_marquees=[];
  subst_cell();
  var cell=document.getElementById(cell_active);
  var formule=cell.getAttribute("formule");
  var reg_lettre=RegExp("[A-E]","g");
  var letter=cell_active.match(reg_lettre);
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre=Number(cell_active.match(reg_nombre));
  var i=0;
  for (i=nombre+1 ; i<n_lign ; i++){ 
    cell_marquees.push(letter+i);
    cell =document.getElementById(letter+i);
    cell.setAttribute("formule",translation(formule,0,i-nombre));
  }
  i=n_lign-1;
  cell_max(lettre[n_col-1]+i);
  calculeTab();
  saisie=false;
  return;
}

function fill_right() {
  cell_marquees=[];
  subst_cell();
  var cell=document.getElementById(cell_active);
  var formule=cell.getAttribute("formule");
  var cell=document.getElementById(cell_active);
  var reg_lettre=RegExp("[A-Z]","g");
  var letter=String(cell_active.match(reg_lettre));
  var code=Number(letter.charCodeAt(0));
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre=Number(cell_active.match(reg_nombre));
  var i=0;
  for (i=1 ; i<n_col ; i++){ 
    letter=String.fromCharCode(code+i);
    cell_marquees.push(letter+nombre);
    cell =document.getElementById(letter+nombre);
    cell.setAttribute("formule",translation(formule,i,0));
  }
  i=n_lign-1;
  cell_max(letter+i);
  calculeTab();
  saisie=false;
  return;
}

function spreadForm(i,j){
  var cell=document.getElementById(lettre[j]+i);
  var f=cell.getAttribute("formule");
  if (f!="" && f!=" ")
    return cell.getAttribute("formule");
  else
    return "0";
}

function cellSubst(cell,s,m){
  var l,L,f;
  result=cell.getAttribute("result");
  if (result==s)
    return;
  cell.setAttribute("result",s);
  if (cell.firstChild)
    cell.removeChild(cell.firstChild);
  l=cell.clientWidth+0;
  var f=cell.getAttribute("formule");
  if (f!="" && f!=" "){
    insereMathML(cell,m);
    if (isIE){
      var tab=document.getElementById("tableau");
      L=cell.firstChild.clientWidth+0;
      if (L>l){
	 cell.style.width=L;
       tab.style.width=tab.clientWidth+L-l;
      }
      else
       cell.style.width=l;
    }
  }
  return;
}

function spreadSubst(i,j,s,m){
  var cell=document.getElementById(lettre[j]+i);
  cellSubst(cell,s,m);
  return;
}


function calculeTab(){
  var spreadheet=new String();
  spreadsheet="spreadsheet[";
  var cell=document.getElementById('AO');
  var i=0;
  var j=0;
  var der_col=nb_col-1, der_lign=nb_lign-1;
  for (i=0 ; i<nb_lign ; i++){
    spreadsheet+="[";
    for (j=0 ; j<der_col ; j++) 
      spreadsheet+="[regrouper("+spreadForm(i,j)+"),1,0],";
    spreadsheet+="[regrouper("+spreadForm(i,j)+"),1,0]]";
    if (i==der_lign)
      spreadsheet+="]";
    else
      spreadsheet+=",";
  }
  var req=reqInit();
  var s=reqSPREAD(req,spreadsheet);
  var t=s.split(new RegExp("`","g"));
  var k;
  // on affiche en premier la cellule active
  var act=coordonnees(cell_active);
  //var t0=new Date();
  var A="A";
  k=2*((Number(act[0].charCodeAt(0))-A.charCodeAt(0))+nb_col*act[1]);
  cellSubst(document.getElementById(cell_active),t[k],t[k+1]);
  //var t1=new Date();
  //alert(t1.getTime()-t0.getTime());
  for (i=0; i<nb_lign ; i++)
    for (j=0 ; j<nb_col ; j++){
      k=2*(i*nb_col+j);
      spreadSubst(i,j,t[k],t[k+1]);
    }
  return;
}


// Generateur de suites --------------
function suiteGen(){
  var u0=document.getElementById("u0").value;
  var uRec=document.getElementById("uRec").value;
  // a corriger
  //var  regU=RegExp("u(n)","g");
  //var  regV=RegExp("v(n)","g");
  //uRec=uRec.replace(regU,"u_n");
  //uRec=uRec.replace(regV,"v");
  //alert(uRec);
  var v0=document.getElementById("v0").value;
  var vRec=document.getElementById("vRec").value;
  //vRec=vRec.replace(regU,"u_n");
  //vRec=vRec.replace(regV,"v_n");
  var n=parseInt(document.getElementById("nDer").value)+1;
  var req=reqInit();
  var decimale=document.getElementById("decimale").checked;
  // on abandonne le mathml si trop de termes (lenteur)
  if (n>100){
    decimale=true;
    document.getElementById("decimale").checked=true;
  }
  var s=u0+"`"+uRec+"`"+v0+"`"+vRec+"`"+n+"`"+decimale;
  if (decimale)
    s=reqSuite(req,s);
  else
    s=reqSuiteMathml(req,s);  
  //affichage des suites
  var t=s.split(new RegExp("`","g"));
  var sortie=document.getElementById("sortieSuite");
  sortie.removeChild(sortie.firstChild);
  var indice=-1;
  var fin=t.length-1;
  // construire le tableau avec les id
  var s="<table style='width:100%'>";
  if (decimale){
    for (var i=0; i<fin ; i++){
      if (i%2==0){
	if (i%4==2)
	  s+="<tr style='background-color:silver'><td>";
	else
	  s+="<tr><td>"
	    indice++;
	s+="u<sub>"+indice+"</sub>="+t[i]+"</td>";
      } else
	s+="<td id='s"+i+"'>v<sub>"+indice+"</sub>="+t[i]+"</td></tr>";
    }
    s+="</table>";
    sortie.innerHTML+=s;
  } else {
    for (var i=0; i<fin ; i++){
      if (i%2==0){
	if (i%4==2)
	  s+="<tr style='background-color:silver'><td id='s"+i+"'>";
	else
	  s+="<tr><td id='s"+i+"'>"
	    indice++;
	s+="u<sub>"+indice+"</sub>=</td>";
      } else
	s+="<td id='s"+i+"'>v<sub>"+indice+"</sub>=</td></tr>";
    }
    s+="</table>";
  sortie.innerHTML+=s;
  // puis remplir le tableau
  var td;
  for (var i=0; i<fin ; i++){
    td=document.getElementById("s"+i);
    insereMathML(td,t[i],true);  //true pour "inline"
  }
  }
  return;
}

// ------ traitement de texte -----------

var editeur;

function initialiseEditeur(){
  editeur=document.getElementById("editeur");
  //editeur.designMode = "on";
  //document.execCommand("2D-position",false,false);
}

function getSelectedText(){
        var selectedText ="";
        if (window.getSelection){
                selectedText = window.getSelection();
        }else if(document.getSelection){
                selectedText = document.getSelection();
        }else if(document.selection){
                selectedText = document.selection.createRange().text;
        };
        return selectedText;
}

function doFormat(cmd) {
  if (cmd=="overline"){
    var t="<span style='text-decoration:overline'>"+getSelectedText()+"</span>"
     document.execCommand("inserthtml", false,t);
  }
  else
    document.execCommand(cmd, false, null);
}

function doFormatFocus(cmd){
  editeur.focus();
 //alert(document.queryCommandValue(cmd));
  document.execCommand(cmd,null,null);
  editeur.focus();
}


var saisieCAS=false;

function detecteCtrl(ev,editeur){
 //alert(ev.keyCode);
  if (ev.ctrlKey){
    if(ev.keyCode==77){  //ctrl m
      modeCAS()
    }
  }
  if (ev.keyCode==13 && saisieCAS){  //retour chariot dans champ math
    var mode=document.getElementById("modeMath");
    text2mathml(mode.options[mode.selectedIndex].value);
    saisieCAS=false;
  }
}

function modeCAS(){
  saisieCAS=true;
  insereCAS();
}

function insereCAS(mode){
  document.getElementById("editeur").focus();
  var html="<input id='entreeCAS'/>";//&nbsp;";
 //alert(html);
  if (isIE){
    var editeur = document.getElementById("editeur").document;
    var range = editeur.selection.createRange();
   //alert(html);
    range.pasteHTML(html);
  } else {
    document.execCommand("inserthtml",false,html);
  }
  var entreeCAS=document.getElementById("entreeCAS");
  //alert(entreeCAS.parentNode.parentNode.previousSibling.tagName);
  if (isIE){
    var p=entreeCAS.parentNode;
    var pp=p.previousSibling;
    p.innerHTML=pp.innerHTML+" "+p.innerHTML;
    p.parentNode.removeChild(pp);
    entreeCAS=document.getElementById("entreeCAS");
  }
  entreeCAS.focus();
  return;
}


function text2mathml(mode){
  var input=document.getElementById("entreeCAS");
  var entree;
  entree=input.value;
  //alert(input.parentNode);
 var req=reqInit();
 var sr;
//alert(mode);
 switch (mode){
 case "g" : 
   sr=reqGIAC(req,entree);
   break;
 case "m" : 
   sr=reqMath(req,entree);
   break;
 case "=" :
   sr=reqMathGIAC(req,entree);
   break;
 }
  // on affiche le resultat en mathml
  var t=sr.split(new RegExp("`","g"));
  var node=insereMathML(input,t[1],true,true);
  if (!isIE && !isOpera){
    elimineBr(node,true);
    elimineBr(node,false);
    //while (node.tagName!="br")
    // node=node.previousSibling;
    //node.parentNode.removeChild(node);
  }
}

function elimineBr(node,versAvant){
  var node1=node;
  if (versAvant){
    while (node1.tagName!="br" && node1.nextSibling)
      node1=node1.nextSibling;
  } else {
    while (node1.tagName!="br" && node1.previousSibling)
      node1=node1.previousSibling;
  }
  if (node1.tagName=="br")
    node1.parentNode.removeChild(node1);
}

function appliqueStyle(s){
  var t=s.split(";");
  document.execCommand(t[0],false,t[1]);
  editeur.focus();
  return;
}
var fenetre_impression;

function imprimer(){
  if (!fenetre_impression)
    fenetre_impression=window.open("impr.xhtml","impression");
  var texte=document.getElementById("texte");
  var editeur=document.getElementById("editeur");
  var form=document.createElement('form');
  if (!isIE)
    form.setAttribute("enctype","x-application/x-www-form-urlencoded");
  form.action="impr.php";
  form.method="post";
  form.target="impression";
  var input=document.createElement("input");
  input.type="hidden";
  input.name="impr";
  var s=editeur.innerHTML;
  input.value=s;
  form.appendChild(input);
  texte.appendChild(form);
  form.submit();

  //fenetre_impression =window.open("impr.xhtml"); 
  //fenetre_impression.document.close();
  //fenetre_impression.document.open();
  //alert("ok");
  //fenetre_impression.document.write(preambule+document.getElementById('editeur').innerHTML+fin);
  //fenetre_impression.document.write("Content-Type:application/xhtml+xml\n\n<html><body><h1>test</h1></body></html>");
  //fenetre_impression.document.close();
  //alert(fenetre_impression);
}
