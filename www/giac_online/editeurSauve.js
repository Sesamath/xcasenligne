
// ------ traitement de texte -----------


var cursorPos;
var saisieCAS=false;
var inline=true;
var sauveActiveInput;
var activeMml;


function getSelectedText(){
  var selectedText ="";
  if (window.getSelection){
    selectedText = window.getSelection();
  }else if(document.getSelection){
    selectedText = document.getSelection();
  }else if(document.selection){
    selectedText = document.selection.createRange().text;
  };
  return selectedText;
}

// pour les formats non predefinis :
function styleSurSelection(style){
  var t="<span style='"+style+"'>"+getSelectedText()+"</span>";

  if (isIE)
    document.selection.createRange().pasteHTML(t);
  else
    document.execCommand("inserthtml", false,t);
  return;
}

// pour les formats predefinis :
function doFormat(cmd) {
  document.execCommand(cmd, false, null);
}

function doFormatFocus(cmd){
  editeur.focus();
  //alert(document.queryCommandValue(cmd));
  document.execCommand(cmd,null,null);
  editeur.focus();
}

function appliqueStyle(s){
  editeur.focus();
  var t=s.split(";");
  //alert(t[1]);
  document.execCommand(t[0],false,"<"+t[1]+">");
  editeur.focus();
  return;
}

function testeStyle(){
  var selecteur=document.getElementById("formatBlock");
  var s=selecteur.options[selecteur.selectedIndex].value;
  if (s=="")
    return;
  var t=s.split(";");
  if (isIE){
    if (!(document.queryCommandValue(t[0])==true))
      selecteur.selectedIndex=1;
  } else {
    if (selecteur.selectedIndex>4 && !document.queryCommandState(t[0]))
      selecteur.selectedIndex=1;
  }
  if (!document.queryCommandValue(t[0]) && t[1])
    selecteur.selectedIndex=1;
}

function testeLesStyles(){
  // ne marche pas
  var selecteur=document.getElementById("formatBlock");
  var i;
  //for (i=5;i<selecteur.options.length;i++)
  selecteur.selectedIndex=0;
}

function detecteCtrl(ev){
  if (ev.keyCode==ENTREE)
    testeStyle();
  if (ev.ctrlKey){
    if(ev.keyCode==CTRL_M){  //ctrl m
      modeCAS(!ev.shiftKey)
	}
  }
  if (ev.keyCode==ENTREE && saisieCAS){  //retour chariot dans champ math
    //var mode=document.getElementById("modeMath");
    text2mathml("g");
  }
}



function disp(){
  editeur.focus();
  // var editeur = document.getElementById("editeur");
  //alert(canAcceptCommand());
}

function editeCAS(){
  var entreeCAS;
  if (!activeMml){
    insereCAS(inline);
    if (isIE){
      var p=entreeCAS.parentNode;
      var pp=p.previousSibling;
      p.innerHTML=pp.innerHTML+" "+p.innerHTML;
      p.parentNode.removeChild(pp);
    }aa
       entreeCAS=document.getElementById("entreeCAS");
  } else {
    var commande;
    var node=activeMml.firstChild;
    if (isIE){    
      node=node.firstChild;
    }
    commande=node.getAttribute("commande");
    inline=(node.getAttribute("display")=="inline");  
    editeur.focus();
    node.parentNode.removeChild(node);
    insereCAS(inline);
    entreeCAS=document.getElementById("entreeCAS");
    entreeCAS.setAttribute("value",commande);
    saisieCAS=true;
  }
  sauveActiveInput=activeInput;
  activeInput=entreeCAS;
  entreeCAS.focus();
  return;
}



function insereHTML(html){
  document.getElementById("editeur").focus();
  if (isWebKit){
    var fragment = document.createDocumentFragment();
    var div = document.createElement("div");
    div.innerHTML = html;
    while(div.firstChild)
      editeur.appendChild(div.firstChild);
    return;
  }
  if (isIE){
    var range = editeur.document.selection.createRange();
    var range = editeur.createRange();
    cursorPos=range;
    range.pasteHTML("<br/>"+html);
    return;
  }
  document.execCommand("inserthtml",false,html);
}

function insereCAS(dansLigne){
  saisieCAS=true;
  inline=dansLigne;
  var html="<span contentEditable='false' style='color:black;text-align:center' ><input id='entreeCAS'/></span><span contentEditable='false' id='boutonsCas'><input type='button' value='Tel quel' onclick='text2mathml(\"m\")' /><input type='button' value='Eval' onclick='text2mathml(\"g\")' /><input type='button' value='Les 2'  onclick='text2mathml(\"=\")'/></span>&#160;";
  insereHTML(html);
  //editeCAS(null,true);
  document.getElementById("entreeCAS").focus();
  return;
}

function dispMathML(){
  if (!activeMml)
    return;
  var node=activeMml.firstChild;
  if (isIE)
    node=node.firstChild;
  var disp=node.getAttribute("display");
  if (disp=="block")
    node.setAttribute("display","inline");
  else
    node.setAttribute("display","block");
  if (isIE){
    node.update();
    disp=node.getAttribute("display");
    activeMml.style.display=disp;
  }
  editeur.focus();
}

function text2mathml(mode){
  activeInput=sauveActiveInput;
  var input=document.getElementById("entreeCAS");
  //elimineInput(input.nextSibling);
  var entree;
  entree=input.value;
  var req=reqInit();
  var sr;
  //alert(mode);
  switch (mode){
  case "g" : 
    sr=reqGIAC(req,entree);
    break;
  case "m" : 
    sr=reqMath(req,entree);
    break;
  case "=" :
    sr=reqMathGIAC(req,entree);
    break;
  }
  // on affiche le resultat en mathml
  var t=sr.split(new RegExp("`","g"));
  var node=insereMathML(input,t[1],inline,true);
  node.setAttribute("commande",entree);
  if (!isIE && !isOpera){
    elimineBr(node,true);
    elimineBr(node,false);
  }
  editeur.focus();
  var click;
  if (isIE){
    //node.parentNode.ondblclick=function(){editeCAS(this,false);};
    //node.parentNode.onclick=function(){if(this.firstChild.firstChild.tagName=="math"){activeMml=this.firstChild.firstChild;setTimeout("dispMathML()",250);};};
    node.parentNode.onclick=function(){selectionne(this);};
  }
  else {
    node.parentNode.setAttribute("ondblclick","editeCAS(this,false)");
    //node.parentNode.setAttribute("onclick","activeMml=this.firstChild;setTimeout('dispMathML()',250)");
    node.parentNode.setAttribute("onclick","selectionne(this)");
  }
  node=document.getElementById("boutonsCas");
  node.parentNode.removeChild(node);
  if (isIE)
    cursorPos.select();
  saisieCAS=false;
}


function selectionne(node){
  if (activeMml){
    activeMml.style.color="black";
  }
  var color="red";
  if (activeMml==node){
    activeMml=null;
    color="black";
  }
  else
    activeMml=node;
  //alert(activeMml);
  node.style.color=color;
}

function elimineBr(node,versAvant){
  var node1=node;
  if (versAvant){
    while (node1.tagName!="br" && node1.nextSibling)
      node1=node1.nextSibling;
  } else {
    while (node1.tagName!="br" && node1.previousSibling)
      node1=node1.previousSibling;
  }
  if (node1.tagName=="br")
    node1.parentNode.removeChild(node1);
}


function insertAfter(newNode,node){
  if (node.nextSibling)
    node.parentNode.insertBefore(newNode,node.nextSibling);
  else
    node.parentNode.appendChild(newNode);
}

function changeCouleurMath(node){
  if (node.mathcolor)
    node.mathcolor="navy";
  var fils=node.firstChild;
  while (fils) {
    changeCouleurMath(fils);
    fils=fils.nextSibling;
  }
}

function cloneConsole(){
  var node=document.getElementById("histo");
  insereHTML("<span id='consAnchor'></span>");
  var consAnchor=document.getElementById("consAnchor");
  var cloneConsole;
  //node=node.lastChild;
  if (node.lastChild){
    cloneConsole=node.cloneNode(true);
    if (isIE)
      changeCouleurMath(cloneConsole);
    consAnchor.appendChild(cloneConsole);
  }
  else {
    alert("console vide");
    return;
  }
 
  consAnchor.removeAttribute("id");
  insertAfter(document.createElement("br"),consAnchor);
}

function cloneProgramme(){
  var script=document.getElementById("script");
  var html="<pre id='progAnchor'></pre>";
  insereHTML(html); 
  var progAnchor=document.getElementById("progAnchor");
  var texte=document.createTextNode(script.value);
  while (progAnchor.firstChild)
    progAnchor.removeChild(progAnchor.firstChild);
  progAnchor.appendChild(texte);
  progAnchor.removeAttribute("id");
  insertAfter(document.createElement("br"),progAnchor);
  editeur.focus();
}

function cloneTbody(){
  var Tbody=document.getElementById("table").firstChild;
  while(Tbody.nextSibling && Tbody.tagName!="tbody")
    Tbody=Tbody.nextSibling;
  var nodeLi,nodeCell;
  var nodeTbody=document.createElement("tbody"), nodeTR, nodeTD;    
  var i,j;
  var iDer,jDer;
  if (isIE){
    for (i=1; i<=nb_lign ; i++){
      nodeTR=document.createElement("tr");
      nodeTbody.appendChild(nodeTR);
      nodeLi=Tbody.childNodes[i];
      for (j=1 ; j<=nb_col ; j++){
	nodeTD=document.createElement("td");
	nodeTR.appendChild(nodeTD);
	nodeCell=nodeLi.childNodes[j];
	if (nodeCell.firstChild)
	  nodeTD.appendChild(nodeCell.firstChild.cloneNode(true));
      }
    }
  } else {
    iDer=1+2*nb_lign;
    jDer=1+nb_col;  
    for (i=3; i<=iDer ; i=i+2){
      nodeTR=document.createElement("tr");
      nodeTbody.appendChild(nodeTR);
      nodeLi=Tbody.childNodes[i];
      for (j=2 ; j<=jDer ; j++){
	nodeTD=document.createElement("td");
	nodeTR.appendChild(nodeTD);
	nodeCell=nodeLi.childNodes[j];
	if (nodeCell.firstChild)
	  nodeTD.appendChild(nodeCell.firstChild.cloneNode(true));
      }
    }
  }
  return nodeTbody;
}

function cloneTable(){
  if (nb_col==0 && nb_lign==0)
    alert("la feuille de calculs est vide");
  var nodeTbody=cloneTbody();
  var html="<table id='tableAnchor'></table>";
  insereHTML(html);
  var table=document.getElementById("tableAnchor");
  table.appendChild(nodeTbody);
  table.removeAttribute("id");
  insertAfter(document.createElement("br"),table);
  //insereHTML("<br/>"); 
  editeur.focus();
}

function cloneGraphique(){
  if (isIE){
    alert("Impossible sous Internet Explorer.\n Mais vous pouvez copier l'image avec le menu contextuel et la coller ensuite dans un traitement de textes")  ;
    return;
  }
  insereHTML("\n<h3 style='margin-left:20%'>Titre figure</h3>\n"); 
  var html="<span class='svgAnchor' id='svgAnchor'></span>";
  insereHTML(html); 
  var svgAnchor=document.getElementById("svgAnchor");
  svgAnchor.appendChild(cloneSVG());
  svgAnchor.removeAttribute("id"); 
  svgAnchor.setAttribute("contentEditable",false);
  insertAfter(document.createElement("br"),svgAnchor);
  editeur.focus(); 
}

function cloneTabSuite(){
  var sortieSuite=document.getElementById("sortieSuite");
  if (!sortieSuite.firstChild){
    alert("Pas de suite !");
    return;
  }
  var node=sortieSuite.firstChild;
  while (node.tagName!="table" && node.tagName!="TABLE" && node.nextSibling)
    node=node.NextSibling;
  return node;
}

function cloneSuite(){ 
  var html="<table id='suiteAnchor'></table>";
  insereHTML(html); 
  var suiteAnchor=document.getElementById("suiteAnchor");
  var node= cloneTabSuite();
  if (node.tagName!="table" && node.tagName!="TABLE"){
    alert("pas de suite");
    return;
  }
  suiteAnchor.appendChild(node.firstChild.cloneNode(true));
  suiteAnchor.removeAttribute("id"); 
  insertAfter(document.createElement("br"),suiteAnchor);
  editeur.focus(); 
}


var fenetre_impression;

function imprimer(){
  if (!fenetre_impression){
    fenetre_impression=window.open("impression.xhtml","impression");
  }
  var s="",s1="";
  var check,elem;
  if (document.getElementById("i0").checked){
    s1=document.getElementById("histo").innerHTML;
    if (isIE)
      s1=s1.replace(new RegExp("yellow","g"),"navy");
    s+="<h2>Console</h2>"+s1;
  }
  if (document.getElementById("i1").checked)
    s+="<h2>Programme</h2><pre>"+document.getElementById("script").value+"</pre>";
  if (document.getElementById("i2").checked)
    s+="<h2>Feuille de calculs</h2><table>"+cloneTbody().innerHTML+"</table>";
  if (document.getElementById("i3").checked){
    s+="<h2>Graphique</h2>"+sourceSVG("cadreSVG");
  }
  if (document.getElementById("i4").checked){
    var node= cloneTabSuite();
    if (!isIE)
      s+="<h2>Suite(s)</h2>"+node.innerHTML;
    else
      s+="<h2>Suite(s)</h2><table>"+node.innerHTML+"</table>";
  }
  if (document.getElementById("i5").checked)
    s+=document.getElementById("editeur").innerHTML;
  var form=document.getElementById('formImpr');
  var input=document.getElementById("impr");
  s=s.toLowerCase();
  s=s.replace(new RegExp("math/mathml","g"),"Math/MathML");
  s=s.replace(new RegExp("preserveAspectRatio=\"xmidymin","g"),"preserveAspectRatio=\"xMidyMin");
  s=s.replace(new RegExp("viewbox","g"),"viewBox");
  input.value=s;
  form.submit();
}
