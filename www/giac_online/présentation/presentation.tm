<TeXmacs|1.0.6.14>

<style|article>

<\body>
  <\hide-preamble>
    <assign|R|<macro|<with|math-font|Bbb|R>>>

    <assign|C|<macro|<with|math-font|Bbb|C>>>

    <assign|Z|<macro|<with|math-font|Bbb|Z>>>

    <assign|N|<macro|<with|math-font|Bbb|N>>>
  </hide-preamble>

  <\center>
    <\with|font-size|1.41>
      Un survol rapide de Xcas
    </with>
  </center>

  <vspace|0.2cm>

  <with|font-family|tt|Xcas> est un logiciel libre de calcul formel,
  �galement capable de faire de la g�om�trie dynamique (dans le plan ou
  l'espace, en mode approch� ou exact), du tableur et de la programmation
  (selon plusieurs syntaxes: maple, calculatrices TI, language de type Logo).

  Lorsqu'on lance <with|font-family|tt|Xcas>, une fen�tre s'ouvre, avec de
  haut en bas<nbsp>:

  <\itemize>
    <item>une barre de menu principale, compose de deux parties<nbsp>: sur la
    gauche fichier, �dition, configuration, aide et exemples, et sur la
    droite des commandes de <with|font-family|tt|Xcas> class�es par th�mes
    (maths, physique, g�om�trie, programmation, graphes).

    <item>un onglet de session nomm� ``Unnamed'' et sa barre de boutons
    (<framebox|?> pour ouvrir l'index, <framebox|Save> pour sauvegarder la
    session, un bouton affichant la configuration, <framebox|STOP> pour
    interrompre un calcul trop long, etc.)

    <item>un num�ro de niveau <framebox|1> suivi d'un espace rectangulaire
    sur fond blanc appel� <with|font-series|bold|ligne de commande> servant �
    faire du calcul formel, on y tape par exemple <verbatim|deriver(x^4-1)>
    puis la touche Entree pour d�river <with|mode|math|x<rsup|4>-1>.

    <item>une zone gris�e qui se remplira au fur et � mesure que l'on ajoute
    des niveaux, automatiquement ou en utilisant le menu
    <with|font-family|tt|Edit-\<gtr\>Ajouter> qui permet d'ajouter (entre
    autres) une figure de g�om�trie ou tableur.
  </itemize>

  Les commandes de <with|font-series|bold|calcul formel> se font en g�n�ral
  en-dehors d'un tableur ou d'une figure dans une ligne de commande. On peut
  utiliser les menus ou l'aide. Par exemple, on clique si n�cessaire dans une
  ligne de commande, on va dans le menu Scolaire, Seconde, on s�lectionne
  <with|font-family|tt|factoriser>, puis on tape le polyn�me � factoriser,
  disons <verbatim|x^4-1> et on valide en tapant sur
  <with|font-family|tt|Entr�e>. Le r�sultat apparait en-dessous ainsi qu'une
  nouvelle ligne de commande vide permettant de saisir une nouvelle commande.
  On peut aussi modifier la premi�re ligne de commande, et ex�cuter la
  commande modifi�e en tapant sur <with|font-family|tt|Entr�e>. Le bouton
  d'<with|font-series|bold|aide> <framebox|<with|font-family|tt|?>>
  (raccourci clavier touche de tabulation) sert � la fois de touche de
  compl�tion et d'aide, on tape le d�but d'un nom de commande, par exemple
  <with|font-family|tt|integ> puis <framebox|<with|font-family|tt|?>>, et
  l'index de l'aide s'ouvre � cet endroit. Le menu
  <with|font-family|tt|Aide-\<gtr\>Debuter en calcul formel-\<gtr\>Tutoriel>
  ouvrira dans votre navigateur un tutoriel pour vos premiers pas.

  Pour la <with|font-series|bold|g�om�trie>, on cr�e une figure (menu
  <with|font-family|tt|Edit-\<gtr\>Ajouter-\<gtr\>graphe et geo>). On peut
  ensuite travailler sur un mode proche de celui des logiciels usuels<nbsp>:
  on s�lectionne un mode (Pointeur, point, ligne, etc.) � la souris et on
  construit des objets � la souris. Il apparait alors les commandes
  correspondantes en vis-�-vis dans des petites lignes de commande. On peut
  aussi d�finir directement un nouvel objet g�om�trique par une commande en
  s'aidant au besoin du menu <with|font-family|tt|Geo> et de l'aide. Le menu
  de la figure permet de la sauvegarder, ou de faire des op�rations plus
  math�matiques, comme ajouter un param�tre r�el ou un graphe de fonction ou
  de courbe param�trique.

  De m�me, le menu <with|font-family|tt|Edit-\<gtr\>Ajouter-\<gtr\>Tableur,
  statistiques> cr�e un <with|font-series|bold|tableur>, dont l'usage
  ressemble aux tableurs usuels, avec moins d'options mais la possibilit� de
  d�finir des cellules ayant des valeurs formelles, rationnelles, ou
  d'utiliser une d�finition faite pr�c�demment dans une ligne de commande.
  Par exemple on peut d�finir une fonction <verbatim|f(x):=x^2+2x-1> dans une
  ligne de commande en-dehors du tableur, saisir <with|font-family|tt|1/2>
  dans la cellule <with|font-family|tt|A0> puis <with|font-family|tt|=f(A0)>
  dans la cellule <with|font-family|tt|B0>.

  On peut saisir des petits programmes en language Xcas, Maple ou
  calculatrices TI89/92 directement dans une ligne de commande, ou mieux dans
  un �diteur (menu <with|font-family|tt|Edit-\<gtr\>Ajouter-Programme>).

  Les lignes de commande, figures, tableurs ou programmes sont num�rot�s et
  peuvent �tre d�plac�s ou effac�es � la souris en s�lectionnant ce num�ro.
  L'ensemble de ces niveaux forment une <with|font-series|bold|session> qui
  peut �tre sauvegard�e en utilisant le menu <with|font-family|tt|Fich>.

  Pour des <with|font-series|bold|informations compl�mentaire>, il suffit de
  chercher <with|font-family|tt|xcas> sur <with|font-family|tt|google> ou
  saisir l'URL <verbatim|http://www-fourier.ujf-grenoble.fr/~parisse/>
</body>