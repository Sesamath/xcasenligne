//   Copyright (C) 2008 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


if (!isIE){
  grapheurCharge=true;
  if (isWebKit){
    setTimeout("svgGrid(-5,-5,5,5)",1000);
  }
  else
    svgGrid(-5,-5,5,5);
  document.getElementById("combo").value="";
  restaureSvg();
}

document.getElementById("fenetre").reset();

function restaureSvg(){
  var i;
  var histSVG=document.getElementById("histSVG");
  for (i=0; i<tabSVG.length ; i++) 
    if (tabSVG[i]!="")
      ajouteOptionSvg(histSVG, tabSVG[i],i);
  hist2svg();
}

//gestion de la combobox svg 
function deplieCombo(){
  if (comboDepliee){
    replieCombo();
    comboDepliee=false;
    return;
  }
  var combo=document.getElementById("combo");
  combo.blur();
  combo.style.display="none";
  var histSVG=document.getElementById("histSVG");
  if (isIE){
    histSVG.style.width="300px";
  }
  histSVG.focus();
  comboDepliee=true;
}

function replieCombo(){
  var histSVG=document.getElementById("histSVG");
  var n=histSVG.options[histSVG.selectedIndex].getAttribute("isvg");
  selectedSVG=parseInt(n);
  var combo=document.getElementById("combo");
  if (isIE)
    histSVG.style.width="20px";
  combo.style.display="block";
  setTimeout("TexteCombo()",1);
  comboDepliee=false;
}

function TexteCombo(){
  var combo=document.getElementById('combo');
  var histSVG=document.getElementById("histSVG");
  var s=histSVG.options[histSVG.selectedIndex].text;
  if (histSVG.selectedIndex>0)
    combo.value=s;
  else
    combo.value="";
  combo.focus();
}

function fenetrageSVG(){ 
  var i=vaEtVient("fenetre");
  var tab=["none","block"];
  if (isIE)
    document.getElementById("contientSVG").style.display=tab[1-i];
}

function palette(){
  var i=vaEtVient("palette");
  return;
}

// affiche le resultat en SVG
function insereSVG(id,s,estTexte,indexSVG){
  var sid;
  if (estTexte)
    sid="texte";
  else
    sid="svg";
  if (indexSVG>-1){
    s="<g onclick='selectSVG(this.id,true)' onmouseover=\"selectSVG(this.id,false);changeEpais(this,'0.08');\" onmouseout=\"selectSVG('-1',false);changeEpais(this,'0.04');\" id='"+sid+indexSVG+"'>"+s+"</g>";
    selectedSVG=0;
  }
  insere(id,s,isIE);
}


function couleurSVG(i){
  if (selectedSVG==-1)
    return;
  changeCouleur(i,selectedSVG);
  tabCSVG[selectedSVG]=i;
  ecrireCookie("csvg",tabCSVG.join("`"));
}

function supprimeSVG(){
  var histSVG = document.getElementById('histSVG');
  //var texte=histSVG.options[histSVG.selectedIndex].text;
  var texte=tabSVG[selectedSVG];
  if (texte=="")
    return;
  if (!confirm("effacer "+texte+" ?"))
    return;
  var n=selectedSVG;
  tabSVG[n]="";
  ecrireCookie("svg",tabSVG.join("`"));
  supprime("svg"+n);
  supprime("texte"+n);
  // corrige bug :
  if (isIE)
    document.getElementById("contientSVG").setAttribute("display","block");
  var i;
  for (i=0; i<histSVG.options.length ;i++) {
     var n=histSVG.options[i].getAttribute("isvg");
     if (selectedSVG==parseInt(n)){
       histSVG.removeChild(histSVG.options[i]);
       break;
     }
  }
  comboSVG(histSVG);
  return;
}

function svgGrid(xmin,ymin,xmax,ymax){
  var req=reqInit();
  var fenetre="xyztrange("+xmin+","+xmax+","+ymin+","+ymax+",-5,5,"+xmin+","+xmax+","+ymin+","+ymax+",-2,2,1,0,1)";
  effaceTout("cadreSVG");
  var sr=reqSVGgrid(req,fenetre);
  insere("cadreSVG",sr,isIE);
  return;
}

function hist2svg(){
  var req=reqInit();
  var s;
  for (var i=0 ; i<tabSVG.length ; i++){
    if (tabSVG[i]!=""){
      s=reqSVG(req,tabSVG[i]);
      var t=s.split(new RegExp("`","g"));
      insereSVG("figures",t[0],false,i);
      insereSVG("legende",t[1],true,i);
      if (tabCSVG[i]>=0)
	changeCouleur(tabCSVG[i],i);
    }
  }
}

function arrondit(nombre){
 // on arrondit delta au multiple de 10 superieur
  if (nombre==0)
    return nombre;
  var moins=false;
  if (nombre<0)
    moins=true;
  if (moins)
    nombre=nombre*(-1);
  var n=Math.log(Math.abs(nombre))/Math.LN10;
  var m=Math.floor(n);
  //if (m<n)
    //m++;
  m=Math.pow(10,m);
  var i;
  for (i=0; m*(1+i/2)<nombre ; i++);
  i=m*(1+i/2);
  if (moins)
    return i*(-1);
  else
    return i;
}

function estMultipleDe5(nombre){
  var n=Math.abs(Math.log(Math.abs(nombre))/Math.log(5));
  var m=Math.floor(n);
  return m==n;
}

var newGrid=false;

function newSvgGrid(){
  if (!newGrid){
    fenetrageSVG(0);
    return;
  }
  newGrid=false;
  var xm=arrondit(parseFloat(document.getElementById("xm").value));
  var ym=arrondit(parseFloat(document.getElementById("ym").value));
  var xM=arrondit(parseFloat(document.getElementById("xMax").value));
  var yM=arrondit(parseFloat(document.getElementById("yMax").value));
  var delta=(yM-ym-xM+xm)/2;
  if (delta!=0){   
    if (delta<0){ 
      if (estMultipleDe5(delta)){
	yM-=delta;
	ym+=delta;
      }
      else
	ym+=2*delta;
    }
    else {
      if (estMultipleDe5(delta)){
	xM+=delta;
	xm-=delta;
      }
      else
	xm-=2*delta;
    }
  }
  fenetrageSVG(0);
  svgGrid(xm,ym,xM,yM);
  hist2svg();
  return;
}

// mise a jour de l'input "combo"
function comboSVG(hist){
  var combo=document.getElementById("combo");
  combo.value="";
  hist.blur(); 
  return;
}


function ajouteOptionSvg(hist,s,n){
 var option=new Option(s,s,true,true);
 if (isIE)
   hist.add(option,1);
 else
   hist.add(option,hist.options[1]);
 hist.options[1].setAttribute("isvg",n);
 comboSVG(hist);
}
