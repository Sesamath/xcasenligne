var suitesCookie="";

restaureSuites();

function changeNom(sId,nom){
  var span=document.getElementById(sId);
  span.firstChild.nodeValue=nom;
}

function changeNomSuite(input,i){
  var nom=input.value;
  changeNom("uInit"+i,nom);
  changeNom("uRec"+i,nom);
  changeNom("uN"+i,nom);
}

function changeModeSuite(select,i){
  var div=[document.getElementById("saisieRec"+i),document.getElementById("saisieExplicite"+i)];
  var index=select.selectedIndex;
  div[index].style.display="block";
  div[1-index].style.display="none";
  return;
}

// Generateur de suites --------------
function suiteGen(){
  var suiteCookie="";
  suitesCookie="";
  var i;
  //test de définition suite
  var def=[true,true,true,true];
  var virgule="",virguleRec="",virguleEx="",nomSortie="(",nomRec="(",u0Rec="(",fRec="(",nomEx="(",fEx="(";
  var tabNom=[];
  var bRec;
  var elem, s;
  for (i=0; i<4; i++){
    suiteCookie="";
    elem=document.getElementById("bRec"+i);
    bRec=(elem.selectedIndex==0);
    if (bRec){
      suiteCookie="1`";
      elem=document.getElementById("prem"+i);
    }
    else {
      suiteCookie="0`";
      elem=document.getElementById("expli"+i);
    }
    def[i]=(elem.value!="");
    if (def[i]) {
      // nom des suites
      elem=document.getElementById("nomSuite"+i);
      suiteCookie+=elem.value+"`";
      nomSortie+=virgule+elem.value+"_n";
      if (virgule=="")
	virgule=",";  
      tabNom.push(elem.value);
      if (bRec){
	nomRec+=virguleRec+elem.value+"_n";
	// premier terme
	elem=document.getElementById("prem"+i);
	suiteCookie+=elem.value+"`";
	u0Rec+=virguleRec+elem.value;
	// relation de recurrence
	elem=document.getElementById("Rec"+i);
	suiteCookie+=elem.value;
	fRec+=virguleRec+elem.value;
	if (virguleRec=="")
	  virguleRec=",";
      } else {
	nomEx+=virguleEx+elem.value+"_n";
	elem=document.getElementById("expli"+i);
	suiteCookie+=elem.value;
	fEx+=virguleEx+elem.value;
	if (virguleEx=="")
	  virguleEx=",";
      }
      if (suitesCookie!="")
	suitesCookie+="`";
      suitesCookie+=suiteCookie;
    }
  }
  ecrireCookie("suitesCookie",suitesCookie);
  var nomRec2="(", u0Rec2="(", fRec2="(";
  virgule="";
  for (i=0; i<tabNom.length;i++){
    nomRec2+=virgule+"SIGMA"+tabNom[i];
    u0Rec2+=virgule+"0";
    fRec2+=virgule+"SIGMA"+tabNom[i]+"+"+tabNom[i]+"_n";
    if (virgule=="")
      virgule=","
  }
  nomRec2+=")";
  u0Rec2+=")";
  fRec2+=")";
  nomSortie+=")";
  nomRec+=",n)";
  u0Rec+=",0)";
  fRec+=",n+1)";
  nomEx+=")";
  fEx+=")";
  var n=parseInt(document.getElementById("nDer").value);
  //var p=n+1;
  var req=reqInit();
  var s=nomSortie+"`"+nomRec+"`"+u0Rec+"`"+fRec+"`"+nomRec2+"`"+u0Rec2+"`"+fRec2+"`"+nomEx+"`"+fEx+"`"+n;
  s=reqSuite(req,s); 
  //affichage des suites
  var t=s.split(new RegExp("`","g"));
  var sortie=document.getElementById("sortieSuite");
  if (sortie.firstChild)
    sortie.removeChild(sortie.firstChild);
  //construire le tableau
  s="<table style='width:100%'><tbody>";
  var regListe=new RegExp(",","g");
  var  liste;
  var j;
  for (var i=0; i<=n; i++){
    if (i%2==1)
      s+="<tr style='background-color:silver'>";
    else
      s+="<tr>";
    liste=t[i].split(regListe);
    for (j=0 ; j<liste.length ; j++)
	s+="<td>"+tabNom[j]+"<sub>"+i+"</sub>="+liste[j]+"</td>";
    s+="</tr>";
  }
  s+="</tbody></table>";
  sortie.innerHTML=s;
}

function restaureSuitesSansCookie(s){
  var tab=s.split(new RegExp("`","g"));
  var i=0;
  var bRec;
  for (j=0; j<tab.length; j++){    
    elem=document.getElementById("bRec"+i);
   //alert(i+"   "+tab[j]);
    if (tab[j]==0){
      elem.selectedIndex=1;
      changeModeSuite(elem,i);
    }
    bRec=tab[j];
    j++;
    // nom des suites
    elem=document.getElementById("nomSuite"+i);
    elem.value=tab[j];
    j++;
    if (bRec==1){
      // premier terme
      elem=document.getElementById("prem"+i);
      elem.value=tab[j];
      j++;
      // relation de recurrence
      elem=document.getElementById("Rec"+i);
      elem.value=tab[j];
    } else {
      elem=document.getElementById("expli"+i);
      elem.value=tab[j];
    }
    i++;
  }
}

function restaureSuites(){
  if (!restaureSession)
    return;
  var s=lireCookie("suitesCookie");
  if (s=="")
    return;
  restaureSuitesSansCookie(s);
}
