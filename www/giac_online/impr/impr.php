<?php
header("Content-Type:application/xhtml+xml ");
echo "<?xml version='1.0' encoding='UTF-16'?>"; 
?>

<!DOCTYPE html PUBLIC
    "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN"
    "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:svg="http://www.w3.org/2000/svg"
  xml:lang="en">

<head>
  <title>Impression XCAS</title>
<link rel="stylesheet" href="impr.css" type="text/css" />
<?php  
      //for inline Adobe SVG:
  echo "<object id='AdobeSVG' classid='clsid:78156a80-c6a1-4bbf-8e6a-3cd390eeb4e2'></object><?import namespace='svg' implementation='#AdobeSVG'?>";
?>
</head>
  
<body>
<?php 
  $impr=$_POST["impr"];
  $impr=stripslashes($impr);
  print $impr."\n";
?>

</body>
</html>