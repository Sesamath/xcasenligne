<?php
header("Content-Type:application/xhtml+xml ");
echo "<?xml version='1.0' encoding='iso-8859-1'?>\n";

?>
<!DOCTYPE html PUBLIC
    "-//W3C//DTD XHTML 1.1 plus MathML 2.0 plus SVG 1.1//EN"
    "http://www.w3.org/2002/04/xhtml-math-svg/xhtml-math-svg.dtd">



<html xmlns="http://www.w3.org/1999/xhtml" 
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:svg="http://www.w3.org/2000/svg"
  xml:lang="en">

<head>
  <title>Xcas en ligne</title>

  <script   type="text/javascript"  src="demoGiacPhp.js"></script>
  <link rel="stylesheet" href="demoGiacPhp.css" type="text/css" />

  <!--[if !IE]> <-->   
      <link rel="stylesheet" href="correctifFF.css" type="text/css" />
  <!--> <![endif]-->
   
</head>
  
<body onload="lireCookies();prog(); recadre();">

<div class="haut">
  <p class="doc">
  <a href="javascript:escamoteAide()">
 <img  onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" title="afficher/masquer info" src="img/info.gif"/>&#160;
  </a>
<a href="javascript:afficheTheme(6)">
 <img  onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" title="Assistant" src="img/wizard.gif"/> 
  </a>
 <a href="javascript:afficheTheme(-1)">
  <img  onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" title="Aide" src="img/bouee.gif"/> 
  </a>
 <a href="javascript:afficheTheme(7)">
 <img  onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" title="Doc officielle" src="img/docOff.gif"/> 
  </a>
 
  </p>
    <p id="choix" class="choix">
      <a id="t0" href="javascript:afficheCadre(0)" title="console" style="color:orange">
         <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" src="img/console.gif" />
      </a>
      <a id="t1" href="javascript:afficheCadre(1)"  title="programmation">
         <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" src="img/progr.gif" /> 
      </a>
      <a id="t2" href="javascript:afficheCadre(2)"  title="tableur">
        <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" src="img/tableur.gif" />
      </a>
      <a id="t3" href="javascript:afficheCadre(3)"  title="graphiques">
        <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" src="img/graph.gif" />
      </a>
      <a id="t4" href="javascript:afficheCadre(4)"  title="suites">
        <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" src="img/suite.gif" />
      </a>
      <a id="t5" href="javascript:afficheCadre(5)"  title="Edition scientifique">
        <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" src="img/editeur.gif" />
      </a>
      <a id="t5" href="javascript:afficheCadre(6)" title="imprimer">
        <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" src="img/impr.gif" />
      </a>
      <a id="t5" href="javascript:afficheCadre(7)" title="Sauvegarder/Restaurer">
        <img onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" style="top:2px" src="img/floppy.gif" />
      </a>
      <a id="t5" href="Facilimaths/accueil/page.xhtml" target="_blank" title="Exerciseur (dans un nouvel onglet)">
        <img onmouseover="opacite(this,false)" 
             onmouseout="opacite(this,true)"
             onclick="alert('Pour des raisons techniques, les exercices seront dans un nouvel onglet')" 
             src="img/exerciseur.gif" />
      </a>
    </p>
    <p class="syntaxe">
	Syntaxe
        <select id="syntaxe">
          <option value="0">Xcas</option>
          <option value="1">Maple</option>
          <option value="2">Mupad</option>
          <option value="3">TI89</option>
        </select>
    </p> 
</div>

<div id="milieu" class="milieu">

<div  class="aide" id="aide">
  <object data="aide_off_prog.html" 
  id="tutoProg" type="text/html" style="display:none">
  </object>

  <object data="aide_off_calcul.html"  id="tutoCalc" type="text/html" style="display:none">
  </object>

  <object data="aide_impr.html" id="aide_impr" type="text/html" style="display:none">
  </object>

  <object data="aide_editeur.html" id="aide_editeur" type="text/html" style="display:none">
  </object>

 <object data="aide_suite.html" id="aide_suite" type="text/html" style="display:none">
  </object>
  
  <object data="aide_grapheur.html" id="aide_grapheur" type="text/html" style="display:none">
  </object>

  <object data="aide_tableur.html" id="aide_tableur" type="text/html" style="display:none">
  </object>

  <object data="aide_prog.html" id="aide_prog" type="text/html" style="display:none">
  </object>

 <object data="aide_console.html" id="aide_console" type="text/html" style="display:none">
  </object>

 <object data="aide_fr.html" id="licence" type="text/html" style="display:block">
  </object>

  <div id="assistant"  style="display:none">
   <h2>Assistant d'Xcas en ligne</h2>
    <p><i>Cliquez sur une des entr&#233;es de l'assistant ci-dessous pour construire une instruction Xcas ou bien utilisez <a href="javascript:afficheTheme(8)">
	la documentation compl&#232;te
      </a> pour avoir plus de commandes</i>.
    </p>
    <h3 onclick="vaEtVient('numerique')">Calcul num&#233;rique</h3>
    <p id="numerique"><div></div>
      <span onclick="assistant(this,'','DIGITS:=','chiffres')">
      Nombre de chiffres</span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','evalf','expression')">
	Valeur approch&#233;e
      </span><fieldset></fieldset>
    </p>
    <h3 onclick="vaEtVient('arithmetique')">Arithm&#233;tique</h3>
    <p id="arithmetique" ><div></div>
      <span onclick="assistant(this,'','iquo','diviser','par')">
	Quotient division euclidienne
      </span><fieldset></fieldset><br />
      <span onclick="assistant(this,'','irem','diviser','par')">
	Reste division euclidienne
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','is_prime','nombre')">
	Savoir si un nombre est premier
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','iegcd','a','b')">
	Identit&#233; de B&#233;zout
      </span><fieldset></fieldset><br/>    
    </p>
    <h3 onclick="vaEtVient('algebre')">Alg&#232;bre</h3>
    <p id="algebre"><div></div>
      <span onclick="assistant(this,'',':=','variable','valeur')">
      	Initialiser une variable
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','purge','variable')">
      	Purger une variable
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','normal','expression')">
      	D&#233;velopper et r&#233;duire
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','factor','expression')">
      	Factoriser
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','simplify','expression')">
      	Simplifier
      </span><fieldset></fieldset><br/>
       <span onclick="assistant(this,'','canonical_form','expression')">
      	Forme canonique
      </span><fieldset></fieldset><br/>
       <span onclick="assistant(this,'','solve','equation','inconnue')">
	R&#233;soudre une &#233;quation
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'S&#233;parer les inconnues par une virgule','linsolve','Inconnues','Equation 1','Equation 2','Equation 3','Equation 4')">
	 R&#233;soudre un syst&#232;me lin&#233;aire
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','tlin','expression')">
	 Lin&#233;ariser
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'Taper chaque ligne de la matrice en separant les elements par des virgules','matrice','ligne1','ligne2','ligne3','ligne4')">
	 Entrer une matrice
      </span><fieldset></fieldset>
    </p>
    <h3 onclick="vaEtVient('analyse')">Analyse</h3>
    <p id="analyse"><div></div>
      <span onclick="assistant(this,'',':=','Nom (f(x), g(x))','expression')">
	D&#233;finir une fonction	
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','plotfunc','f(x)','xMin','xMax')"> 
	Repr&#233;senter une fonction	
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','diff','expression','variable')"> 
	D&#233;river  	
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','integrate','expression','variable')">
  	Primitiver
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','integrate','expression','variable','borne basse','borne haute')">
	Int&#233;grer
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'(taper inf pour infini)','limite','expression','variable','tend vers')">
	Limites
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','sum','terme','variable','borne basse','borne haute')">
	Somme de termes
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','series','f(x)','a','ordre')">
	D&#233;veloppement limit&#233; de f(x) au voisinage de a
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'','desolve','Equation','Inconnue','Condition initiale 1','Condition initiale 2')">
	Equation differentielle (coeff const)
      </span><fieldset></fieldset>
      </p>
     <h3 onclick="vaEtVient('probas')">Tirages al&#233;atoires</h3>
      <p id="probas"><div></div>
        <span onclick="assistant(this,'','rand','n')">
	D&#8217;un entier entre 0 et n-1
      </span><fieldset></fieldset><br/>
        <span onclick="assistant(this,'Donner les bornes a et b','hasard','a','b')">
	D&#8217;un r&#233;el issu d&#8217;une loi uniforme
      </span><fieldset></fieldset><br/>
      <span onclick="assistant(this,'Donner la moyenne et l&#8217;&#233;cart-type','randnorm','m','&#963;')">
	Selon une loi normale
      </span><fieldset></fieldset><br/>
      </p>
  </div>
</div>

<div class="accueil">
<h1>XCAS en ligne</h1>
  <p><i>Ce site vous permet d'utiliser partiellement le logiciel de math&#233;matiques XCAS dans votre navigateur, sans l'installer sur votre ordinateur. Les calculs seront faits par le serveur.</i>
  </p>
  <p>Ces programmes sont sous <b>licence GPL</b></p>
  <hr/>
  <p><b>Modules disponibles</b> (barre d'outils en haut) :</p>
  <p>
  <img src="img/console.gif" />  &#160; Console XCAS  &#160; &#160;
  <img src="img/progr.gif" />  &#160; Programmation  &#160; &#160;
  <img src="img/tableur.gif" /> &#160; Mini tableur
  </p><p>
  <img src="img/graph.gif" /> &#160; Graphiques  &#160; &#160; 
  <img src="img/suite.gif" /> &#160; Suites  &#160; &#160;
  <img src="img/editeur.gif" /> &#160; Editeur maths  &#160; &#160;
   <img src="img/exerciseur.gif" /> &#160; Exerciseur
  </p>
  <hr/>
  <p>
  <img src="img/info.gif"/> &#160; Afficher ou masquer l'info  &#160; &#160;
  <img src="img/wizard.gif"/> &#160; Assistant
  <br />
  <img src="img/bouee.gif"/> &#160; Aide contextuelle  &#160; &#160;
  <img src="img/docOff.gif"/> &#160; doc officielle d'Xcas 
  </p>
  <p>
  <b>Nouveau : </b>
  <a href="../fabrique/" style="color:blue;text-decoration:underline">la fabrique</a>
  permet de construire des exercices en ligne utilisant XCAS.
  </p> 
  <hr />
  <p>
    Pour b&#233;n&#233;ficier des fonctionnalit&#233;s compl&#232;tes d'XCAS, t&#233;l&#233;chargez
				 celui-ci (lien en bas).
  </p>
</div>

<div class="tableur" id="tableur" >
  <div class="entree">
    <form action="javascript:fill_cell(false)" style="padding:0px; margin:0px;">
      &#160;<b id="tab_cell">A0</b> : 
      <input onkeyup="modeSaisie(event,this)" onkeydown="ctrl(event,this)" style="width:80%" id="tab_entry" />
      &#160;
    </form>
  </div>
  <div class="outils">
    <select id="typeTab" onchange="fill_cell(true)">
      <option>=</option>
      <option>&#8776;</option>
    </select>
    <a href="javascript:fill_bottom()" title="Remplir en bas">
      <img src="img/bas.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
    </a>
    <a href="javascript:fill_right()" title="Remplir &#224; droite">
      <img src="img/droit.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
    </a>
    <a href="javascript:annule()" title="Annuler" style="color:red">
      <img src="img/annul.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
    </a>
    <a href="javascript:vide()" title="Vide la feuille" style="color:red">
      <img src="img/eraser.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
    </a>
    <a href="javascript:ajouteLigne()" title="Ajouter une ligne" style="color:red">
      <img src="img/nouvelleLigne.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
    </a>
  </div>

 <?php
  $nb_line=20;
  $nb_col=7;
  print "<script>n_lign=$nb_line; n_col=$nb_col;nb_lign=0; nb_col=0;</script>\n";
  ?>

  <div class="conteneur" id="conteneur">
  <div class="tableau">
  <table id="table">
  <tbody>
  <tr>
  <td class="col1">
  <a href="javascript:fill_cell(true)" title="Recalculer">
      <img src="img/initial.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
  </a>
  </td>
  <?php
  $ord = ord("A");
  for ($j=0;$j<$nb_col;$j++) {
    print "<td bgcolor=\"lightGrey\" align=\"center\" >";
    print chr($ord+$j);
    print "</td>\n";
  }
  print "</tr>\n";
  for ($i=0; $i<$nb_line ; $i++) {
    print "<tr><td class='col1'>".$i."</td>\n";
    for ($j=0 ; $j<$nb_col ; $j++) {
      $ord = ord("A");
      $col =chr($ord+$j);
      print "<td  id=\"".$col.$i."\"  formule='' result='' onclick=\"cell('".$col.$i."')\" ></td>";
    }
    print "</tr>\n";
  }
 ?>
  </tbody>
  </table>

  </div>
  </div>
  </div>




<div  id="progr" class="programme">
  <textarea  id="script" onclick="javascript:nouveauScript()"></textarea> 
  <button style="position:absolute;right:20px;top:5px;background-color:yellow;cursor:pointer;" onclick="indente()">
     Indentation automatique
  </button>
</div>



<div class="console" id="console" onclick="activeInput=document.getElementById('in');activeInput.focus()">
<p>
<b>Xcas en ligne</b>.
Tapez une instruction dans cette console (assistant avec la bou&#233;e).
</p>
<div id="histo">
</div>
<form action="javascript:giac(document.getElementById('in'))">
      <input type='text' id='in' size='50' onkeydown="javascript:fleche(event);"/>    
</form>
<span id="fin">
</span>
</div>



<div class="svg" id="svg">
<object  data="figure.svg" type="image/svg+xml" id="contientSVG">
</object>


<div class="histoSVG" id="histoSVG">
 <!--[if !IE]> <-->    
<select id="histSVG"  size="1" onchange="replieCombo()">
 <!--> <![endif]-->
<!--[if IE]>
<select id="histSVG"  size="1" onchange="replieCombo()" onmousedown="deplieCombo()">
<![endif]-->
    <option selected="true">Nouvelle figure</option>
 </select>
<input type="text" id="combo"  class="combo" onkeydown="if(event.keyCode==13) {giac(this);}" />
<select style="position:absolute;left:87%;top:35px;display:none" id="palette" size="8" onchange="couleurSVG(this.selectedIndex);">
<option style="background-color:black;color:white">
noir
</option>
<option style="background-color:lightgrey">
gris
</option>
<option style="background-color:red">
rouge
</option>
<option style="background-color:blue">
bleu
</option>
<option style="background-color:yellow">
jaune
</option>
<option style="background-color:green">
vert
</option>
<option style="background-color:orange">
orange
</option>
<option style="background-color:violet">
violet
</option>
</select>

<a href="javascript:supprimeSVG()" title="Supprimer" style="position:absolute; right:63px;width:25px;color:red">
<img src="img/eraser.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
</a>
<a href="javascript:palette()" title="Couleur" style="position:absolute; right:30px;width:30px;color:red">
<img src="img/palette.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
</a>
<a href="javascript:fenetrageSVG()" title="Fenetre" style="position:absolute; right:2px;width:25px;">
<img src="img/magnificator.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
</a>


</div>

<form id="fenetre">
<table>
<tr>
<td>x<sub>min</sub></td><td><input id="xm" value="-5" onchange="newGrid=true" /></td>
</tr><tr>
<td>y<sub>min</sub></td><td><input id="ym" value="-5" onchange="newGrid=true" /></td>
</tr><tr>
<td>x<sub>max</sub></td><td><input id="xMax" value="5" onchange="newGrid=true" /></td>
</tr><tr>
<td>y<sub>max</sub></td><td><input id="yMax" value="5" onchange="newGrid=true" /></td>
</tr>
<tr>
<td colspan="2">
<input type="button" value="ok" onclick="newSvgGrid()"/>
<input type="reset" onclick="newGrid=true" value="d&#233;faut" />
<input type="button" value="fermer"  onclick="fenetrageSVG(0)" />
</td>
</tr>
</table>
</form>
</div>


<div class="svg" id="suite">
<div class="saisieSuite">
<b>Suites</b>
<i>(taper u_n pour u<sub>n</sub>)</i>.
Calcul jusqu'&#224; l'indice 

<input id="nDer" value="10" size="2"/>
<input type="button" value="Calculer" onclick="suiteGen()" style="background-color:yellow" />
<table style="width:100%"  id="saisieSuite">
<?php
$nom = array(0 => "u", 1 =>"v", 2 =>"w", 3=>"x");
  for ($i=0; $i<4 ; $i++) {
    print"<tr><td>\n";
    print "<input  size='1' onkeydown='if (event.keyCode==ENTREE) changeNomSuite(this,".$i.");' value='".$nom[$i]."' id='nomSuite".$i."'/>\n";
    //print "</td><td>\n";
    print "<select id='bRec".$i."' style='width:100px' onchange='changeModeSuite(this,".$i.");'>\n";
    print "<option  value='0'>R&#233;currente</option>\n";
    print "<option value='1'>Explicite</option>\n";
    print "</select>\n";
    print "</td><td>\n";
    print "<div id='saisieRec".$i."'><span id='uInit".$i."'>".$nom[$i]."</span><sub>0</sub>=<input id='prem".$i."' size='3'/> <span id='uRec".$i."'>".$nom[$i]."</span><sub>n+1</sub>=<input id='Rec".$i."' size='22' />&#160;&#160;</div>\n";
    print  "<div class='saisieExplicite' id='saisieExplicite".$i."'><span id='uN".$i."'>".$nom[$i]."</span><sub>n</sub>=<input id='expli".$i."' size='25' />&#160;&#160;</div>\n";
    print "</td></tr>\n";
  }
?>
</table>
</div>
<div  id="sortieSuite">
</div>
</div>

<div class="svg" id="texte">
<div class="histoSVG">
<table>
<tr>
<td>
<select id="formatBlock" style="width:125px;" onchange="appliqueStyle(this.options[this.selectedIndex].value)">
    <option value="">Style :</option>
    <option value="formatBlock;p">Paragraphe</option>
    <option value="formatBlock;h1">Titre</option>
    <option value="formatBlock;h2">Sous-titre</option>
    <option value="formatBlock;h3">Sous-sous-titre</option>
    <option value="insertUnorderedList">Liste puces</option>
    <option value="insertOrderedList">Liste num</option>
</select>
</td><td>
<button onclick="doFormat('bold');"><strong>B</strong></button>
</td><td>
<button onclick="doFormat('italic');"><em>I</em></button>
</td><td>
<button onclick="doFormat('underline');"><span style="text-decoration:underline">U</span></button>
</td><td>
<button onclick="doFormat('Paste');"><span style="text-decoration:underline">P</span></button>

</td><td>
<img src="img/align-left.gif"  onclick="doFormatFocus('justifyLeft');" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)"/>
</td><td>
<img src="img/align-center.gif" onclick="doFormatFocus('justifyCenter');" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)"/>
</td><td>
<img src="img/align-right.gif" onclick="doFormatFocus('justifyright');" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)"/>
</td><td>
<button onclick="insereCAS(true)">math</button> 
</td>
<td>
<button onclick="dispMML()">&#8592;&#160;&#8594;</button> 
</td>
</tr>
</table>
</div>
<p class="MenuBasEditeur">
<b>Coller</b> : 
<button onclick="cloneConsole()">Console</button> 
<button onclick="cloneProgramme()">Programme</button> 
<button onclick="cloneTable()">Tableau</button>
<button onclick="cloneGraphique()">Graphique</button>
<button onclick="cloneSuite()">Suites</button> 
</p>
</div>

<div class="svg" id="imprime" style="padding:10px;">
   <h2 style="text-align:center">Pr&#233;paration d'un document pour l'impression</h2>
   <p>
       <i>Le document sera affich&#233; dans un ouvel onglet ou une nouvelle fen&#234;tre.
       Vous pourrez ensuite l'imprimer ou l'enregistrer avec votre navigateur.
       </i>
   </p>
    <form>
      <input type="checkbox" id="i0"/>Console<br/>
      <input type="checkbox" id="i1"/>Programme<br/>
      <input type="checkbox" id="i2"/>Tableur<br/>
      <input type="checkbox" id="i3"/>Graphique<br/>
      <input type="checkbox" id="i4"/>Suites<br/>
      <input type="checkbox" id="i5" checked="true" />Editeur<br/>
      <input type="button" value="ok" onclick="imprimer()" />
    </form>
    <form id="formImpr" method="post" action="impr.php" target="impression">
      <input id="impr" name="impr" type="hidden" />
    </form>
    <hr />
    <p>
     <b>Attention !</b>
     XCAS en ligne ne peut relire les documents enregistr&#233;s par ce proc&#233;d&#233;.<br />
     Cliquez sur la disquette pour sauvegarder votre travail.
   </p>
</div>
<div class="svg" id="sauve" style="padding:10px;">
   <h2 style="text-align:center">Sauvegarde et restauration</h2>
    <p>
       <i>Vous pouvez enregistrer votre travail sur votre propre disque 
          ou bien ouvrir un travail pr&#233;c&#233;demment enregistr&#233;.<br />
	  (Xcas en ligne poss&#232;de aussi une sauvegarde d'urgence par cookies,
          mais elle est limit&#233;e).<hr />
       </i>
    </p>

   <!-- dialogue Enregistrer Ouvrir -->
   <form name="formOuvrir"  enctype="multipart/form-data" method="post" target="iframeOuvrir">
     <input type="button" value="Sauvegarder" onclick="vaEtVient('spanSauver','spanRestaurer')" />
     <input type="button" value="Restaurer" onclick="vaEtVient('spanRestaurer','spanSauver')" />
     <input type="hidden" name="texte" />
     <span id='spanRestaurer' style="display:none">
      <br/>
      Choisissez le fichier de restauration  sur votre disque,
      puis cliquez sur OK :
      <br />
      <!-- utile dans IE pour effacer ensuite le nom du fichier -->
      <input type='file' name='fichier' id='fichier'/>
      <input type="button" value="OK" onclick="ouvrir(this.form,'script')" />
     </span>
     <span id='spanSauver' style="display:none">
       <br/>S&#233;lectionner les modules devant &#234;tre enregistr&#233;s:<br />
       <input type="checkbox" id="s0"/>Console<br/>
       <input type="checkbox"  checked="true"  id="s1"/>Programme<br/>
       <input type="checkbox" id="s2"/>Tableur<br/>
       <input type="checkbox" id="s3"/>Graphique<br/>
       <input type="checkbox" id="s4"/>Suites<br/>
       <input type="checkbox" id="s5"/>Editeur<br/><br />
       <input type="button" value="Enregistrer" onclick="enregistrer(this.form,'script')" />
     </span>
   </form>
   <!-- fin du dialogue --> 
</div>

</div>

<!-- A part car firefox n aime pas contenteditable dans les div emboitees -->
 <!--[if !IE]> <-->    
<div id="editeur" contenteditable="false" onclick="testeLesStyles();"  onkeyup="detecteCtrl(event)">
?
</div>
 <!--> <![endif]-->
<!--[if IE]>
<div id="editeur" contenteditable="true" onclick="testeLesStyles()"  onkeyup="detecteCtrl(event,this)">
</div>
<![endif]-->

<!--iframe cachee pour la sauvegarde -->
<iframe id="iframeOuvrir" name="iframeOuvrir"></iframe>

<div class="bas">

<a href="http://www-fourier.ujf-grenoble.fr/~parisse/giac_fr.html"  style="margin-top:5px">
  T&#233;l&#233;charger Xcas
</a>
<table>
<tr>
<td onclick="touche('^')">
x<sup>n</sup>
</td><td  onclick="touche('sqrt(')">
<img src="img/sqrt.gif" />
</td><td  onclick="touche('exp(')">
e<sup>x</sup>
</td><td onclick="touche('arccos(')">
cos<sup>-1</sup>
</td><td onclick="touche('arcsin(')">
sin<sup>-1</sup>
</td><td  onclick="touche('arctan(')">
tan<sup>-1</sup>
</td><td onclick="touche('pi')">
&#960;
</td><td onclick="touche('inf')">
&#8734;
</td><td onclick="touche('rand(6)+1')">
<img src="img/de.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
</td><td onclick="touche(ans)">
R&#233;p
</td>
<td onclick="angle_radian(this.firstChild)" style="background-color:yellow">
Radians
</td>
<td onclick="complex_mode(this.firstChild)" style="background-color:yellow">
Dans R
</td>
<td onclick="effaceHisto(true)">
<img src="img/eraser.gif" onmouseover="opacite(this,false)" onmouseout="opacite(this,true)" />
</td>
</tr>
</table>

 
</div>
</body>
</html>