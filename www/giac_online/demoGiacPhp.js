//   Copyright (C) 2008 J-P Branchard <Jean-Pierr.Branchard@ac-grenoble.fr>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


var restaureSession;
var isIE = window.ActiveXObject;
var isOpera=(navigator.appName=="Opera");
var isFirefox=(navigator.userAgent.indexOf('Gecko')>-1 && navigator.userAgent.indexOf('KHTML')==-1);
var isWebKit=(navigator.userAgent.indexOf('AppleWebKit/') > -1 || navigator.userAgent.indexOf('KHTML')>-1);
var testIE7=new Array();
if (isIE && testIE7.every){
  isIE=false;
  isWebKit=true;
 }
var mathmlURL = "http://www.w3.org/1998/Math/MathML";
var svgURL="http://www.w3.org/2000/svg";
//var fenetres=["console","progr"];
var changeScript=true;
var htmlSymbols=["&int;","&Integral;","&Sigma;","&part;","&nbsp;",
		 "&rarr;","&VerticalBar;","&deg;","&infin;","&InvisibleTimes;",
		 "&pi;","&le;","&ge;"];
var utfSymbols=["\u222B","\u0222B","\u03A3","\u2202","\u0020",
		"\u2192","|","\u000B0","\u221E","", 
		"\u03C0","\u2264","\u2265"];
var FLECHE_HAUTE=38,FLECHE_BASSE=40, FLECHE_GAUCHE=37, FLECHE_DROITE=39, CTRL_M=77, ENTREE=13;
var inviteProg="//Tapez votre programme ici :\n";
var tabHisto=new Array();
var consoleCookie="";
var spreadCookie="";
var n_lign, n_col;
var suitesCookie="";
var aideVisible=true;
var nb_col=0, nb_lign=0; // tableur
var activeInput;
var activeAssistant;
var cadreActif=0;
var indexTabHisto=0;
var ans;
var tabSVG=new Array();
var tabCSVG=new Array();
var selectedSVG=0;
var tableurCharge=false;
var grapheurCharge=false;
var comboDepliee=false;
var editeurCharge=false;
var editeur;
var suiteCharge=false;
//var imprCharge=false;
var sauvegardeCharge=false;
// seront initialisees par le script du fichier svg embarque
function insere(){}
function supprime(){}
function effaceTout(){};
function cloneSVG(){};
function changeCouleur(){};
function sourceSVG(){};

// inclusion de programmes javascript
function include(fileName){
  if (!document.getElementsByTagName){
    alert ("Chargement des modules impossible");
    return;
  }
  var script = document.createElement("script");
  script.type = "text/javascript"; 
  script.src =fileName;
  var head=document.getElementsByTagName("head");
  if (head[0])
    head[0].appendChild(script);
  return;
}

// inclusion feuille de style
function includeStyle(fileName){
 var link= document.createElement("link");
  link.type = "text/css"; 
  link.href =fileName;
  link.rel="stylesheet";
  var head=document.getElementsByTagName("head");
  if (head[0])
    head[0].appendChild(link);
  return;
}


function vaEtVient(sId,sId1){
  var fenetre=document.getElementById(sId);
  var tab=["none","block"];
  var i;
  if (fenetre.style.display=="block")
    i=0;
  else
    i=1;
  fenetre.style.display=tab[i];
  if (sId1){
     fenetre=document.getElementById(sId1);
     fenetre.style.display="none";
  }
  return i;
}

function opacite(objet,opaque){
  var IEopacite, opacite;
  if (opaque) {
    IEopacite="100";
    opacite=1;
  } else {
    IEopacite="60";
    opacite=0.6;
  }
  if (isIE)
    objet.style.filter="alpha(opacity="+IEopacite+")";
  else
    objet.style.opacity=opacite;
}

function fleche(ev){
  if (ev.keyCode!=FLECHE_HAUTE && ev.keyCode != FLECHE_BASSE)
    return;
  var input=document.getElementById("in");
  if (ev.keyCode==FLECHE_HAUTE && indexTabHisto>0){
    indexTabHisto--;
    input.value=tabHisto[indexTabHisto];
  }
  if (ev.keyCode==FLECHE_BASSE && indexTabHisto<tabHisto.length){
    indexTabHisto++; 
    if (indexTabHisto<tabHisto.length)
      input.value=tabHisto[indexTabHisto];
    else 
      input.value="";
  }
  return;
}

function touche(instruction){
  activeInput.value+=instruction;
  activeInput.focus();
  return;
}

var inputs;

function closeNodeAssistant(node){
  if (!node.firstChild)
    return;
  legendText=document.createTextNode(node.firstChild.firstChild.nodeValue);
  node.previousSibling.appendChild(legendText);
  node.setAttribute("deploye","0");
  while (node.firstChild)
    node.removeChild(node.firstChild);
  node.style.display="none";
}

function assistant(node,explication,commande,arg1,arg2,arg3,arg4){
  var i;
  var legendText;
  if (node.style.display=="block"){
    closeNodeAssistant(node);
    return;
  }
  if (activeAssistant)
    closeNodeAssistant(activeAssistant);
  legendText=document.createTextNode(node.firstChild.nodeValue);
  node.removeChild(node.firstChild);
  node=node.nextSibling;
  activeAssistant=node;
  node.style.display="block";
  node.setAttribute("commande",commande);
  var legend=document.createElement("legend");
  legend.appendChild(legendText);
  legend.onclick=function(){assistant(this.parentNode);};
  node.appendChild(legend);
  if (explication!=""){
    var texte=document.createTextNode(explication);
    node.appendChild(texte);
    var br=document.createElement("br");
    node.appendChild(br);
  }
  inputs=new Array();
  for (i=3 ; i<arguments.length ; i++){
    texte=document.createTextNode(arguments[i]+" : ");
    node.appendChild(texte);
    inputs.push(document.createElement("input"));
    node.appendChild(inputs[inputs.length-1]);
    br=document.createElement("br");
    node.appendChild(br);
  }
  if (document.getElementById("console").style.display=="block"){
    var input2=faitInput("Ecrire et calculer");
    if (isIE)
      input2.onclick=function(){faitCommande(this.parentNode);giac(document.getElementById('in'));};
    else
      input2.setAttribute("onclick","faitCommande(this.parentNode);giac(document.getElementById('in'));");
    node.appendChild(input2);
  }
  var input=faitInput("Ecrire sans calculer");
  if (isIE)
    input.onclick=function(){faitCommande(this.parentNode);};
  else
    input.setAttribute("onclick","faitCommande(this.parentNode);");
  node.appendChild(input);
  node.scrollIntoView(true);
}

function faitInput(value){
  var input=document.createElement("input");
  input.setAttribute("type","button");
  input.setAttribute("value",value);
  return input;
}

function faitCommande(node){
  var s=node.getAttribute("commande");
  if (s==":=")
    s=inputs[0].value+s+inputs[1].value;
  else if (s=="DIGITS:=")
    s+=inputs[0].value;
  else if (s=="plotfunc")
    s+="("+inputs[0].value+",x="+inputs[1].value+".."+inputs[2].value+")";
  else if (s=="desolve"){
    s+="(["+inputs[0].value;
    if (inputs[2].value!="")
      s+=","+inputs[2].value;
    if (inputs[3].value!="")
      s+=","+inputs[3].value;
    s+="],"+inputs[1].value+")";
    s=s.replace(new RegExp("\"","g"),"\'\'");
  }
  else  if (s=="limite"){
    s+="("+inputs[0].value+","+inputs[1].value;
    var s1=inputs[2].value;
    var derCar=s1.charAt(s1.length-1);
    if (derCar=="+" || derCar=="-"){
      s1=s1.substr(0,s1.length-1);
      s+=","+s1;
      if (derCar=="+")
	s+=",1";
      else
	s+=",-1";
    } else
      s+=","+s1;
    s+=")";
  }
  else if (s=="linsolve"){
    s+="([";
    for (i=1 ; i<inputs.length ; i++){
      if (inputs[i].value!=""){
	if (i>1)
	  s+=",";
	s+=inputs[i].value;
      }
    }
    s+="],["+inputs[0].value+"])";
  }
  else if (s=="matrice"){
    s="[";
    for (i=0 ; i<inputs.length ; i++){
      if (inputs[i].value!=""){
	if (i>0)
	  s+=",";
	s+="["+inputs[i].value+"]";
      }
    }
    s+="]";
  }
  else {
    s+="(";
    for (i=0 ; i<inputs.length ; i++){
      if (i>0)
	s+=",";
	s+=inputs[i].value;
    }
    s+=")";
  }
  touche(s);
}


// pour Ie
function recadre(){
  //document.getElementById("console").style.display="block";
  if (isWebKit)
    includeStyle("mathml.css");
  var elem;
  elem=document.getElementById("histSVG");
  if (isOpera){
    elem.onclick="deplieCombo()";
    return;
  }
  //activeInput.focus();
  // sort si pas internet explorer
  if (!isIE){
    elem.onfocus="deplieCombo()";
    return;
  }
  //elem.onmousedown="alert(0);deplieCombo()";
  elem=document.getElementById("milieu");
  elem.style.height=document.body.clientHeight-65;
  elem=document.getElementById("conteneur");
  elem.style.height=document.body.clientHeight-100;
  elem=document.getElementById("sortieSuite");
  elem.style.height=document.body.clientHeight-235;
  elem=document.getElementById("editeur");
  elem.style.height=document.body.clientHeight-132;
  elem.style.position="absolute";
  //81;
  return;
}

if (isIE)
  window.onresize=recadre;

function ie(){
document.getElementById("contenu").style.right="10px";
}

function escamoteAide(){
  aideVisible=!aideVisible;
  var largeurAide, largeur;
  var choix=document.getElementById("choix")
  if (aideVisible){
    largeurAide="36%";
    largeur="64%";
    choix.style.left="36%";
  } else {
    largeurAide="0";
    largeur="100%";
    choix.style.left="25%";
  }
  document.getElementById("aide").style.width=largeurAide;
  var cadres=["console","progr","tableur","svg","suite","texte","editeur"];
  var i,cadre;
  for (i=0 ; i<cadres.length ; i++){
    cadre=document.getElementById(cadres[i]);
    cadre.style.left=largeurAide;
    cadre.style.width=largeur;
  } 
}


function aide(n){
  if (n<0)
    n=cadreActif;
  var t=["aide_console","aide_prog","aide_tableur","aide_grapheur","aide_suite","aide_editeur","assistant","tutoCalc","licence"];
  if (n==7){
    var tc=document.getElementById('tutoCalc');
    if (tc.style.display=="block"){
      var tc1=tc.cloneNode(false);
      tc.parentNode.appendChild(tc1);
      tc.parentNode.removeChild(tc);
    }
  }
  var i;
  var id;
  for (i=0; i<t.length ; i++){
    id=document.getElementById(t[i]);
    if (i==n)
      id.style.display="block";
    else
      id.style.display="none";
  }
}
	
function afficheTheme(n){
  if (n==6){
    if (!activeInput)
      afficheCadre(0);
  }
   if (!aideVisible)
      escamoteAide();
  aide(n);
}

function afficheCadre(choix){
  var cadre=["console","progr","tableur","svg","suite","texte","imprime","sauve"];
  var cadreInput=["in","script","tab_entry","combo","uInit0","editeur","imprime","sauve"];
  if (choix<6){
    afficheTheme(choix);
    cadreActif=choix;
  }
  if (!isIE)
    document.getElementById("editeur").setAttribute("contenteditable","false");
  //chargement des fichiers js
  switch (choix){
  case 2 :
    if (!tableurCharge){
      include("tableur.js");
      tableurCharge=true;
    }
    break;
  case 3 :
    if (!grapheurCharge)
      include("grapheur.js");
    var objet=document.getElementById("contientSVG");
    objet.scrollTop=100;
    break;
  case 4 :
    if (!suiteCharge){
      include('suite.js');
      suiteCharge=true;
      var i;
      var select;
      for (i=0; i<4; i++){
	select=document.getElementById("bRec"+i);
	select.selectedIndex=0;
      }
    }
    break;
  case 5 :
    if (!editeurCharge){
      include("editeur.js");
      editeurCharge=true;
      editeur=document.getElementById("editeur");
    }
    if (!isIE)
      editeur.setAttribute("contenteditable","true");
    //editeur.blur();editeur.focus();
    break;
  case 6 :
    if (!editeurCharge){
      include("editeur.js");
      editeurCharge=true;
      editeur=document.getElementById("editeur");
      editeur.focus();
    }
    break;
 case 7 :
    if (!sauvegardeCharge){
      include("sauvegarde.js");
      sauvegardeCharge=true;
    }
    break;
  }
  var elem;
  var i;
  for (i=0; i<cadre.length ; i++){
    elem=document.getElementById(cadre[i]);
    if (i==choix){
      if (choix==0)//console
	elem.style.height="100%";
      elem.style.display="block";
    }
    else if (i==0 && choix==1){
      elem.style.height="50%";
      elem.style.display="block";
    }
     else
       elem.style.display="none";
  }
  activeInput=document.getElementById(cadreInput[choix]);
  activeInput.setAttribute("autocomplete", "off"); 
  if (cadre[choix]=="texte")
    activeInput.style.display="block";
  else
    document.getElementById("editeur").style.display="none";
  activeInput.focus();
  if (choix==3 &&!grapheurCharge){
     grapheurCharge=true;
     if (isIE){
       setTimeout("svgGrid(-5,-5,5,5)",1000);
       restaureSvg();
       document.getElementById("combo").value="";
     }     
  }
  if (choix==5)
    document.getElementById("formatBlock").selectedIndex=0;
   return;
}

function createElementMathML(t) {
  if (isIE) return document.createElement("mml:"+t);
  else return document.createElementNS(mathmlURL,t);
}

function createElementSVG(t) {
  if (isIE) return document.createElement("svg:"+t);
  else return document.createElementNS(svgURL,t);
}

function nouveauScript(){
  changeScript=true;
}

function reqInit(){
  var req;
  if(window.XMLHttpRequest){
    req = new XMLHttpRequest(); 
  }
  else if(isIE) 
    req = new ActiveXObject("Microsoft.XMLHTTP"); 
    //req = new XMLHttpRequest(); 
  else { 
    alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
    return; 
  } 
  return req;
}

function reqG(req, s, url,option,maple_mode){
  var i,reg;
  // correction du bug sur les saisie d exposant
  for(i=4 ;i<10;i++){
    reg=RegExp(String.fromCharCode(i+8304),"g");
    s=s.replace(reg,"^"+i);
  }
  //s=s.replace(new RegExp("\"","g"),"\'\'");
  // cross domain possible avec firefox 3.5!!!!!
  //url ="http://vds1100.sivit.org/giac/giac_online/"+url;
  //alert(url);
  req.open('post',url, false);
  req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  //alert("option="+encodeURIComponent(option)+"&in="+encodeURIComponent(s));
  req.send("option="+encodeURIComponent(option)+"&maple_mode="+encodeURIComponent(maple_mode)+"&in="+encodeURIComponent(s));
  var s=req.responseText;
//alert("reponse="+s);
  for(i=0;i<htmlSymbols.length;i++){
    reg=RegExp(htmlSymbols[i],"g");
    s=s.replace(reg,utfSymbols[i]);
  }
  return s;
}

function reqProg(req, s){
  return reqG(req,s,"giac_interface.php","prog");
}


function reqGIAC(req, s){
  return reqG(req,s,"giac_interface.php","text");
}

function reqMath(req, s){
  return reqG(req,s,"giac_interface.php","math");
}

function reqMathGIAC(req, s){
  return reqG(req,s,"giac_interface.php","math_eval");
}
 
function reqSPREAD(req,s){
  return reqG(req,s,"giac_interface.php","spread");
}

function reqSPREADf(req,s){
  return reqG(req,s,"giac_interface.php","spreadf");
}

function reqSVG(req, s){
  return reqG(req,s,"giac_interface.php","svg");
}

function reqMathSVG(req, s){
  return reqG(req,s,"giac_interface.php","text_math_svg");
}
function reqSuiteMathml(req, s){
  return reqG(req,s,"giac_interface.php","suiteMathml");
}

function reqSuite(req, s){
 return reqG(req,s,"giac_interface.php","suite");
}


function reqSVGgrid(req,s){
  return reqG(req,s,"giac_interface.php","svg_grid");
}

// envoie le contenu de la fenetre "programmation"
function prog(){
  var s="";
  if (!changeScript)
    return s;
  if (!grapheurCharge)
      include("grapheur.js");
  var id=document.getElementById("script");
  if (id.value==inviteProg)
    return s;
  var tab=id.value.split(new RegExp("\n","g"));
  var i;
  for (i in tab){
    if (tab[i].substr(0,3)=="//*")
      tab[i]="";
    else
      tab[i]+="\n";
  }
  // echange avec le serveur
  var req=reqInit();
  var sp=reqProg(req,tab.join(""));
  var t=sp.split(new RegExp("`","g"));
  if (sp!="") {
    s="Dans votre programme, erreur dans la ligne "+t[0]+" juste avant "+t[1];
    tab[t[0]-1]="//* Erreur dans la ligne suivante, juste avant "+t[1]
      +"\n"+
      tab[t[0]-1];
    //var sc="";
    //for (i in tab)
     // sc+=tab[i]+"\n";
  }
  id.value=tab.join("");
  changeScript=false;
  ecrireCookie("prog",id.value);
  return s;
} 

// mode complexe ou reel
function complex_mode(textNode){
  var mode;
  var compl="Dans C";
  var reel="Dans R";
  if (textNode.data==reel){
    mode=1;
    textNode.data=compl;
  } else {
    mode=0;
    textNode.data=reel;
  } 
  var req=reqInit();
  reqGIAC(req,"complex_mode:="+mode);
  return;
}

// mode degr� ou radians
function angle_radian(textNode){
  var mode;
  var rad="Radians";
  var deg="Degr\u00E9s";
  if (textNode.data==deg){
    mode=1;
    textNode.data=rad;
  } else {
    mode=0;
    textNode.data=deg;
  } 
  var req=reqInit();
  reqGIAC(req,"angle_radian:="+mode);
  return;
}

// explore le dom et construit un clone pour Opera
function exploreDom(node,clone){
  var node1;
  if (node.tagName)
    node1=createElementMathML("mml:"+node.tagName);
  else
    node1=document.createTextNode(node.nodeValue);
  clone.appendChild(node1);
  var fils=node.firstChild;
  while (fils){
    exploreDom(fils,node1);
    fils=fils.nextSibling;
  }
  return clone;
}



// affiche le resultat en mathml
function insereMathML(id,s,inline,remplace){
  if (inline)
    display="inline";
  else
    display="block";
  if(isIE){
    var reg=RegExp("<","g");
    s=s.replace(reg,"<mml:");
    reg=RegExp("<mml:/","g");
    s=s.replace(reg,"</mml:");
    s="<mml:maction actiontype='link' dsi:href=''><mml:mrow>"+s+"</mml:mrow></mml:maction>";
    var mml = document.createElement("mml:math");
    mml.innerHTML+=s;
  } else if (isFirefox){
    var range = document.createRange();
    var mml=createElementMathML("math");
    // plantage avec opera ICI
    range.selectNodeContents(mml);
    var fragment = range.createContextualFragment(s);
    mml.appendChild(fragment);
  } else if (isOpera){
    var range = document.createRange();
    var brouillon=createElementMathML("math");
    var fragment = range.createContextualFragment(s);
    brouillon.appendChild(fragment);
    var mml=createElementMathML("mml:math");
    mml=exploreDom(brouillon,mml);
  } else if (isWebKit){
    //var mml=document.createElementNS("http://www.dessci.com/mathml","math");
    var mml=createElementMathML("math");
    var mml2=document.createElement("span");
    mml2.innerHTML=s;
    mml=exploreDom(mml2,mml);
  }
  mml.setAttribute("display",display);
  if (isIE)
    mml.update();
  if (remplace)
    id.parentNode.replaceChild(mml,id);
  else
    id.appendChild(mml);
  return mml;
}

function execSvg(fig,leg){
   insereSVG("figures",fig,false,tabSVG.length-1);
      insereSVG("legende",leg,true,tabSVG.length-1);
}






function giac(input){
  var id,p,text;
  // ------ syntaxe (xcas mapple ...)
  var req=reqInit();
  id=document.getElementById("syntaxe");
  var syntaxe="maple_mode("+id.selectedIndex+")";
  reqGIAC(req,syntaxe); 
  var s=input.value;
  if (s == "")
    return;
  // ------ on enregistre la requete
  tabHisto.push(s);
  indexTabHisto=tabHisto.length;
  input.value="";
  input.focus();
  text=document.createTextNode(s+" ");
  id=document.getElementById("histo");
  p=document.createElement("p");
  id.appendChild(p);
  var span=document.createElement("span");
  span.onclick=function(){activeInput=document.getElementById("in");touche(text.nodeValue)};
  span.appendChild(text);
  p.appendChild(span);

  //------ traitement du programme eventuel
  var sprog=prog();
  if (sprog!=""){
    text=document.createTextNode(sprog);
    p.appendChild(text);
  } else {
    //------- traitement de l'instruction
    var sr=reqMathSVG(req,s); 
    //alert(sr);
    // rustine pour IE
    if (isIE)
      sr=sr.replace(new RegExp("``","g"),"` `");
    var t=sr.split(new RegExp("`","g"));
    // on affiche le resultat en mathml  
    ans=t[0];
    var mml,sMml;
    //-------- on affiche le graphique eventuel
    if (t[6] && t[6] != "" && t[6]!=" " && t[6] != "error" && t[6]!="undef"){
      var hist=document.getElementById("histSVG");
      afficheCadre(3);
      tabSVG.push(s);
      tabCSVG.push(-1);
      insereSVG("figures",t[6],false,tabSVG.length-1);
      insereSVG("legende",t[7],true,tabSVG.length-1);
      // on affiche l'instruction
      ajouteOptionSvg(hist,s,tabSVG.length-1);
      ecrireCookie("svg",tabSVG.join("`"));
      ecrireCookie("csvg",tabCSVG.join("`"));
      if (t[2]!="" && t[2]!=" ")
	sMml=t[1]+t[2]+t[3]+t[5];
      else
	sMml=t[4];
    } else {
      if (t[2]!="" && t[2]!=" ")
	sMml=t[1]+t[2]+t[3]+t[4]+t[5];
      else
	sMml=t[4];
    }
    mml=insereMathML(p,sMml,false,false);
    mml.onclick=function(){activeInput=document.getElementById("in");touche("("+t[0]+")");};
    // on scrolle en bas
    var fin=document.getElementById("fin");
    fin.scrollIntoView(false);
    consoleCookie+=s+"`"+sMml+"`"; 
    ecrireCookie("console",consoleCookie);
  }

  return;
}


function effaceHisto(confirme){
  if (confirme)
    if (!confirm("Cela effacera la console ET les variables !"))
	return;
  // sur le serveur
  var req=reqInit();
  var url="giac_interface.php";
  req.open('post',url, false);
  req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
  //req.send("efface="+encodeURIComponent("on"));
  req.send("option="+encodeURIComponent("efface"));
  // sur le poste local 
  var histo=document.getElementById("histo");
  if (!histo.firstChild)
    return;
  while (histo.childNodes.length>0)  
    histo.removeChild(histo.lastChild);
  return;
}


function ecrireCookie(nom,val){
  var date=new Date();
  date.setFullYear(date.getFullYear()+1);
  document.cookie=nom+"="+escape(val)+";expires="+date;

  //date.setFullYear(date.getYear()+1901,1,30);
  //document.cookie=nom+"="+escape(val)+";expires="+date.ToGMTString;
  //alert(document.cookie);
}

function lireCookie(nom){
  var s=document.cookie;
  var i=s.indexOf(nom,0);
  if (i==-1)
    return "";
  s=s.substring(i);
  i=s.indexOf("=",0);
  var j=s.indexOf(";",0);
  if (j!=-1)
    s=s.substring(i+1,j);
  else
    s=s.substring(i+1);
  return unescape(s);
}
 
function integreCookie(nom,id,s){
  if (!s)
    var s=lireCookie(nom);
  if (nom=="console"){
    var tab=s.split("`");
    var  p,id=document.getElementById("histo");
    var i,t;
    var n=tab.length-1
    for(i=0; i<n ; i=i+2){
      p=document.createElement("p");
      id.appendChild(p);
      t=document.createTextNode(tab[i]+" ");
      p.appendChild(t);
      insereMathML(p,tab[i+1],false,false);
    }
    var fin=document.getElementById("fin");
    fin.scrollIntoView(false);
    return;
  }
  if (s=="")
    return;
  if (id=="script")
    document.getElementById(id).value=s;
  else
    document.getElementById(id).innerHTML=s;
}

function initialiseProg(){
  var script=document.getElementById("script");
  if (script.value!="")
    changeScript=true;
  else {
    script.value=inviteProg;
    changeScript=false;
  }
}


function lireCookies(){
  document.cookie="test=test";
  if (document.cookie==""){
    alert("Il faut accepter les cookies pour sauvegarder votre travail");
    initialiseProg();
    return;
  }
  if (document.cookie=="test=test"){
    initialiseProg();
    return;
  }
  if (confirm("Restaurer la sauverarde d'urgence ?")){
    restaureSession=true;
    integreCookie("console","histo");
    tabSVG=lireCookie("svg").split("`");
    tabCSVG=lireCookie("csvg").split("`");
    integreCookie("prog","script");
  } else {
    restaureSession=false;
    effaceHisto(false);
    initialiseProg();
  }
}

function selectSVG(id,selected){
  if (isIE) 
    return; 
  id=id.replace(new RegExp("svg","g"),"");
  var index=parseInt(id);
  if (index==-1)
    index=selectedSVG;
  else if (selected)
    selectedSVG=index;
  var combo=document.getElementById("combo");
  combo.value=tabSVG[index];
  var i,n;
  var histSVG=document.getElementById("histSVG");
  if (!selected)
    return;
  histSVG.selectedIndex=-1;
  for (i=0; i<histSVG.options.length ;i++) {
     var n=histSVG.options[i].getAttribute("isvg");
     if (selectedSVG==parseInt(n)){
       histSVG.selectedIndex=i;
       break;
     }
  }
}


function accoladeOuvrante(s){
  var acc=[
	  /\{[\(\s]/i, 
	  /\balors[\(\s]/i,
	  /\bsinon[\(\s]/i,
	  /\bfaire[\(\s]/i,
	  /\brepeter[\(\s]/i,
	  ];
  var i,j=-1;
  for (i in acc)
    if (s.search(acc[i])>-1)
      j=i;
  return j;
}

function accoladeFermante(s,j){
  if (!j)
    return false;
  if (j==-1)
    return false;
  var acc=[
	   /\}[;\(\s]/i,  
	   /\bsinon[\(\s]/i,
	   /\bfsi[;\(\s]/i,
	   /\bfpour[;\(\s]/i,
	   /\bjusqua[\(\s]/i,
	   /\bftantque[;\(\s]/i,
	  ];
  var i,k=-1;
  for (i in acc)
    if (s.search(acc[i])>-1)
      k=i;
  if (k==j)
    return true;
  if (j==1 && k==2 || j==3 && k==5)
    return true;
  return false; 
} 


function contientMotCle(s){
  var motCles=[
	       /\bif[\(\s]/i,
	       /\belse[\r\{\s]/i,
	       /\bfor[\(\s]/i,
	       /\bwhile[\(\s]/i,
	       /\bfunction\b/i,
	       /\bsi[\(\s]/i,     
	       /\balors[\(\s]/i,
	       /\bsinon[\(\s]/i,
	       /\bpour[\(\s]/i,
	       /\brepeter[\(\s]/i,
	       /\btantque[\(\s]/i,
	       /\bfaire[\(\s]/i,
	       ];
  var i;
  for (i in motCles)
      if (s.search(motCles[i])>-1)
          return true;
  return false;
}


function indente(){
  var lignes=new Array();
  var script=document.getElementById("script");
  var prog=script.value;
  script.value="";
  lignes=prog.split(new RegExp("\n","g"));
  var i=0,j;
  while (i<lignes.length){
    lignes[i] = lignes[i].replace(/^\s*|\s*$/,"");
    if (lignes[i]=="")
      lignes.splice(i,1);
    else {
      lignes[i]+="\n";
      i++;
    }
  }
  while (lignes[lignes.length-1]=="")
    lignes.pop();
  var deca=0;
  var accOuv=[-1];
  var blanc,ouvre,ferme, mot=false;
  for (i=0 ; i<lignes.length ; i++){
    ferme=accoladeFermante(lignes[i],accOuv[deca]);
    if (ferme && deca>0){
      deca--;
      accOuv.pop();
    }
    blanc="";
    for (j=0; j<deca ; j++)
      blanc+="  ";
    if (ouvre==-1 && mot)
      script.value+=blanc+" "+lignes[i];
    else
      script.value+=blanc+lignes[i];
    mot=contientMotCle(lignes[i]);
    ouvre=accoladeOuvrante(lignes[i]);
    if (ouvre>-1){
      deca++;
      accOuv.push(ouvre);
    }  
  }
}

