<?php
session_start();
// autorisation eventuelle du cross-domain
// il faut resoudre encore le pb des cookies (pour les sessions)
header("Access-Control-Allow-Origin:*");
// neutralise le cache
header("Expires:Sat,1 Jan 2000 00:00:00 GMT");
header("Cache-control : no-store, no-cache, must-revalidate");
header("Cache-Control : post-check=0, pre-check=0",false);

//on avertit le navigateur qu'on lui envoie du html (text/html remplace par text)
header("Content-Type: text/plain; charset=iso-8859-1");
//header("Content-Type:text; charset=UTF-8");

// systeme en francais semble sans effet
//putenv("LANG=fr_FR.UTF-8");
//semble censure
//putenv("PATH=" .$_ENV["PATH"]. ":/home/jp");
//print $_ENV["PHP_"];

//system("export LANG=\"fr_FR.UTF-8\"");
//dl("phpgiac.so");
// initialisation des variables pour essayer de faire taire les warning
// semble sans effet
$option="";
$archive="";
$retour="";
// limite le temps de calcul � 1 seconde sur le serveur
set_time_limit(1);
if (isset($_POST["option"])){
  $option=$_POST["option"];
  if ($option=="efface") {
    session_destroy();
  }else {
    // autre essai pour faire taire les warning
    if (isset($_SESSION["archive"]))
      $archive=$_SESSION["archive"];
    $context=session_id();
    // essai de correction du bug "purge"
    giac_eval_txt("rm_all_vars()",$context);
    //  essai reussi de mettre en espagnol
    //print giac_lang(3,$context);
    // initialisation du generateur aleatoire
    giac_eval_txt("srand()",$context);
    giac_unarchive_session($archive,$context);
    if (isset($_POST["in"])){ 
      $input=$_POST["in"];
      //print $input."\n";
      $input=stripslashes($input);
      // pout eliminer l'entete xml
      $xml= <<<'EOT'
<?xml version="1.0" encoding="iso-8859-1"?>
EOT;
      switch ($option) {
      case "prog" :
	$retour=giac_eval_prog($input,$context);
	break;
      case "text" :
	$retour=giac_eval_txt_mathml($input,$context);
	break;
      case "math" :
	$retour=giac_txt_mathml($input,$context);
	break;
      case "math_eval" :
	$retour=giac_txt_mathml_et_eval($input,$context);
	break;
      case "spread" :
	$retour=giac_eval_spread($input,$context);
	break;
      case "spreadf" :
	$retour=giac_evalf_spread($input,$context);
	break;
      case "svg" :
	$retour=giac_eval_svg($input,$context);	
	$retour=str_replace ($xml, "",$retour);
	if ($retour=="undef`" || $retour=="`" || $retour=="error`" || $retour=="`")
	  $retour="E";
	break;
      case "text_math_svg" :
	$retour=giac_eval_txt_mathml_svg($input,$context);
	break;
      case "svg_grid" :
	giac_eval_txt_mathml($input,$context);
	$retour=giac_svg_preamble(11,11)."<g id=\"figures\"></g></g><g>".giac_svg_grid()."<g id='legende'></g></svg>";
	$retour=str_replace ($xml, "",$retour);
	break;
      case "suiteMathml" :
	$in=explode("`",$input);
	$retour=giac_suite($in[0],$in[1],$in[2],$in[3],true,true,$context);
	break;
      case "suite" :
	$in=explode("`",$input);
	$retour=$in[3];
	$retour=giac_suite($in[0],$in[1],$in[2],$in[3],$in[4],$in[5],$in[6],$in[7],$in[8],$in[9],$context);
	break;
      }
    }
    print $retour;//."\n";
    if ($option!="spread"){
      $archive=giac_archive_session($context);
      $_SESSION["archive"]=$archive;
    }
  }
 }
?>
