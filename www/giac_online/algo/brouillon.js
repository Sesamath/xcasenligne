
// log decimal
function log(x){
  return Math.log(x)/Math.log(10);
}

// calcul du pas de la grille
function pas(pas){
  pas=pas/12.5;
  var a,b,dixN;
  a=log(pas);
  b=a-Math.floor(a);
  dixN=Math.pow(10,a-b);
  if (b<log(2))
    return dixN;
  else if (b<log(4))
    return 2*dixN;
  else if (b<log(8))
    return 5*dixN;
  else
    return 10*dixN;
}

 this.xPas=pas(Dx);
  this.yPas=pas(Dy);
  this.ctx.beginPath();
  this.ctx.strokeStyle="red";
  this.moveTo(this.x(0),this.y(0));
  this.lineTo(this.x(xPas),this.y(0));
  this.moveTo(this.x(0),this.y(0));
  this.lineTo(this.x(0),this.y(yPas));
  this.ctx.stroke(); 
   this.ctx.strokeStyle="black"; 
