   
function effacer(){
  if (!confirm("Effacer le programme ?"))
    return;
  var h=document.getElementById("h");
  h.value="";
  h.focus();
}

// variable globale : true si on avorte le programme (voir fonction demander)
var stop=false;

function lancer(){
  var prog=document.getElementById("h").value;
  try {
    eval(prog);
  }
  catch(err){
    afficher(err.name," : ",err.message);
    afficher("Sous Firefox, vous pouvez activer Outils/console d'erreur : celle-ci affiche l'instruction erron�e (ou quelquefois l'instruction juste apr�s)");
    //on le remet pour activer la console d'erreur de Firefox :
    if (stop)
      stop=false;
    else
      eval(prog);
  }
}

function afficher() {
  var idLieu="b";
  var lieu =document.getElementById(idLieu);
  var p=document.createElement("p");
  var texte="";
  for (var i=0; i<arguments.length ; i++)
    texte+=arguments[i]+" ";
  var text=document.createTextNode(texte);
  p.appendChild(text);
  lieu.appendChild(p);
  p.scrollIntoView(false);
}

// convertit une chaine en nombre SI PERTINENT
function nombre(s){
  var n=parseInt(s);
  var x=parseFloat(s);
  if (isNaN(n) && isNaN(x))
    return s;
  if (x==n)
    return n;
  return x;
}

function demander(){
  var message="";
  for (var i=0; i<arguments.length ; i++)
    message+=arguments[i]+" ";
  var x=prompt(message+" (ou taper stop pour avorter)");
  if (x=="stop") {
    stop=true;
    eval("x=avorte programme"); 
  }
  var s=x+" ";
  var t=s.split(new RegExp("[,;]+","g"));
  if (t.length<2)
    return nombre(x);
  for (var i=0 ; i<t.length ; i++)
    t[i]=nombre(t[i]);
  return t;
}

// log decimal
function log(x){
  return Math.log(x)/Math.log(10);
}

// calcul du pas de la grille
function pas(pas){
  pas=pas/12.5;
  var a,b,dixN;
  a=log(pas);
  b=a-Math.floor(a);
  dixN=Math.pow(10,a-b);
  if (b<log(2))
    return dixN;
  else if (b<log(4))
    return 2*dixN;
  else if (b<log(8))
    return 5*dixN;
  else
    return 10*dixN;
}

function repere(xMin,yMin,xMax,yMax,larg,haut){
  this.xMin=xMin;
  this.yMin=yMin;
  this.Dx=xMax-xMin; 
  this.Dy=yMax-yMin;
  var c=document.createElement("canvas");
  if (!c.getContext)
    afficher("d�sol�, votre navigateur ne sait pas dessiner !");
  if (larg)
    this.larg=larg;
  else
    this.larg=100;
  if (haut)
    this.haut=haut;
  else
    this.haut=100;
  c.setAttribute("width",this.larg);
  c.setAttribute("height",this.haut);
  document.getElementById("b").appendChild(c);
  this.ctx = c.getContext("2d");
  this.ctx.beginPath();
  this.ctx.moveTo(this.x(0),this.y(yMin));
  this.ctx.lineTo(this.x(0),this.y(yMax));
  this.ctx.moveTo(this.x(xMin),this.y(0));
  this.ctx.lineTo(this.x(xMax),this.y(0));
  this.ctx.stroke();
  this.xPas=pas(Dx);
  this.yPas=pas(Dy);
  this.ctx.beginPath();
  this.ctx.strokeStyle="red";
  this.moveTo(this.x(0),this.y(0));
  this.lineTo(this.x(xPas),this.y(0));
  this.moveTo(this.x(0),this.y(0));
  this.lineTo(this.x(0),this.y(yPas));
  this.ctx.stroke(); 
  this.ctx.strokeStyle="black";  
  // non implemente par les navigateur
  //this.ctx.font = "1em";
  //this.ctx.fillText("test",100,100);
  return;
}

repere.prototype.couleurFond=function(color){
  this.ctx.fillStyle=color;
}
  
  repere.prototype.couleurTrait=function(color){
  this.ctx.strokeStyle=color;
}



    repere.prototype.x=function(abscisse){
      return this.larg*(abscisse-this.xMin)/this.Dx;
    }

      repere.prototype.y=function(ord){
	return this.haut-this.haut*(ord-this.yMin)/this.Dy;
      }

	repere.prototype.allerEn=function(x,y){
	  this.ctx.beginPath();
	  this.ctx.moveTo(this.x(x),this.y(y));
	}

	  repere.prototype.ligneVers=function(x,y){
	    this.ctx.lineTo(this.x(x),this.y(y));
	    this.ctx.stroke();
	  }

	    repere.prototype.cercle=function(x,y,r) {
	      this.ctx.beginPath();
	      r=r*this.larg/this.Dx;
	      this.ctx.arc(this.x(x), this.y(y), r, 0, Math.PI*2, true);
	      this.ctx.stroke();
	    }

	      repere.prototype.rect=function(xm,ym,xM,yM,plein){
		var x=this.x(xm);
		var y=this.y(ym);
		var larg=this.x(xM)-x;
		var haut=this.y(yM)-y;
		if (plein)
		  this.ctx.fillRect(x,y,larg,haut);
		else 
		  this.ctx.strokeRect(x,y,larg,haut);
	      }

		repere.prototype.rectPlein=function(xm,ym,xM,yM){
		  this.rect(xm,ym,xM,yM,true)
		}

		  repere.prototype.rectCreux=function(xm,ym,xM,yM){
		    this.rect(xm,ym,xM,yM,false)
		  }

		  // test pour ajouter des methodes
		    repere.prototype.test=function(){
		      this.ctx.fillStyle = "rgb(200,0,0)";
		      this.ctx.fillRect (10, 10, 55, 50);
		      this.ctx.fillStyle = "rgba(0, 0, 200, 0.5)";
		      this.ctx.fillRect (30, 30, 55, 50);
		    } 



		      var tableIndex=0;

function tableau(){
  var tab=document.createElement("table");
  this.id="t"+tableIndex;
  tableIndex++;
  tab.setAttribute("id",this.id);
  var thead=document.createElement("thead");
  tab.appendChild(thead);
  this.nbCol=arguments.length;
  var th,txt;
  for (var i=0 ; i<arguments.length ; i++) {
    th=document.createElement("th");
    txt=document.createTextNode(arguments[i]);
    th.appendChild(txt);
    thead.appendChild(th);
  }
  document.getElementById("b").appendChild(tab);
}

tableau.prototype.ajouter=function(){
  var tab=document.getElementById(this.id);
  var tr=document.createElement("tr");
  tab.appendChild(tr);
  for (var i=0; i<this.nbCol ; i++) {
    td=document.createElement("td");
    txt=document.createTextNode(arguments[i]);
    td.appendChild(txt);
    tr.appendChild(td);
  }
}
