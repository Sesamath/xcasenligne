
//--------- gestion du tableur ----------

// initialisation dans la page :
var cell_active='A0';
var cell_marquees=new Array();
// spreadCookie, nb_col et nb_lign definies dans demoGiacPhp.js
var nb_lign_sauv, nb_col_sauv;
var lettres=['A','B','C','D','E','F','G'];
var saisie=false;
//document.getElementById("tab_entry").setAttribute("autocomplete", "off"); 

restaureTab(); //avec Cookie

function ctrl(ev,tab_entry){
 if (ev.keyCode==17)
    tab_entry.select();
 return;
}

function modeSaisie(ev,tab_entry){
// probleme avec Opera qui ne d�tecte pas ev.ctrlKey
  if (ev.ctrlKey){
    if (ev.keyCode==67){
      copy();
      saisie=false;
    } else if (ev.keyCode==86)
      paste();
    return;
  }
  if (ev.keyCode==13){ //entree
    saisie=false;
    return;
  }
  if (tab_entry.value.charAt(0)!="=")
    return;
  if (ev.keyCode!=17)
    saisie=true;	
  return;
}

function coordonnees(cellule){
 //alert(cellule);
  var s=cellule.slice(1);
  return [cellule.charAt(0), parseInt(s)];
}

function cell_max(cellule){
  var t=coordonnees(cellule);
  var A="A";
  nb_col_sauv=nb_col;
  nb_lign_sauv=nb_lign;
  nb_lign=Math.max(nb_lign,t[1]+1);
  nb_col=Math.max(nb_col,Number(t[0].charCodeAt(0))-A.charCodeAt(0)+1);
  return;
}

function cell(cellule){
  var tab_entry=document.getElementById("tab_entry");
  if (saisie){
    tab_entry.focus();
    tab_entry.value+=cellule;
    return;
  }
  var dom_cellule=document.getElementById(cell_active);
  dom_cellule.style.backgroundColor="white";
  dom_cellule.style.color="black";
  cell_active=cellule;
  dom_cellule=document.getElementById(cell_active);
  dom_cellule.style.backgroundColor="#ffff99";
  //var img=document.createElement("img");
  //img.setAttribute("scr","img/rect.gif");
  //img.setAttribute("style","position:absolute;bottom:0;right:0");
  //dom_cellule.appendChild(img);
  var tab_cell=document.getElementById("tab_cell");
  tab_cell.firstChild.nodeValue=cellule;
  tab_entry.value=dom_cellule.getAttribute('formule');
  tab_entry.focus();
}


function annule(){
  var i,t,dom_cellule;
  nb_lign=nb_lign_sauv;
  nb_col=nb_col_sauv;
  for (i=0; i<cell_marquees.length;i++){
      dom_cellule=document.getElementById(cell_marquees[i]);
      t=dom_cellule.getAttribute("ancienne");
      dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
      dom_cellule.setAttribute("formule",t);
    }
  calculeTab();
  return;
}

function subst_cell(){
 var dom_cellule=document.getElementById(cell_active);
 var tab_entry=document.getElementById("tab_entry");
 dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
 dom_cellule.setAttribute("formule",tab_entry.value);
 return;
}

function vide(){
  if (!confirm("Vider la feuille ?"))
    return;
  var i,j, dom_cellule, t,cell;
  cell_marquees=[];
  for (i=0; i<nb_col; i++)
    for (j=0; j<nb_lign ; j++){
      cell=lettres[i]+j;
      dom_cellule=document.getElementById(cell);
      dom_cellule.setAttribute("ancienne",dom_cellule.getAttribute("formule"));
      dom_cellule.setAttribute("formule","");
      cell_marquees.push(cell);
      if (dom_cellule.firstChild)
	dom_cellule.removeChild(dom_cellule.firstChild);
    }
  nb_col=0;
  nb_lign=0;
  ecrireCookie("nb_col","");
  return;
}

function fill_cell(statique){
  cell_marquees=[cell_active];
  cell_max(cell_active);
  subst_cell();
  var t=coordonnees(cell_active);
  calculeTab();
  if (statique)
    return;
  if (t[1]<n_lign-1){
    t[1]+=1;
    saisie=false;
    cell(t[0]+t[1]);
  }
  return;
}




//translation de coordonnees de cellules
// nouvelle version
function translation(chaine,k,l){
  var chiffres=["0","1","2","3","4","5","6","7","8","9"];
  var i=0;
  var absLi,absCol=false;
  var refCol=-1,refLi;
  var nouvelleChaine="";
  var s,lettre;
  while (i<chaine.length){
    lettre=chaine.charAt(i);
    if (lettre=="$"){
	absCol=true;   
	nouvelleChaine+=lettre;
	i++;
    } else {
      if (refCol==-1){
	for (j in this.lettres)
	  if (lettre==this.lettres[j]){
	    refCol=j;
	    break;
	  }
      } 
      if (refCol==-1){
	nouvelleChaine+=lettre;
	i++;
      }
      else {
	refLi=0;
	i++;
	lettre=chaine.charAt(i);
	if (lettre=="$"){
	  absLi=true;
	  i++;
	  lettre=chaine.charAt(i);
	}
	j=i;
	while (lettre in chiffres){
	  refLi=10*refLi+parseInt(lettre);
	  i++;
	  if (i<chaine.length)
	    lettre=chaine.charAt(i);
	  else
	    lettre="";
	}
	if (j!=i) {
	  if (!absCol)
	    refCol=parseInt(refCol)+k;
	  if (!absLi)
	    refLi+=l;
	  else
	    refLi="$"+refLi;
	  nouvelleChaine+=this.lettres[refCol]+refLi;
	}
	else
	  nouvelleChaine+=lettre;
	refCol=-1;
	absCol=false;
	absLi=false;
      }
    }
  }
  return nouvelleChaine;
}

 

var ref_copy;
var form_copy;
function copy(){
  saisie=false;
  ref_copy=cell_active;
  dom_cellule=document.getElementById(cell_active);
  var t=document.getElementById("tab_entry");
  form_copy=t.value;
  dom_cellule.setAttribute("formule",t.value);
  return;
}


function paste(){
  cell_marquees=[cell_active];
  cell_max(cell_active);
  var reg_lettre=RegExp("[A-E]","g");
  var lettre_cell=String(cell_active.match(reg_lettre));
  var lettre_ref=String(ref_copy.match(reg_lettre));
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre_cell=Number(cell_active.match(reg_nombre));
  var nombre_ref=Number(ref_copy.match(reg_nombre));
  dom_cellule=document.getElementById(cell_active);
  var c=translation(form_copy, lettre_cell.charCodeAt(0)-lettre_ref.charCodeAt(0) ,nombre_cell-nombre_ref );
  dom_cellule.setAttribute("formule",c);
  var tab_entry=document.getElementById("tab_entry");
  tab_entry.value=c;
  calculeTab();
  return;
}

function fill_bottom() {
  cell_marquees=[];
  subst_cell();
  var cell=document.getElementById(cell_active);
  var formule=cell.getAttribute("formule");
  var reg_lettre=RegExp("[A-Z]","g");
  var letter=cell_active.match(reg_lettre);
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre=Number(cell_active.match(reg_nombre));
  var i=0;
  for (i=nombre+1 ; i<n_lign ; i++){ 
    cell_marquees.push(letter+i);
    cell =document.getElementById(letter+i);
    cell.setAttribute("formule",translation(formule,0,i-nombre));
  }
  i=n_lign-1;
  cell_max(letter+i);
  calculeTab();
  saisie=false;
  return;
}

function fill_right() {
  cell_marquees=[];
  subst_cell();
  var cell=document.getElementById(cell_active);
  var formule=cell.getAttribute("formule");
  var cell=document.getElementById(cell_active);
  var reg_lettre=RegExp("[A-Z]","g");
  var letter=String(cell_active.match(reg_lettre));
  var code=Number(letter.charCodeAt(0));
  var reg_nombre=RegExp("[0-9].|[0-9]","g");
  var nombre=Number(cell_active.match(reg_nombre));
  var fin=lettres[n_col-1].charCodeAt(0)-code+1;
  for (i=0 ; i<fin ; i++){
    letter=String.fromCharCode(code+i);
    cell_marquees.push(letter+nombre);
    cell =document.getElementById(letter+nombre);
    cell.setAttribute("formule",translation(formule,i,0));
  }
  //correction bug :  n_lign remplace par n_lign-1
  if (nombre<n_lign-1)
    nombre++;
  cell_max(lettres[n_col-1]+nombre);
  calculeTab();
  saisie=false;
  return;
}

function spreadForm(i,j){
  var cell=document.getElementById(lettres[j]+i);
  var f=cell.getAttribute("formule");
  if (f!="" && f!=" ")
    return f;
  else
    return "0";
}

function cellSubst(cell,s,m){
  var l,L,f;
  result=cell.getAttribute("result");
  var f=cell.getAttribute("formule");
  spreadCookie+=f+"`";
  if (result==s && result!="0")
    return;
  cell.setAttribute("result",s);
  if (cell.firstChild)
    cell.removeChild(cell.firstChild);
  l=cell.clientWidth+0;
  if (f!="" && f!=" "){
    insereMathML(cell,m,false,false);
    if (isIE){
      //var tab=document.getElementById("tableau");
      var tab=document.getElementById("table");
      L=cell.firstChild.clientWidth+0;
      if (L>l){
       cell.style.width=L;
       tab.style.width=tab.clientWidth+L-l;
      }
      else
       cell.style.width=l;
    }
  } 
  return;
}

function spreadSubst(i,j,s,m){
  var cell=document.getElementById(lettres[j]+i);
  cellSubst(cell,s,m);
  return;
}

function actualiseTab(s){
  var t=s.split(new RegExp("`","g"));
  var k;
  spreadCookie="";
  // on affiche en premier la cellule active
  // sans effet ...
  //var act=coordonnees(cell_active);
  //var t0=new Date();
  //var A="A";
  // bogue le cookie : 
  //k=2*((Number(act[0].charCodeAt(0))-A.charCodeAt(0))+nb_col*act[1]);
  //cellSubst(document.getElementById(cell_active),t[k],t[k+1]);
  for (i=0; i<nb_lign ; i++)
    for (j=0 ; j<nb_col ; j++){
      k=2*(i*nb_col+j);
      spreadSubst(i,j,t[k],t[k+1]);
    }
  ecrireCookie("nb_lign",nb_lign);
  ecrireCookie("nb_col",nb_col);
  //alert(spreadCookie);
  ecrireCookie("formules",spreadCookie);
}


function calculeTab(){
  var spreadheet=new String();
  spreadsheet="spreadsheet[";
  var cell=document.getElementById('AO');
  var i=0;
  var j=0;
  var der_col=nb_col-1, der_lign=nb_lign-1;
  var formule="";
  for (i=0 ; i<nb_lign ; i++){
    spreadsheet+="[";
    for (j=0 ; j<der_col ; j++){
      formule=spreadForm(i,j);
      spreadsheet+="[regrouper("+formule+"),1,0],";
    }
    formule=spreadForm(i,j);
    spreadsheet+="[regrouper("+formule+"),1,0]]";
    if (i==der_lign)
      spreadsheet+="]";
    else
      spreadsheet+=",";
  }
  var req=reqInit();
  var s;
  if (document.getElementById("typeTab").selectedIndex==0)
    s=reqSPREAD(req,spreadsheet);
  else
    s=reqSPREADf(req,spreadsheet); 
  if (s.indexOf("Fatal error")>=0){
    alert("DUREE EXCESSIVE. CALCUL REFUSE. Passez en mode numerique")
    return;
  }
  actualiseTab(s);
  return;
}

function restaureTableurSansCookie(nLi,nCol,formule){
  nb_lign=parseInt(nLi);
  nb_col=parseInt(nCol);
  var der_col=nb_col-1, der_lign=nb_lign-1;
  var tab=formule.split(new RegExp("`","g"));
  var cell;
  for (i=0; i<nb_lign ; i++)
    for (j=0 ; j<nb_col ; j++){
      cell=document.getElementById(lettres[j]+i);
      k=i*nb_col+j;
      cell.setAttribute("formule",tab[k]);
    }
  calculeTab();
}

function restaureTab(){
  if (!restaureSession)
    return;
  var nCol=lireCookie("nb_col");
  if (nCol=="")
    return;
  var nLi=lireCookie("nb_lign");
  var formule=lireCookie("formules");
  restaureTableurSansCookie(nLi,nCol,formule);
}

function ajouteLigne(){
  var table=document.getElementById("table");
  var n=n_col+1;
  var tr=table.insertRow(-1);
  var txt,lettre;
  var td;
  for (var i=0; i<n ; i++) {
    td=tr.insertCell(i);
    if (i==0){
      td.setAttribute("class","col1");
      td.style.backgroundColor="lightgrey";
      td.style.textAlign="center";
      txt=document.createTextNode(n_lign);
      td.appendChild(txt);
      tr.appendChild(td);
    }
    else {
      lettre=lettres[i-1];
      td.setAttribute('id',lettre+n_lign);
      td.setAttribute("formule","");
      td.setAttribute("result","");
      td.onclick=function(){cell(this.id);};
   }
  }
  n_lign++;
}


cell('A0');
