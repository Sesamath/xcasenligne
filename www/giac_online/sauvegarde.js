var marqueurSauv="@`";

function enregistrer(form,source) {
  //alert(consoleCookie+"---"+document.getElementById("script").value+"---"+spreadCookie+"---"+tabSVG.join("'")+"---"+suitesCookie);
  var s="";
  if (document.getElementById("s0").checked)
    s+=consoleCookie;
  s+=marqueurSauv;
  if (document.getElementById("s1").checked)
    s+=document.getElementById("script").value;
  s+=marqueurSauv;
  if (document.getElementById("s2").checked)
    s+=nb_lign+marqueurSauv+nb_col+marqueurSauv+spreadCookie;
  else
    s+=marqueurSauv+marqueurSauv;
  s+=marqueurSauv;
 if (document.getElementById("s3").checked && tabSVG!=null)
   s+=tabSVG.join("'");
  s+=marqueurSauv;
 if (document.getElementById("s4").checked)
   s+=suitesCookie;
  s+=marqueurSauv;
 if (document.getElementById("s5").checked)
   s+=document.getElementById("editeur").innerHTML;
  s+=marqueurSauv;
 //alert(s);
  form.action ="enregistrer.php";
  s=s.replace(/\'/g,"'");
  form.texte.value=s;
 //  form.action ="enregistrer.php";
//   var texte=document.getElementById(source).value;
//   texte=texte.replace(/\'/g,"'");  
//   form.texte.value=texte;
  form.submit();
}

function ouvrir(form) { 
  //alert(destination);
  form.action ="ouvrir.php";
  form.submit();
}

// on uploade le fichier vers ouvrir.php
// et ouvrir.php renvoie <script> parent.retour_ouvrir(contenu du fichier upload�,destination)

function retour_ouvrir(contenu) {
  //alert(contenu);
  contenu=contenu.replace(/\'/g,"'");                   // � cause des �ventuels magic_quotes et addslashes
  contenu=contenu.replace(/&&/g,"\n");                  // on remet les sauts de ligne
  // extraction du contenu
  var tab=contenu.split(marqueurSauv);
  if (tab[0]!="")
    integreCookie("console","histo",tab[0]);
  if (tab[1]!="")
    integreCookie("prog","script",tab[1]);
  if (tab[2]!=""){
    if (!tableurCharge){
      include("tableur.js");
      tableurCharge=true;
    }
    setTimeout(function(){restaureTableurSansCookie(tab[2],tab[3],tab[4]);},500);
  }
  if (tab[5]!=""){
     tabSVG=tab[5].split("`");
     if (!grapheurCharge)
       include("grapheur.js");
     setTimeout("restaureSvg()",500);
  }
  if (tab[6]!=""){
    if (!suiteCharge){
      include('suite.js');
      suiteCharge=true;
    }
    setTimeout(function(){restaureSuitesSansCookie(tab[6]);},500);
  }
  if (tab[7]!=""){
    document.getElementById("editeur").innerHTML=tab[7];
  }
  document.getElementById("fichier").form.reset();
}
