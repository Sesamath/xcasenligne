Ce dépôt est une archive du site https://www.xcasenligne.fr/ et son module
php_giac.

Ce site permettait d'utiliser Xcas (développé par Bernard Parisse) en ligne
via un module php (php_giac), développé par Jean-Pierre Branchard.

Ce module n'est plus compatible avec les versions moderne de php (la
dernière à fonctionner était php 5.3).

Si quelqu'un peut adapter ce module aux versions actuelles de php, qu'il
n'hésite pas à le signaler, le site pourrait alors réouvrir.

Les sources (en C) sont dans le dossier php_giac.
